#ifndef MONITOR_H
#define MONITOR_H

#include <QWidget>
#include <QTimer>
#include <duktape.h>
#include "src/ECS/Node.h"
#include "src/ECS/Rect.h"
#include <QOpenGLWidget>

class Monitor : public QOpenGLWidget
{
   Q_OBJECT
public:
   explicit Monitor(QWidget *parent = nullptr);
   static void registerScripting(duk_context* ctx);

   void setViewport(const QRectF& viewport);
   const QRectF& getViewport() const { return vp; }

   void setECSRoot(ecs::Node* newRoot) { root = newRoot; }
   ecs::Node* getECSRoot() { return root; }

   const QTransform &getWorldTransform() const { return wt; }

   void setBackgroundColor(const QColor& bkgcol);
   const QColor& getBackgroundColor() const { return bgcol; }

   void setOriginColor(const QColor& originColor);
   const QColor& getOriginColor() const { return orgcol; }

   void setDrawOrigin(bool drawOrg);
   bool getDrawOrigin() const { return drawOrigin; }

   QTimer& getTimer() { return timer; }

protected:
   void initializeGL() override;
   void paintEvent(QPaintEvent *e) override;

   void applyVP(QPainter& p);
   QRectF vp;
   QTransform wt;
   ecs::Node *root = nullptr;
   QColor bgcol, orgcol;
   bool drawOrigin;
   QTimer timer;
signals:

};

#endif // MONITOR_H
