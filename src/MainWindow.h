#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QLabel>
#include <QTreeWidgetItem>
#include <duktape.h>
#include <debug_proto/duk_trans_dvalue.h>
#include "dbg_core/debugger.h"
#include "dlgAbout.h"
#include "../Monitor.h"
#include "../Inspector.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class dbgController;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

public slots:
  void onActHelpAboutQt() { QApplication::aboutQt(); }
  void onActHelpAbout() { dlgAbout a(this); a.exec(); }
  void onCursorMove(int line, int column);
  void onCurrentNodeChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
  void onMoveCurrentNodeUp();
  void onMoveCurrentNodeDown();

protected slots:
  void onAddNode();
  void onDelNode();
  void onCompiled();
  void onTimer();
  void onNodeNameChanged(QString name);
  void onShowAABB(bool show) { ecs::Node::paintAABB = show; mon->update(); }
  void onShowPivot(bool show) { ecs::Node::paintPivot = show; mon->update(); }

protected:
  void closeEvent(QCloseEvent *event) override;
  void showEvent(QShowEvent *event) override;
  void loadSettings();
  void saveSettings();
  void initStatusBar();
  void initDockWidgets();
  void initDebugController();
  void initInspector();
  void initSceneTree();
  void fillSceneTree();
  void updateSceneUI();
  QTreeWidgetItem* fillSceneTree(ecs::Node *node, QTreeWidgetItem* parent);

private:
	dbgController* ctrl;
	Ui::MainWindow *ui;
  bool settingsRead = false;
  QLabel* sbCPos;
  Monitor* mon;
  Inspector* insp;
  QTreeWidgetItem* rootItem;
};

extern MainWindow* mainWnd;

#endif // MAINWINDOW_H

