#include <QApplication>
#include "dbgConsole.h"

DebugConsole::DebugConsole(const QString &text, QWidget *parent) : QPlainTextEdit(text, parent)
{
	init();
}

DebugConsole::DebugConsole(QWidget *p) : QPlainTextEdit(p)
{
	init();
}

void DebugConsole::init()
{
	promptStr = "> ";
	promptLen = promptStr.size();

	line = 1;

	welcome();
	prompt();
}

void DebugConsole::clear()
{
	setPlainText(QString());
	prompt(false);
	setFocus();
}

void DebugConsole::welcome()
{
	appendPlainText("Duktape REPL-Console V0.02 by ageman");
	setCursorWidth(8);
}

void DebugConsole::prompt(bool newLine)
{
	if (newLine)
		appendPlainText("\n"+promptStr);
	else
		appendPlainText(promptStr);

	line = document()->blockCount()-1;
	curBlock = document()->findBlockByLineNumber(line);
	startPos = curBlock.position() + promptLen;
	sanatizeCaretPos();
	curText = curBlock.text();

	mode = Mode::read;
	ensureCursorVisible();
}

void DebugConsole::histUp()
{
	if (hCur>=hSize) {
		beep();
		return;
	}
	int idx = hIdx - hCur - 1;
	if (idx < 0)
		idx += REPL_HISTORY_SIZE;
	replaceCurLineWith(history[idx]);
	hCur++;
}

void DebugConsole::histDown()
{
	if (hCur-1 < 0) {
		beep();
		return;
	}
	if (hCur == 0) {
		replaceCurLineWith(QString());
		hCur--;
		return;
	}
	int idx = hIdx - hCur + 1;
	if (idx < 0)
		idx += REPL_HISTORY_SIZE;
	replaceCurLineWith(history[idx]);
	hCur--;
}

void DebugConsole::toHistory(QString text)
{
	int idx = hSize > 0 ? hIdx-1 : hIdx;
	if (idx<0)
		idx+=REPL_HISTORY_SIZE;

	if (!text.isEmpty() && history[idx] != text) {
		history[hIdx] = text;
		hIdx++;
		if (hIdx >= REPL_HISTORY_SIZE)
			hIdx = 0;
		if (hSize < REPL_HISTORY_SIZE-1)
			hSize++;
	}
	hCur = 0;
}

void DebugConsole::replaceCurLineWith(const QString& text)
{
	QTextCursor c = textCursor();
	c.setPosition(startPos, QTextCursor::MoveMode::MoveAnchor);
	c.movePosition(QTextCursor::End, QTextCursor::MoveMode::KeepAnchor);
	c.removeSelectedText();
	c.insertText(text);
	setTextCursor(c);
}

void DebugConsole::beep()
{
	QApplication::beep();
}

int DebugConsole::caretPos()
{
	return textCursor().position();
}

void DebugConsole::sanatizeCaretPos()
{
	int pos = caretPos();
	if (pos < startPos) {
		QTextCursor c = textCursor();
		c.setPosition(startPos);
		setTextCursor(c);
		ensureCursorVisible();
	}
}

void DebugConsole::keyPressEvent(QKeyEvent* e)
{
	sanatizeCaretPos();

	switch (e->key()) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
			eval();
			return;
		case Qt::Key_Backspace:
			if (caretPos() <= startPos) {
				beep();
				return;
			}
			break;
		case Qt::Key_Up:
			histUp();
			return;
		case Qt::Key_Down:
			histDown();
			return;
		case Qt::Key_Left:
			if (caretPos() <= startPos) {
				beep();
				return;
			}
			break;
		case Qt::Key_PageDown:
		case Qt::Key_PageUp:
			beep();
			return;
		case Qt::Key_Home:
			QTextCursor c = textCursor();
			c.setPosition(startPos);
			setTextCursor(c);
			ensureCursorVisible();
			return;
	}

	QPlainTextEdit::keyPressEvent(e);
}

void DebugConsole::eval()
{
	dbgController* ctrl = &dbgController::Instance();
	if (!ctrl) {
		appendPlainText("[FATAL] debug controller not present.");
		return;
	}
	duk_context *ctx = ctrl->getCtx();
	if (!ctx) {
		appendPlainText("[FATAL] invalid debug-context.");
		return;
	}


	QString toEval = document()->findBlockByLineNumber(document()->blockCount()-1).text();
	toEval = toEval.right(toEval.size()-promptLen);

	if (!toEval.isEmpty()) {
		toHistory(toEval);

		try {
			DukValue result = dukglue_peval<DukValue>(ctx, toEval.toUtf8().data());
			switch (result.type()) {
				case DukValue::Type::UNDEFINED:
					//appendPlainText("undefined");
					break;
				case DukValue::Type::NULLREF:
					//appendPlainText("nullref");
					break;
				case DukValue::Type::BOOLEAN:
					appendPlainText(result.as_bool() ? "true" : "false");
					break;
				case DukValue::Type::NUMBER:
					appendPlainText(QString::number(result.as_double(), 'g', 15));
					break;
				case DukValue::Type::STRING:
					appendPlainText(QString::fromUtf8(result.as_c_string()));
					break;
				case DukValue::Type::OBJECT:
					appendPlainText("Object");
					break;
				case DukValue::Type::BUFFER:
					appendPlainText("buffer");
					break;
				case DukValue::Type::POINTER:
					appendPlainText("pointer");
					break;
				case DukValue::Type::LIGHTFUNC:
					appendPlainText("lightfunc");
					break;
			}
		} catch (DukErrorException *err) {
			appendPlainText("[ERROR] "+QString::fromUtf8(err->what()));
		} catch (DukException *e) {
			appendPlainText("[ERROR] "+QString::fromUtf8(e->what()));
		} catch (std::exception *e) {
			appendPlainText("[ERROR] "+QString::fromUtf8(e->what()));
		} catch (...) {
			appendPlainText("[ERROR]");
		}

		prompt();
	} else
		prompt(false);
}

