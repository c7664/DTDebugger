#ifndef AMSDOCKWIDGET_H
#define AMSDOCKWIDGET_H

#include <QDockWidget>
#include <QPaintEvent>
#include <QSizeGrip>
#include <QVBoxLayout>

class AMSDockWidget : public QDockWidget
{
public:
   AMSDockWidget(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
   AMSDockWidget(const QString &title, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());

   void adjustLayout();

protected slots:
   void onDockLocationChanged(Qt::DockWidgetArea area);

protected:
   void paintEvent(QPaintEvent* ev) override;
   void init();
   void enableSizeGrip(bool enable);

   QSizeGrip *grip = nullptr;
   QVBoxLayout *vlayout = nullptr;
   QWidget *proxy = nullptr;
};

#endif // AMSDOCKWIDGET_H
