#include <QPainter>
#include <QFontMetrics>
#include <QLayout>

#include "AMSDockWidget.h"

AMSDockWidget::AMSDockWidget(QWidget *parent, Qt::WindowFlags flags) : QDockWidget(parent, flags)
{
   init();
}

AMSDockWidget::AMSDockWidget(const QString &title, QWidget *parent, Qt::WindowFlags flags) : QDockWidget(title, parent, flags)
{
   init();
}




void AMSDockWidget::init()
{
   connect(this, &AMSDockWidget::dockLocationChanged, this, &AMSDockWidget::onDockLocationChanged);
}

void AMSDockWidget::adjustLayout()
{
   // This hack is neccessary because there is a problem as soon as a dockwidget
   // switches to the floating state. When that happens the user has almost no
   // way of resizing that widget anymore because the mousecursor has to be at
   // exactly one specific pixel around the border, otherwise the mousecursor
   // switches back to a normal arrow immediatly.
   // This, of course, is quite tedious. The only "solution" so far is to add
   // a QSizeGrip to bottom of the widget. That doesn't really help with the
   // extremely thin resizing area around the border, but at least now there is
   // an approx. 20x20 pixel area in the lower right corner of the dock-widget
   // that reacts to resizing-attempts.
   if (proxy)
      return;
   QWidget *tmp = widget();      // current content-widget of the dock-widget
   proxy = new QWidget(this);    // this will be the "proxy"-widget with a new QVBoxLayout
   vlayout = new QVBoxLayout();  //
   vlayout->setContentsMargins(0, 0, 0, 0);
   vlayout->setSpacing(0);
   layout()->removeWidget(tmp);  // remove old content
   setWidget(proxy);             // install proxy widget

   proxy->setLayout(vlayout);    // install new layout
   vlayout->addWidget(tmp);      // reinstall old content
   // after this rewiring it's possible to install a QSizeGrip at the bottom
   // of the new QVBoxLayout (see AMSDockWidget::enableSizeGrip(...))
}

void AMSDockWidget::enableSizeGrip(bool enable)
{
   if (!vlayout)
      adjustLayout();
   if (enable) {
      if (grip)
         return;
      grip = new QSizeGrip(this);
      vlayout->addWidget(grip);
      vlayout->setAlignment(grip, Qt::AlignBottom | Qt::AlignRight);
   } else {
      if (!grip)
         return;
      vlayout->removeWidget(grip);
      delete grip;
      grip = nullptr;
   }
}

void AMSDockWidget::onDockLocationChanged(Qt::DockWidgetArea area)
{
   Q_UNUSED(area)

   enableSizeGrip(area == 0); // ..when we're floating
}





void AMSDockWidget::paintEvent(QPaintEvent *ev)
{
   QPainter p(this);
   QFontMetrics fm(font());
   int h = fm.height()+2;
   QRect r = ev->rect();
   r.setHeight(h);
   p.setPen(Qt::NoPen);
   p.setBrush(palette().color(QPalette::Dark));
   p.drawRect(r);

   p.setPen(palette().color(QPalette::Midlight));
   p.setBrush(Qt::NoBrush);
   r = rect();
   r.setWidth(r.width()-1);
   r.setHeight(r.height()-1);
   p.drawRect(r);

   p.setPen(palette().color(QPalette::WindowText));
   p.drawText(QPointF(r.x()+10, fm.capHeight()+(h-fm.capHeight())*0.5), windowTitle());

}

