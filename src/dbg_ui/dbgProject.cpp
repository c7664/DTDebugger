#include <QFileInfo>
#include <QFileDialog>
#include "dbgProject.h"

dbgProject::dbgProject(QObject *parent) :
   QObject(parent)
{

}

void dbgProject::setFilePath(const std::string &path)
{
   filePath = path;
}

std::string dbgProject::getName() const
{
   if (filePath.empty())
      return "untitled";
   QFileInfo fi(filePath.c_str());
   return fi.fileName().toStdString();
}



//////////////////////////////////////////////////////////////////////////////
// entries
//////////////////////////////////////////////////////////////////////////////

int dbgProject::addFile(const std::string &path)
{
   for (int i=0; i<(int)files.size(); i++)
      if (files[i].filePath == path)
         return i;
   dbgProjectEntry e;
   e.filePath = path;
   files.push_back(e);
   return files.size()-1;
}

bool dbgProject::delFile(const std::string &path)
{
   return delFile(indexOf(path));
}

bool dbgProject::delFile(int index)
{
   if (index>=0 && index<(int)files.size()){
      files.erase(files.begin() + index);
      return true;
   }
   return false;
}

int dbgProject::indexOf(const std::string &path)
{
   for (int i=0; i<(int)files.size(); i++)
      if (files[i].filePath == path)
         return i;
   return -1;
}

dbgProjectEntry *dbgProject::getEntry(int index)
{
   if (index>=0 && index<(int)files.size()){
      return &files[index];
   }
   return nullptr;
}

void dbgProject::clearProject()
{
   files.clear();
   filePath.clear();
   clearVars();
   clearEditFiles();
}


//////////////////////////////////////////////////////////////////////////////
// vars
//////////////////////////////////////////////////////////////////////////////

int dbgProject::addVar(const std::string &name)
{
   vars.push_back(name);
   return (int)vars.size()-1;
}

bool dbgProject::delVar(const std::string &name)
{
   return delVar(indexOfVar(name));
}

bool dbgProject::delVar(int index)
{
   if (index>=0 && index<numVars()) {
      vars.erase(vars.begin() + index);
      return true;
   }
   return false;
}

int dbgProject::indexOfVar(const std::string &name)
{
   for (int i = 0; i < numVars(); i++)
      if (vars[i] == name)
         return i;
   return -1;
}

std::string dbgProject::getVar(int index)
{
   if (index >= 0 && index < numVars())
      return vars.at(index);
   return std::string();
}

void dbgProject::clearVars()
{
   vars.clear();
}




//////////////////////////////////////////////////////////////////////////////
// edit files
//////////////////////////////////////////////////////////////////////////////

int dbgProject::addEditFile(const std::string &path)
{
   editFiles.push_back(path);
   return (int)editFiles.size()-1;
}

bool dbgProject::delEditFile(const std::string &path)
{
   return delEditFile(indexOfEditFile(path));
}

bool dbgProject::delEditFile(int index)
{
   if (index>=0 && index<numEditFiles()) {
      editFiles.erase(editFiles.begin() + index);
      return true;
   }
   return false;
}

int dbgProject::indexOfEditFile(const std::string &path) const
{
   for (int i=0; i<numEditFiles(); i++) {
      if (editFiles[i] == path)
         return i;
   }
   return -1;
}

std::string dbgProject::getEditFile(int index)
{
   if (index>=0 && index<numEditFiles())
      return editFiles[index];
   return std::string();
}

void dbgProject::clearEditFiles()
{
   editFiles.clear();
}





void dbgProject::toDataStream(QDataStream &st)
{
   st << QString("Scripts");
   // entries
   st << qint32(files.size());
   for (int i = 0; i < (int)files.size(); i++) {
      QString p = QString::fromStdString(files[i].filePath);
      st << QString(p);
      st << qint32(files[i].breakpoints.size());
      for (int j = 0; j < (int)files[i].breakpoints.size(); j++) {
         QString fp = QString::fromStdString(files[i].breakpoints[j].file);
         st << QString(fp);
         st << qint32(files[i].breakpoints[j].line);
         st << qint32(files[i].breakpoints[j].active);
         st << qint32(files[i].breakpoints[j].stopCount);
      }
   }

   // vars
   st << qint32(numVars());
   for (int i=0; i<numVars(); i++) {
      QString p = QString::fromStdString(vars[i]);
      st << p;
   }

   // open files
   st << qint32(numEditFiles());
   for (int i=0; i<numEditFiles(); i++) {
      QString p = QString::fromStdString(editFiles[i]);
      st << p;
   }

   //
   st << qint32(curEditor);
}

void dbgProject::fromDataStream(QDataStream &st)
{
   QString mg;
   st >> mg;
   if (mg != "Scripts")
      throw "Error in project file.";
   qint32 n, m;
   dbgProjectEntry e;
   clearProject();
   // entries
   st >> n;
   for (int i = 0; i < n; i++) {
      QString p;
      st >> p;
      e.filePath = p.toStdString();
      st >> m;
      for (int j = 0; j < m; j++) {
         Breakpoint bp;
         QString fp;
         st >> fp;
         bp.file = fp.toStdString();
         qint32 t;
         st >> t;
         bp.line = t;
         st >> t;
         bp.active = (bool)t;
         st >> t;
         bp.stopCount = t;
         e.breakpoints.push_back(bp);
      }
      files.push_back(e);
   }

   // vars
   st >> n;
   for (int i = 0; i < n; i++) {
      QString p;
      st >> p;
      vars.emplace_back(p.toStdString());
   }

   // edit files
   st >> n;
   for (int i = 0; i < n; i++) {
      QString p;
      st >> p;
      editFiles.emplace_back(p.toStdString());
   }

   //
   st >> curEditor;
}

bool dbgProject::openFile()
{
   if (filePath.empty()) {
      QString filter = tr("Project files | (*.");
      filter += DTD_EXTENSION;
      filter += ")";
      QFileDialog fd(nullptr, tr("Open project file"), QString(), filter);
      fd.setAcceptMode(QFileDialog::AcceptOpen);
      if (fd.exec()) {
         QString fn = fd.selectedFiles().at(0);
         filePath = fn.toStdString();
      } else
         return false;
   }
   QFile file(QString::fromStdString(filePath));
   if (!file.exists()) {
      filePath.clear();
      return false;
   }
   if (!file.open(QIODevice::ReadOnly)) {
      filePath.clear();
      return false;
   }
   QDataStream in(&file);
   try {
      fromDataStream(in);
   }  catch (...) {
      filePath.clear();
      return false;
   }
   file.close();
   return true;
}

bool dbgProject::saveFile()
{
   if (filePath.empty()) {
      QString filter = tr("Project files (*.");
      filter += DTD_EXTENSION;
      filter += ")";
      QFileDialog fd(nullptr, tr("Save project file"), QString(), filter);
      fd.setAcceptMode(QFileDialog::AcceptSave);
      fd.setDefaultSuffix(DTD_EXTENSION);
      if (fd.exec()) {
         QString fn = fd.selectedFiles().at(0);
         return saveAsFile(fn.toUtf8().data());
      } else
         return false;
   }
   return saveAsFile(filePath.c_str());
}

bool dbgProject::saveAsFile(const char *path)
{
   QFile file(path);
   if (!file.open(QIODevice::WriteOnly))
      return false;
   QDataStream out(&file);   // we will serialize the data into the file
   toDataStream(out);
   filePath = path;
   return true;
}


