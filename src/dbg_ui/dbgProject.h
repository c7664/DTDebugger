#ifndef DBGPROJECT_H
#define DBGPROJECT_H

#include <QObject>
#include <QDataStream>
#include <vector>
#include "../dbg_core/debugger.h"

#define DTD_EXTENSION "dtd"

struct dbgProjectEntry {
   std::string filePath;
   std::vector<Breakpoint> breakpoints;
};

class dbgProject : public QObject
{
   Q_OBJECT
public:
   explicit dbgProject(QObject *parent = nullptr);
   void setFilePath(const std::string &path);
   std::string getName() const;
   std::string getFilePath() const { return filePath; }
   bool isEmpty() const { return filePath.empty(); }

   // file access
   bool openFile();
   bool saveFile();
   bool saveAsFile(const char* filePath);
   void clearProject();

   void toDataStream(QDataStream &st);
   void fromDataStream(QDataStream &st);

   // entries
   int addFile(const std::string &filepath);
   bool delFile(const std::string &filepath);
   bool delFile(int index);
   int indexOf(const std::string &filepath);
   dbgProjectEntry* getEntry(int index);
   int numEntries() const { return (int)files.size(); }

   // variables
   int addVar(const std::string& name);
   bool delVar(const std::string& name);
   bool delVar(int index);
   int indexOfVar(const std::string& name);
   int numVars() const { return (int)vars.size(); }
   std::string getVar(int index);
   void clearVars();

   // open files (in editor)
   int addEditFile(const std::string& path);
   bool delEditFile(const std::string& path);
   bool delEditFile(int index);
   int indexOfEditFile(const std::string& path) const;
   int numEditFiles() const { return (int)editFiles.size(); }
   std::string getEditFile(int index);
   void clearEditFiles();

   // currently open editor
   void setCurEditor(int num) { curEditor = num; }
   int getCurEditor() const { return curEditor; }

signals:

private:
   std::string filePath;
   std::vector<dbgProjectEntry> files;
   std::vector<std::string> vars;
   std::vector<std::string> editFiles; // in editor
   int curEditor;
};

#endif // DBGPROJECT_H
