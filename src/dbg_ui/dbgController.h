#ifndef DBGCONTROLLER_H
#define DBGCONTROLLER_H

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include <QTabWidget>
#include <QAction>
#include <QTextBrowser>
#include <QAbstractButton>

#include <QSpinBox>
#include <vector>
#include "JSEdit/jsedit.h"
#include "dbgProject.h"
#include "debug_proto/duk_trans_dvalue.h"
#include "../dbg_core/debugger.h"
#include "dbgProject.h"


class Debugger;


struct dbgControllerActions {
	QAbstractButton* btnAddBP;
	QAbstractButton* btnDelBP;
	QAbstractButton* btnClearBP;
	QAbstractButton* btnActiveBP;
	QAbstractButton* btnHitcountBP;
	QSpinBox*        edtHitCount;
	QAbstractButton* btnAddVar;
	QAbstractButton* btnDelVar;
	QAbstractButton* btnClearVars;
	QAbstractButton* btnAddToProject;
	QAbstractButton* btnDelFromProject;

	QAction* actDbgCompile;
	QAction* actDbgStart;
	QAction* actDbgStop;
	QAction* actDbgPause;
	QAction* actDbgOut;
	QAction* actDbgIn;
	QAction* actDbgOver;

	QAction* actDbgNewScript;
	QAction* actDbgOpenScript;
	QAction* actDbgSaveScript;
	QAction* actDbgSaveAsScript;
	QAction* actDbgCloseScript;

	QAction* actDbgNewProject;
	QAction* actDbgOpenProject;
	QAction* actDbgSaveProject;
	QAction* actDbgSaveAsProject;
	QAction* actDbgCloseProject;
};

class dbgController : public QObject
{
	Q_OBJECT
public:
	static dbgController &Instance() {
		static dbgController inst(nullptr);
		return inst;
	}

	// UI-setup
	void setupWidgets(QTabWidget* editorsParent,
							QTableWidget* Breakpoints,
							QTableWidget* Variables,
							QTableWidget* Callstack,
							QTableWidget* Project,
							QTextBrowser* Output);
	void setupActions(dbgControllerActions* actions);

	// Settings
	void setFullPathInCallstack(bool fullPath);
	void setFullPathInBreakpoints(bool fullPath);

	bool getFullPathInCallstack() const { return fullPathInCallstack; }
	bool getFullPathInBreakpoints() const { return fullPathInBreakpoints; }

	// editors
	JSEdit* newEditor(const QString &filePath = "");
	bool delEditor(JSEdit *ed);
	bool delEditor(int index);
	int numEditors() const { return eds.size(); }
	int indexOf(JSEdit* ed);
	JSEdit* getEditor(int index);
	bool clearEditors(bool forceClose = false);
	QString getCurFilePath() const;
	QString getCurFileName() const;
	int getCurLine() const;
	int getCurColumn() const;

	// helpers
	QString getWindowTitle();

	// execution
	void callFunction(const QString& func);

	duk_context *getCtx() { return ctx; }

public slots:
	// debug transport
	void onActDbgStart();
	void onActDbgPause();
	void onActDbgStop();
	void onActDbgOver();
	void onActDbgIn();
	void onActDbgOut();

	// variables
	void onActAddVar();
	void onActDelVar();
	void onActClearVars();

	// breakpoints
	void onActAddBreakpoint();
	void onActDelBreakpoint();
	void onActClearBreakpoints();
	void onActActivateBreakpoint(bool activate);
	void onActActivateHitcount(bool activate);
	void onActChangeHitcount();

	// editor
	void gotoLine(const QString& filePath, int line);

	// menu support
	void onActNewScript();
	void onActOpenSript();
	void onActSaveScript();
	void onActSaveAsScript();
	void onActCloseScript();

	// project
	void onActNewProject();
	void onActOpenProject();
	void onActSaveProject();
	void onActSaveAsProject();
	void onActCloseProject();

	void onAddCurFileToProject();
	void onDelSelectedFilesFromProject();
	void compileProject();

	void debugPrint(const char* msg, dbgLogLevel level);
	void debugOutput(const QString& status, const QString& msg, bool first, bool newLine);

signals:
	void cursorMoved(int line, int column);
	void compiled();

protected:
	void debugBreak(duk_trans_dvalue_ctx* context);
	void debugData(dbgDataType t);
	void debugIdle();
	void debugFatal(const char* msg);
	void addBreakpoint(const QString& filePath, int line, bool active = true, int hitCount = 0);
	void delBreakpoint(const QString& filePath, int line);
	void clearBreakpoits();
	void activateBreakpoint(const QString& filePath, int line, bool activate);
	void changeHitCount(const QString& filePath, int line, int count);
	void updateBreakpointsUI();
	void fillCallstack();
	void fillVariables();
	void fillBreakpoints();
	void fillProject();
	bool preCompile();
	void projectModified(bool mod);

protected slots:
	void onTabChanged(int index);
	void onTabCloseRequested(int index);
	void onModificationChanged(bool changed);
	void onCursorMove();
	void onBreakpointMoved(const QString &filePath, int index, int fromLine, int toLine);
	void onBreakpointChanged(const QString &filePath, int line, JSEdit::BreakpointType type, bool deleted);
	void onBPItemSelectionChanged();
	void onBPCellDoubleClicked(int row, int column);
	void onStackCellDoubleClicked(int row, int column);
	void onProjectCellDoubleClicked(int row, int column);
	void onVarsItemSelectionChanged();


private:
	dbgController(QObject *parent = nullptr);
	~dbgController();

	// C-callbacks (defined in dbgController) need access to this class:
	friend void dbgBreakCallback(duk_trans_dvalue_ctx *ctx);
	friend void dbgDataCallback(dbgDataType t);
	friend void dbgIdleCallback();
	friend void dbgLogCallback(const char* msg, dbgLogLevel level);
	friend void my_fatal(void *udata, const char *msg) ;

	void createContext();
	void freeContext();

	QString fileContent(const char* path);
	void storeBreakpointsInProject();
	void projectBreakpointsToDebugger();
	void updateUI();

	// singletons
	Debugger* deb;
	dbgProject prj;
	duk_context *ctx;

	// settings
	bool fullPathInCallstack = false;
	bool fullPathInBreakpoints = false;

	// editors
	std::vector<JSEdit*> eds;
	JSEdit* ed = nullptr; // currently visible editor
	JSEdit* findEditorByFileName(const char* fileName);
	JSEdit* findEditorByFilePath(const char* filePath);

	// UI-Elements
	dbgControllerActions* act = nullptr;
	QTabWidget* editParent = nullptr;
	QTextBrowser* edOut = nullptr; // output pane
	QTableWidget* tblBP = nullptr; // breakpoints
	QTableWidget* tblVars = nullptr; // variables
	QTableWidget* tblStack = nullptr; // callstack
	QTableWidget* tblProject = nullptr;
	QColor logCol[6];

	// data
	bool needsCompilation = false;
};

#endif // DBGCONTROLLER_H
