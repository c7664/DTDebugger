#include <QHeaderView>
#include <QApplication>
#include <QGuiApplication>
#include <QThread>
#include <QInputDialog>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include "dbgController.h"
#include "../dbg_core/debugger.h"
#include "../MainWindow.h"

// C-Callback prototypes to be registered in the debugger:
void dbgBreakCallback(duk_trans_dvalue_ctx *ctx)
{
	dbgController::Instance().debugBreak(ctx);
}

void dbgDataCallback(dbgDataType t)
{
	dbgController::Instance().debugData(t);
}

void dbgIdleCallback()
{
	dbgController::Instance().debugIdle();
}

void dbgLogCallback(const char* msg, dbgLogLevel level)
{
	dbgController::Instance().debugPrint(msg, level);
}




//////////////////////////////////////////////////////////////////////////////
// dbgController
//////////////////////////////////////////////////////////////////////////////

dbgController::dbgController(QObject *parent) : QObject(parent)
{
	deb = &Debugger::Instance();

	// setup debugger-callbacks:
	deb->setBreakCallback(dbgBreakCallback);
	deb->setDataCallback(dbgDataCallback);
	deb->setIdleCallback(dbgIdleCallback);
	deb->setLogCallback(dbgLogCallback);

	// debug-log-colors:
	logCol[llInternal] = QColor(96, 96, 255, 255);
	logCol[llScript]   = QColor(64, 255, 64, 255);
	logCol[llInfo]     = QColor(192, 192, 192, 255);
	logCol[llWarning]  = QColor(192, 192, 0, 255);
	logCol[llError]    = QColor(255, 64, 64, 255);
	logCol[llFatal]    = QColor(128, 0, 0, 255);
}

dbgController::~dbgController() {
	deb->setBreakCallback(nullptr);
	deb->setDataCallback(nullptr);
	deb->setIdleCallback(nullptr);
	deb->setLogCallback(nullptr);

	delete act;

	// destroy Duktape contex
	freeContext();
}


//////////////////////////////////////////////////////////////////////////////
// UI-setup
//////////////////////////////////////////////////////////////////////////////

void dbgController::setupWidgets(
		QTabWidget* editorsParent,
		QTableWidget *Breakpoints,
		QTableWidget *Variables,
		QTableWidget *Callstack,
		QTableWidget* Project,
		QTextBrowser* Output)
{
	editParent = editorsParent;
	edOut = Output;
	tblBP = Breakpoints;
	tblVars = Variables;
	tblStack = Callstack;
	tblProject = Project;

	if (editParent) {
		editParent->setTabsClosable(true);
		connect(editParent, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int)));
		connect(editParent, SIGNAL(tabCloseRequested(int)), this, SLOT(onTabCloseRequested(int)));
		editParent->setMovable(true);
	}
	if (tblBP) {
		tblBP->setColumnCount(5);
		tblBP->setRowCount(0);
		tblBP->setHorizontalHeaderLabels(QStringList() << tr("Num") << tr("State") << tr("File") << tr("Line") << tr("Hit count"));
		tblBP->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
		connect(tblBP, &QTableWidget::itemSelectionChanged, this, &dbgController::onBPItemSelectionChanged);
		connect(tblBP, &QTableWidget::cellDoubleClicked, this, &dbgController::onBPCellDoubleClicked);
	}
	if (tblStack) {
		tblStack->setColumnCount(4);
		tblStack->setRowCount(0);
		tblStack->setHorizontalHeaderLabels(QStringList() << tr("Num") << tr("File") << tr("Line") << tr("Function"));
		tblStack->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
		connect(tblStack, &QTableWidget::cellDoubleClicked, this, &dbgController::onStackCellDoubleClicked);
	}
	if (tblVars) {
		tblVars->setColumnCount(2);
		tblVars->setRowCount(0);
		tblVars->setHorizontalHeaderLabels(QStringList() << tr("Name") << tr("Value") );
		tblVars->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
		connect(tblVars, &QTableWidget::itemSelectionChanged, this, &dbgController::onVarsItemSelectionChanged);
	}
	if (tblProject) {
		tblProject->setColumnCount(2);
		tblProject->setRowCount(0);
		tblProject->setHorizontalHeaderLabels(QStringList() << tr("File") << tr("Path") );
		tblProject->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
		connect(tblProject, &QTableWidget::cellDoubleClicked, this, &dbgController::onProjectCellDoubleClicked);
		connect(tblProject, &QTableWidget::itemSelectionChanged, this, &dbgController::updateUI);
	}
}

void dbgController::setupActions(dbgControllerActions *actions)
{
	act = actions;

	connect(act->btnAddBP, &QAbstractButton::pressed, this, &dbgController::onActAddBreakpoint);
	connect(act->btnDelBP, &QAbstractButton::pressed, this, &dbgController::onActDelBreakpoint);
	connect(act->btnClearBP, &QAbstractButton::pressed, this, &dbgController::onActClearBreakpoints);
	connect(act->btnActiveBP, &QAbstractButton::clicked, this, &dbgController::onActActivateBreakpoint);
	connect(act->btnHitcountBP, &QAbstractButton::clicked, this, &dbgController::onActActivateHitcount);
	connect(act->edtHitCount, &QSpinBox::editingFinished, this, &dbgController::onActChangeHitcount);

	connect(act->btnAddVar, &QAbstractButton::pressed, this, &dbgController::onActAddVar);
	connect(act->btnDelVar, &QAbstractButton::pressed, this, &dbgController::onActDelVar);
	connect(act->btnClearVars, &QAbstractButton::pressed, this, &dbgController::onActClearVars);

	connect(act->btnAddToProject, &QAbstractButton::pressed, this, &dbgController::onAddCurFileToProject);
	connect(act->btnDelFromProject, &QAbstractButton::pressed, this, &dbgController::onDelSelectedFilesFromProject);

	connect(act->actDbgCompile, &QAction::triggered, this, &dbgController::compileProject);
	connect(act->actDbgStart, &QAction::triggered, this, &dbgController::onActDbgStart);
	connect(act->actDbgStop, &QAction::triggered, this, &dbgController::onActDbgStop);
	connect(act->actDbgPause, &QAction::triggered, this, &dbgController::onActDbgPause);
	connect(act->actDbgOver, &QAction::triggered, this, &dbgController::onActDbgOver);
	connect(act->actDbgIn, &QAction::triggered, this, &dbgController::onActDbgIn);
	connect(act->actDbgOut, &QAction::triggered, this, &dbgController::onActDbgOut);

	connect(act->actDbgNewScript, &QAction::triggered, this, &dbgController::onActNewScript);
	connect(act->actDbgOpenScript, &QAction::triggered, this, &dbgController::onActOpenSript);
	connect(act->actDbgSaveScript, &QAction::triggered, this, &dbgController::onActSaveScript);
	connect(act->actDbgSaveAsScript, &QAction::triggered, this, &dbgController::onActSaveAsScript);
	connect(act->actDbgCloseScript, &QAction::triggered, this, &dbgController::onActCloseScript);

	connect(act->actDbgNewProject, &QAction::triggered, this, &dbgController::onActNewProject);
	connect(act->actDbgOpenProject, &QAction::triggered, this, &dbgController::onActOpenProject);
	connect(act->actDbgSaveProject, &QAction::triggered, this, &dbgController::onActSaveProject);
	connect(act->actDbgSaveAsProject, &QAction::triggered, this, &dbgController::onActSaveAsProject);
	connect(act->actDbgCloseProject, &QAction::triggered, this, &dbgController::onActCloseProject);

	updateBreakpointsUI();
	updateUI();
}



//////////////////////////////////////////////////////////////////////////////
// settings
//////////////////////////////////////////////////////////////////////////////

void dbgController::setFullPathInCallstack(bool fullPath)
{
	if (fullPath != fullPathInCallstack) {
		fullPathInCallstack = fullPath;
		fillCallstack();
	}
}

void dbgController::setFullPathInBreakpoints(bool fullPath)
{
	if (fullPath != fullPathInBreakpoints) {
		fullPathInBreakpoints = fullPath;
		fillBreakpoints();
	}
}




//////////////////////////////////////////////////////////////////////////////
// editors
//////////////////////////////////////////////////////////////////////////////

JSEdit *dbgController::newEditor(const QString &filePath)
{
	JSEdit* ne = new JSEdit();

	// some defaults
	ne->setTabStopDistance(24.0);
	ne->setWordWrapMode(QTextOption::NoWrap);

	// dark color scheme
	ne->setColor(JSEdit::Background,    QColor("#0C152B"));
	ne->setColor(JSEdit::Sidebar   ,    QColor("#000020"));
	ne->setColor(JSEdit::LineNumber,    QColor("#909090"));
	ne->setColor(JSEdit::Cursor,        QColor("#0A1020"));
	ne->setColor(JSEdit::Normal,        QColor("#FFFFFF"));
	ne->setColor(JSEdit::Comment,       QColor("#666666"));
	ne->setColor(JSEdit::Number,        QColor("#DBF76C"));
	ne->setColor(JSEdit::String,        QColor("#5ED363"));
	ne->setColor(JSEdit::Operator,      QColor("#FF7729"));
	ne->setColor(JSEdit::Identifier,    QColor("#8080ff"));
	ne->setColor(JSEdit::Keyword,       QColor("#FDE15D"));
	ne->setColor(JSEdit::BuiltIn,       QColor("#9CB6D4"));
	ne->setColor(JSEdit::Marker,        QColor("#DBF76C"));
	ne->setColor(JSEdit::BracketMatch,  QColor("#1AB0A6"));
	ne->setColor(JSEdit::BracketError,  QColor("#A82224"));
	ne->setColor(JSEdit::FoldIndicator, QColor("#555555"));

	// missing keywords:
	QStringList keywords = ne->keywords();
	keywords << "const";
	keywords << "let";
	ne->setKeywords(keywords);

	// open file
	if (!filePath.isEmpty()) {
		std::string comp = filePath.toStdString();
		if (!ne->openFile(filePath)) {

			QMessageBox::warning(nullptr, tr("An error occurred"), tr("File '%1' not found.").arg(filePath), QMessageBox::Ok);

			delete ne;
			return nullptr;
		}
		for (int i = 0; i < deb->numBreakpoints(); i++) {
			Breakpoint &bp = deb->getBreakpoint(i);
			if (bp.file == comp) {
				if (bp.active)
					ne->setBreakpoint(bp.line, JSEdit::BreakpointType::active);
				else
					ne->setBreakpoint(bp.line, JSEdit::BreakpointType::inactive);
			}
		}
	}

	// open new tab
	if (editParent) {
		editParent->addTab(ne, ne->getFileName());
		editParent->setCurrentWidget(ne);
	}

	// add editor to list
	eds.push_back(ne);

	// wire editor signals
	connect(ne, &JSEdit::breakpointChanged, this, &dbgController::onBreakpointChanged);
	connect(ne, &JSEdit::breakpointMoved, this, &dbgController::onBreakpointMoved);
	connect(ne, &JSEdit::cursorPositionChanged, this, &dbgController::onCursorMove);
	connect(ne, &JSEdit::modificationChanged, this, &dbgController::onModificationChanged);
	ne->document()->setModified(false);
	return ne;
}

bool dbgController::delEditor(JSEdit *ed)
{
	return delEditor(indexOf(ed));
}

bool dbgController::delEditor(int index)
{
	if (index<0 || index>=(int)eds.size())
		return false;

	if (eds[index]->closeIfPossible()) {
		delete eds[index];
		eds.erase(eds.begin() + index);
		return true;
	}
	return false;
}

int dbgController::indexOf(JSEdit *ed)
{
	for (int i=0; i<(int)eds.size(); i++)
		if (eds[i]==ed)
			return i;
	return -1;
}

JSEdit *dbgController::getEditor(int index)
{
	if (index>=0 && index<(int)eds.size())
		return eds[index];
	return nullptr;
}

bool dbgController::clearEditors(bool forceClose)
{
	bool res = true;
	if (forceClose) {
		for (int i=0; i < (int)eds.size(); i++) {
			eds[i]->closeIfPossible();
			delete eds[i]; // just kill it
		}
		eds.clear();
		return true;
	}
	for (int i=0; i < (int)eds.size(); i++) {
		if (!eds[i]->closeIfPossible())
			res = false;
		else
			delete eds[i];
	}
	return res;
}

QString dbgController::getCurFilePath() const
{
	if (ed)
		return ed->getFilePath();
	else
		return QString();
}

QString dbgController::getCurFileName() const
{
	if (ed)
		return ed->getFileName();
	else
		return QString();
}

int dbgController::getCurLine() const
{
	if (ed)
		return ed->curLine();
	else
		return -1;
}

int dbgController::getCurColumn() const
{
	if (ed)
		return ed->curColumn();
	else
		return -1;
}

JSEdit *dbgController::findEditorByFileName(const char *fileName)
{
	for (int i= 0; i < editParent->count(); i++) {
		JSEdit* res = (JSEdit*)editParent->widget(i);
		if (res) {
			if (res->getFileName() == fileName)
				return res;
		}
	}
	return nullptr;
}

JSEdit *dbgController::findEditorByFilePath(const char *path)
{
	for (int i= 0; i < editParent->count(); i++) {
		JSEdit* res = (JSEdit*)editParent->widget(i);
		if (res) {
			if (res->getFilePath() == path)
				return res;
		}
	}
	return nullptr;
}

void dbgController::gotoLine(const QString &path, int line)
{
	JSEdit* e = findEditorByFilePath(path.toUtf8().data());
	if (e) {
		editParent->setCurrentWidget(e);
		e->gotoLineColumn(line);
	} else {
		newEditor(path);
	}
}



//////////////////////////////////////////////////////////////////////////////
// editor callbacks
//////////////////////////////////////////////////////////////////////////////

QString dbgController::getWindowTitle()
{
	QString p = "[" + QString::fromStdString(prj.getName());
	if (needsCompilation)
		p += "*] - ";
	else
		p += "] - ";
	JSEdit* e = (JSEdit*)editParent->currentWidget();
	if (e) {
		if (e->document()->isModified())
			p += e->getFileName() + "* - ";
		else
			p += e->getFileName() + " - ";
	}
	p += QGuiApplication::instance()->applicationName();
	return p;
}

void dbgController::onTabChanged(int index)
{
	Q_UNUSED(index) // don't need it
	ed = (JSEdit*)editParent->currentWidget();
	updateUI();
}

void dbgController::onTabCloseRequested(int index)
{
	JSEdit* e = (JSEdit*)editParent->widget(index);
	if (e)
		if (e->closeIfPossible()) {
			editParent->removeTab(editParent->currentIndex());
		}
	updateUI();
}

void dbgController::onModificationChanged(bool mod)
{
	JSEdit* e = (JSEdit*)sender();
	if (!e)
		return;
	QString t = e->getFileName();
	if (mod)
		t += "*";
	editParent->setTabText(editParent->currentIndex(), t);

	bool prjMod = false;
	for (int i=0; i<editParent->count(); i++) {
		if (((JSEdit*)editParent->widget(i))->document()->isModified()) {
			prjMod = true;
			break;
		}
	}
	if (prjMod && !needsCompilation) {
		needsCompilation = true;
		projectModified(true);
	} else if (!prjMod && needsCompilation) {
		needsCompilation = false;
		projectModified(false);
	}
}

void dbgController::projectModified(bool mod)
{
	Q_UNUSED(mod)
	mainWnd->setWindowTitle(getWindowTitle());
	updateUI();
}




void dbgController::onCursorMove()
{
	JSEdit* ed = (JSEdit*)QObject::sender();
	if (!ed)
		return;
	int line = ed->curLine();
	int column = ed->curColumn();
	emit cursorMoved(line, column);
}

// event coming from editor:
void dbgController::onBreakpointMoved(const QString &filePath, int index, int fromLine, int toLine)
{
	Q_UNUSED(index) // we'll get our own index, just to be sure...
	int idx = deb->findBreakpoint(filePath.toUtf8().data(), fromLine);
	if (idx<0) {
		debugPrint("Editor moved a breakpoint that doesn't exist.", llWarning);
		return;
	}
	Breakpoint copy = deb->getBreakpoint(idx);
	deb->delBreakpoint(idx);
	idx = deb->addBreakpoint(copy.file.c_str(), toLine);
	deb->getBreakpoint(idx).stopCount = copy.stopCount;
	deb->getBreakpoint(idx).count = copy.count;
}

// event coming from editor:
void dbgController::onBreakpointChanged(const QString &filePath, int line, JSEdit::BreakpointType type, bool deleted)
{
	int idx = deb->findBreakpoint(filePath.toUtf8().data(), line);
	if (idx < 0) { // new breakpoint
		idx = deb->addBreakpoint(filePath.toUtf8().data(), line);
		deb->activateBreakpoint(idx, type == JSEdit::BreakpointType::active);
	} else {
		if (deleted)
			deb->delBreakpoint(idx);
		else
			deb->activateBreakpoint(idx, type == JSEdit::BreakpointType::active);
	}
	fillBreakpoints();
}



//////////////////////////////////////////////////////////////////////////////
// Debugger transport
//////////////////////////////////////////////////////////////////////////////

void dbgController::onActDbgStart()
{
	if (deb->isPaused()) {
		// handle this like a 'Resume'-action
		if (act) {
			act->actDbgOver->setEnabled(false);
			act->actDbgIn->setEnabled(false);
			act->actDbgOut->setEnabled(false);
			act->actDbgPause->setEnabled(true);
			act->actDbgStart->setEnabled(false);
			act->actDbgStop->setEnabled(true);
		}
		tblStack->clearContents();
		deb->resume();
		return;
	}
	if (act) {
		act->actDbgOver->setEnabled(false);
		act->actDbgIn->setEnabled(false);
		act->actDbgOut->setEnabled(false);
		act->actDbgPause->setEnabled(true);
		act->actDbgStart->setEnabled(false);
		act->actDbgStop->setEnabled(true);
	}

//   if (needsCompilation)
		compileProject();
		if (needsCompilation)
			goto quit;

	// --- attach & arm debugger ---
	deb->attach(ctx);
	deb->resume();

	// --- call function ---
	// find "myLoop" (will arrive at TOS):
	if (!duk_get_global_string(ctx, "myLoop")) {
		std::string err = "Runtime error: function 'myLoop' not found";
		debugPrint(err.c_str(), llError);
		debugPrint(duk_safe_to_string(ctx, -1), llError);
		deb->detach();
		return;;
	}
	// perform the actual function-call:
	try {
		if (duk_pcall(ctx, 0)) {
			std::string err = "Runtime error in function 'myLoop':";
			debugPrint(err.c_str(), llError);
			debugPrint(duk_safe_to_string(ctx, -1), llError);
			deb->detach();
			return;;
		}

	}  catch (const char* msg) {
		debugPrint(msg, llFatal);
	}

	// discard function-result:
	duk_pop(ctx);

	deb->detach();

quit:
	act->actDbgOver->setEnabled(false);
	act->actDbgIn->setEnabled(false);
	act->actDbgOut->setEnabled(false);
	act->actDbgPause->setEnabled(false);
	act->actDbgStart->setEnabled(true);
	act->actDbgStop->setEnabled(false);
	tblStack->clearContents();
	tblVars->clearContents();
	ed->setExecLine(-1);

	freeContext();
	return;
}

void dbgController::onActDbgPause()
{
	deb->pause();
}

void dbgController::onActDbgStop()
{
	deb->stop();
//   deb->detach();
//   freeContext();
	act->actDbgOver->setEnabled(false);
	act->actDbgIn->setEnabled(false);
	act->actDbgOut->setEnabled(false);
	act->actDbgPause->setEnabled(false);
	act->actDbgStart->setEnabled(true);
	act->actDbgStop->setEnabled(false);
}

void dbgController::onActDbgOver()
{
	deb->stepOver();
}

void dbgController::onActDbgIn()
{
	deb->stepInto();
}

void dbgController::onActDbgOut()
{
	deb->stepOut();
}




//////////////////////////////////////////////////////////////////////////////
// Debugger variables
//////////////////////////////////////////////////////////////////////////////

void dbgController::onActAddVar()
{
	QInputDialog dlg(nullptr);

	dlg.setInputMode(QInputDialog::TextInput);
	dlg.setLabelText(tr("Variable name:"));
	dlg.setWindowTitle(tr("New variable"));
	if (dlg.exec() == QDialog::Accepted)
	{
		QString text = dlg.textValue();
		if (!text.isEmpty()) {
			deb->addVariable(text.toUtf8());
			if (deb->isAttached())
				deb->requestData(variables);
			else
				fillVariables();
		}
	}
}

void dbgController::onActDelVar()
{
	int idx = tblVars->currentRow();
	if (idx<0)
		return;
	QString var = tblVars->item(idx, 0)->text();

	deb->delVariable(var.toUtf8().data());
	fillVariables();
}

void dbgController::onActClearVars()
{
	deb->clearVariables();
	fillVariables();
}

void dbgController::onVarsItemSelectionChanged()
{
	act->btnDelVar->setEnabled(tblVars->currentRow()>=0 && tblVars->currentRow()<tblVars->rowCount());
}





//////////////////////////////////////////////////////////////////////////////
// Debugger breakpoints
//////////////////////////////////////////////////////////////////////////////

void dbgController::onActAddBreakpoint()
{
	QString f = getCurFilePath();
	int line = getCurLine();
	addBreakpoint(f, line);
	tblBP->selectRow(tblBP->rowCount()-1);
}

void dbgController::onActDelBreakpoint()
{
	int row = tblBP->currentRow();
	if (row < 0 || row >= tblBP->rowCount())
		return;
	QString f = tblBP->item(row, 2)->data(Qt::UserRole).toString();
	int line = tblBP->item(row, 3)->text().toInt();
	delBreakpoint(f, line);
	updateBreakpointsUI();
}

void dbgController::onActClearBreakpoints()
{
	clearBreakpoits();
}

void dbgController::onActActivateBreakpoint(bool activate)
{
	int row = tblBP->currentRow();
	if (row < 0 || row >= tblBP->rowCount())
		return;
	QString f = tblBP->item(row, 2)->data(Qt::UserRole).toString();
	int line = tblBP->item(row, 3)->text().toInt();
	activateBreakpoint(f, line, activate);
	tblBP->selectRow(row);
}

void dbgController::onActActivateHitcount(bool activate)
{
	int row = tblBP->currentRow();
	if (row < 0 || row >= tblBP->rowCount())
		return;
	QString f = tblBP->item(row, 2)->data(Qt::UserRole).toString();
	int line = tblBP->item(row, 3)->text().toInt();
	act->edtHitCount->setEnabled(activate);
	changeHitCount(f, line, activate ? 1 : 0);
	tblBP->selectRow(row);
}

void dbgController::onActChangeHitcount()
{
	int row = tblBP->currentRow();
	if (row < 0 || row >= tblBP->rowCount())
		return;
	int count = act->edtHitCount->value();
	QString f = tblBP->item(row, 2)->data(Qt::UserRole).toString();
	int line = tblBP->item(row, 3)->text().toInt();
	changeHitCount(f, line, count>=1 ? count : 0);
	act->btnHitcountBP->setEnabled(count>=1);
	tblBP->selectRow(row);
}

void dbgController::onBPItemSelectionChanged()
{
	int row = tblBP->currentRow();
	bool enbl = true;
	if (row < 0 || row >= tblBP->rowCount())
		enbl = false;

	act->btnDelBP->setEnabled(enbl);
	act->btnActiveBP->setEnabled(enbl);
	act->btnHitcountBP->setEnabled(enbl);
	act->edtHitCount->setEnabled(enbl);

	if (!enbl)
		return;

	bool active = tblBP->item(row, 1)->text() == tr("active");
	int cnt = tblBP->item(row, 4)->text().toInt();

	act->btnActiveBP->setChecked(active);
	act->btnHitcountBP->setChecked(cnt>=1);
	act->edtHitCount->setValue(cnt>=1 ? cnt : 0);
	act->edtHitCount->setEnabled(cnt>=1);
}

void dbgController::onBPCellDoubleClicked(int row, int column)
{
	Q_UNUSED(column);
	if (row < 0 || row >= tblBP->rowCount())
		return;
	QString f = tblBP->item(row, 2)->data(Qt::UserRole).toString();
	int line = tblBP->item(row, 3)->text().toInt();
	gotoLine(f, line);
}



void dbgController::addBreakpoint(const QString &filePath, int line, bool active, int hitCount)
{
	int idx = deb->addBreakpoint(filePath.toUtf8().data(), line);
	if (!active)
		deb->activateBreakpoint(idx, false);
	deb->getBreakpoint(idx).stopCount = hitCount;
	fillBreakpoints();
	JSEdit* edit = findEditorByFilePath(filePath.toUtf8().data());
	if (edit)
		edit->setBreakpoint(line, active ? JSEdit::BreakpointType::active : JSEdit::BreakpointType::inactive);
}

void dbgController::delBreakpoint(const QString &filePath, int line)
{
	deb->delBreakpoint(deb->findBreakpoint(filePath.toUtf8().data(), line));
	fillBreakpoints();
	JSEdit* edit = findEditorByFilePath(filePath.toUtf8().data());
	if (edit)
		edit->delBreakpoint(line);
}

void dbgController::clearBreakpoits()
{
	deb->clearBreakpoints();
	fillBreakpoints();
	for (int i=0; i<editParent->count(); i++) {
		JSEdit* edit = (JSEdit*)editParent->widget(i);
		if (edit)
			edit->clearBreakpoints();
	}
}

void dbgController::activateBreakpoint(const QString &filePath, int line, bool activate)
{
	int idx = deb->findBreakpoint(filePath.toUtf8().data(), line);
	if (idx>=0) {
		deb->activateBreakpoint(idx, activate);
		fillBreakpoints();
		JSEdit* edit = findEditorByFilePath(filePath.toUtf8().data());
		if (edit)
			edit->setBreakpoint(line, activate ? JSEdit::BreakpointType::active : JSEdit::BreakpointType::inactive);
	}
}

void dbgController::changeHitCount(const QString &filePath, int line, int count)
{
	int idx = deb->findBreakpoint(filePath.toUtf8().data(), line);
	if (idx>=0) {
		deb->getBreakpoint(idx).stopCount = count;
		fillBreakpoints();
	}
}

void dbgController::updateBreakpointsUI()
{
	act->btnAddBP->setEnabled(numEditors()>0);
	act->btnClearBP->setEnabled(tblBP->rowCount() > 0);
	act->btnDelBP->setEnabled(tblBP->currentRow() >= 0 && tblBP->currentRow() < tblBP->currentRow());
	act->btnActiveBP->setEnabled(act->btnDelBP->isEnabled());
	act->btnHitcountBP->setEnabled(act->btnDelBP->isEnabled());
	act->edtHitCount->setEnabled(act->btnDelBP->isEnabled());
}

void dbgController::storeBreakpointsInProject()
{
	for (int i = 0; i < prj.numEntries(); i++)
		prj.getEntry(i)->breakpoints.clear();

	for (int i = 0; i < deb->numBreakpoints(); i++) {
		Breakpoint &bp = deb->getBreakpoint(i);
		std::string fp = bp.file;
		for (int j = 0; j < prj.numEntries(); j++) {
			dbgProjectEntry* e = prj.getEntry(j);
			std::string fp2 = e->filePath;
			if (fp == fp2) {
				e->breakpoints.push_back(bp);
				continue;
			}
		}
	}
}

void dbgController::projectBreakpointsToDebugger()
{
	deb->clearBreakpoints();

	for (int i = 0; i < prj.numEntries(); i++) {
		dbgProjectEntry* e = prj.getEntry(i);
		for (int j = 0; j < (int)e->breakpoints.size(); j++) {
			int idx = deb->addBreakpoint(e->breakpoints[j].file.c_str(), e->breakpoints[j].line);
			deb->activateBreakpoint(idx, e->breakpoints[j].active);
			deb->getBreakpoint(idx).stopCount = e->breakpoints[j].stopCount;
		}
	}
}




//////////////////////////////////////////////////////////////////////////////
// Debugger callstack
//////////////////////////////////////////////////////////////////////////////

void dbgController::onStackCellDoubleClicked(int row, int column)
{
	Q_UNUSED(column);
	if (row < 0 || row >= tblStack->rowCount())
		return;
	QString f = tblStack->item(row, 1)->data(Qt::UserRole).toString();
	int line = tblStack->item(row, 2)->text().toInt();
	gotoLine(f, line);
}




//////////////////////////////////////////////////////////////////////////////
// Debugger callbacks
//////////////////////////////////////////////////////////////////////////////

void dbgController::debugBreak(duk_trans_dvalue_ctx *context)
{
	Q_UNUSED(context);
	if (act) {
		act->actDbgOver->setEnabled(true);
		act->actDbgIn->setEnabled(true);
		act->actDbgOut->setEnabled(true);
		act->actDbgPause->setEnabled(false);
		act->actDbgStart->setEnabled(true);
		act->actDbgStop->setEnabled(true);
	}
	deb->requestData(callstack);
}

void dbgController::debugData(dbgDataType t)
{
	switch (t) {
		case variables:
			fillVariables();
			break;
		case callstack:
			fillCallstack();
			deb->requestData(variables);
			fillBreakpoints();
			break;
		default: ;
	}
}

void dbgController::debugIdle()
{
	QApplication::processEvents();
	QThread::msleep(1);
}

void dbgController::debugPrint(const char *msg, dbgLogLevel level)
{
	QApplication::processEvents();
	if (level == llFatal) {
		edOut->setTextColor(Qt::white);
		edOut->setTextBackgroundColor(logCol[llFatal]);
	} else {
		edOut->setTextColor(logCol[level]);
		edOut->setTextBackgroundColor(Qt::transparent);
	}
	switch (level) {
		case llWarning: edOut->insertPlainText("[WARNING] "); break;
		case llError: edOut->insertPlainText("[ERROR] "); break;
		case llFatal: edOut->insertPlainText("[FATAL] "); break;
		default: ;
	}

	QString m(msg);
	m.append('\n');
	edOut->insertPlainText(m);
	QTextCursor c = edOut->textCursor();
	c.movePosition(QTextCursor::End);
	edOut->setTextCursor(c);
	edOut->ensureCursorVisible();
	QApplication::processEvents();
}

void dbgController::debugOutput(const QString& status, const QString& msg, bool first, bool newLine)
{
	if (first) {
		edOut->insertPlainText("["+status+"] "+msg);
		if (newLine)
			edOut->insertPlainText("\n");
	} else {
		QTextCursor c = edOut->textCursor();
		edOut->setTextColor(Qt::white);
		c.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveMode::KeepAnchor);
		c.removeSelectedText();
		edOut->setTextCursor(c);
		edOut->insertPlainText("["+status+"] "+msg);
		if (newLine)
			edOut->insertPlainText("\n");
	}
	edOut->ensureCursorVisible();
	QApplication::processEvents();
}

void dbgController::debugFatal(const char *msg)
{
	debugPrint(msg, llFatal);
	deb->detach();
}

void dbgController::fillCallstack()
{
	if (!tblStack)
		return;
	QTableWidgetItem *item;
	tblStack->clearContents();
	auto d = deb->stackFrames();
	tblStack->setRowCount(d.size());

	for (size_t i= 0; i < d.size(); i++) {
		Stackframe* f = d[i];

		item = new QTableWidgetItem(QString::number(i+1));
		tblStack->setItem(i, 0, item);
		if (fullPathInCallstack) {
			item = new QTableWidgetItem(f->file.c_str());
			item->setData(Qt::UserRole, QString::fromStdString(f->file));
		}
		else {
			QFileInfo fi(QString::fromStdString(f->file));
			item = new QTableWidgetItem(fi.fileName().toUtf8().data());
			item->setData(Qt::UserRole, QString::fromStdString(f->file));
		}
		tblStack->setItem(i, 1, item);
		item = new QTableWidgetItem(QString::number(f->line));
		tblStack->setItem(i, 2, item);
		item = new QTableWidgetItem(f->func.c_str());
		tblStack->setItem(i, 3, item);

		if (i == 0) {
			JSEdit* e = findEditorByFilePath(f->file.c_str());
			if (!e) {
				e = newEditor(QString::fromStdString(f->file));
			} else
				editParent->setCurrentWidget(e);
			if (e)
				e->setExecLine(f->line);
		}
	}
	tblStack->clearSelection();
	tblStack->setCurrentItem(nullptr);
}

void dbgController::fillVariables()
{
	QTableWidgetItem *item;
	tblVars->setRowCount(0);
	auto d =deb->Variables();
	tblVars->setRowCount(d.size());
	tblVars->setColumnCount(2);

	for (size_t i= 0; i < d.size(); i++) {
		Variable* f = d[i];

		item = new QTableWidgetItem(f->name.c_str());
		tblVars->setItem(i, 0, item);
		item = new QTableWidgetItem(f->val.c_str());
		if (d[i]->newVal)
			item->setForeground(QColor(32, 255, 32, 255));
		tblVars->setItem(i, 1, item);
	}
}

void dbgController::fillBreakpoints()
{
	// Num  State  File  Line  Hitcount
	QTableWidgetItem *item;
	tblBP->setRowCount(0);
	int num = deb->numBreakpoints();
	tblBP->setRowCount(num);

	for (int i = 0; i < num; i++) {
		auto d = deb->getBreakpoint(i);
		item = new QTableWidgetItem(QString::number(i+1));
		tblBP->setItem(i, 0, item);
		item = new QTableWidgetItem(d.active ? tr("active") : tr("inactive"));
		tblBP->setItem(i, 1, item);
		if (fullPathInBreakpoints) {
			item = new QTableWidgetItem(d.file.c_str());
			item->setData(Qt::UserRole, QString::fromStdString(d.file));
		}
		else {
			QFileInfo fi(QString::fromStdString(d.file));
			item = new QTableWidgetItem(fi.fileName().toUtf8().data());
			item->setData(Qt::UserRole, QString::fromStdString(d.file));
		}
		tblBP->setItem(i, 2, item);
		item = new QTableWidgetItem(QString::number(d.line));
		tblBP->setItem(i, 3, item);
		item = new QTableWidgetItem(QString::number(d.stopCount));
		tblBP->setItem(i, 4, item);
	}
}

void dbgController::fillProject()
{
	QTableWidgetItem *item;
	tblProject->setRowCount(0);
	int num = prj.numEntries();
	tblProject->setRowCount(num);
	for (int i = 0; i < num; i++) {
		auto d = prj.getEntry(i);
		QString f = QString::fromStdString(d->filePath);
		QFileInfo fi(f);
		QString fn = fi.fileName();
		item = new QTableWidgetItem(fn);
		tblProject->setItem(i, 0, item);
		item = new QTableWidgetItem(f);
		tblProject->setItem(i, 1, item);
	}
}


//////////////////////////////////////////////////////////////////////////////
// menu support
//////////////////////////////////////////////////////////////////////////////

void dbgController::onActNewScript()
{
	newEditor();
	updateUI();
}

void dbgController::onActOpenSript()
{
	QString f = QFileDialog::getOpenFileName(nullptr, tr("Open file"), "", tr("JavaScipt files (*.js);; all files (*.*)"));
	if (f.isEmpty())
		return;
	JSEdit* e = findEditorByFilePath(f.toUtf8().data());
	if (e) {
		editParent->setCurrentWidget(e);
		e->setFocus();
		return;
	}

	e = newEditor(f);
	updateBreakpointsUI();
	e->gotoLineColumn(1, 1);
	projectModified(needsCompilation);
	updateUI();
}

void dbgController::onActSaveScript()
{
	JSEdit* e = (JSEdit*)editParent->currentWidget();
	if (e) {
		e->saveFile(e->getFilePath());
		updateUI();
	}
}

void dbgController::onActSaveAsScript()
{
	JSEdit* e = (JSEdit*)editParent->currentWidget();
	if (e) {
		e->saveFile(QString());
		updateUI();
	}
}

void dbgController::onActCloseScript()
{
	JSEdit* e = (JSEdit*)editParent->currentWidget();
	if (e)
		if (e->closeIfPossible()) {
			editParent->removeTab(editParent->currentIndex());
		}
	updateUI();
}



//////////////////////////////////////////////////////////////////////////////
// project
//////////////////////////////////////////////////////////////////////////////

void dbgController::onActNewProject()
{

	updateUI();
}

void dbgController::onActOpenProject()
{
	if (prj.openFile()) {
		while (editParent->count())
			editParent->removeTab(editParent->count()-1);
		projectBreakpointsToDebugger();
		for (int i=0; i<prj.numEditFiles(); i++)
			newEditor(QString::fromStdString(prj.getEditFile(i)));
		deb->clearVariables();
		for (int i=0; i<prj.numVars(); i++)
			deb->addVariable(prj.getVar(i).c_str());
		fillVariables();
		fillProject();
		projectModified(false);
		fillBreakpoints();
		editParent->setCurrentIndex(prj.getCurEditor());
	}
	updateUI();
}

void dbgController::onActSaveProject()
{
	storeBreakpointsInProject();
	prj.clearVars();
	for (int i=0; i<(int)deb->Variables().size(); i++)
		prj.addVar(deb->Variables()[i]->name);
	prj.clearEditFiles();
	for (int i=0; i<editParent->count(); i++)
		prj.addEditFile(((JSEdit*)editParent->widget(i))->getFilePath().toStdString());
	prj.setCurEditor(editParent->currentIndex());
	if (prj.saveFile()) {
		projectModified(false);
	}
	updateUI();
}

void dbgController::onActSaveAsProject()
{
	storeBreakpointsInProject();
	prj.setFilePath("");
	prj.clearVars();
	for (int i=0; i<(int)deb->Variables().size(); i++)
		prj.addVar(deb->Variables()[i]->name);
	prj.clearEditFiles();
	for (int i=0; i<editParent->count(); i++)
		prj.addEditFile(((JSEdit*)editParent->widget(i))->getFilePath().toStdString());
	prj.setCurEditor(editParent->currentIndex());
	if (prj.saveFile()) {
		projectModified(false);
	}
	updateUI();
}

void dbgController::onActCloseProject()
{
	updateUI();
}

void dbgController::onAddCurFileToProject()
{
	QString fp = getCurFilePath();
	if (!fp.isEmpty()) {
		prj.addFile(fp.toStdString());
		// there might be breakpoints already:
		for (int i = 0; i<ed->numBreakpoints(); i++) {
			JSEdit::Break *b = ed->getBreakpoint(i);
			int idx = deb->addBreakpoint(fp.toUtf8().data(), b->line);
			if (b->type == JSEdit::BreakpointType::inactive)
				deb->activateBreakpoint(idx, false);
		}
		needsCompilation = true;
		projectModified(needsCompilation);
	}
	fillProject();
	updateUI();
}

void dbgController::onDelSelectedFilesFromProject()
{
	auto d = tblProject->selectedItems();
	for (int i=0; i<d.size(); i++) {
		if (d[i]->column() == 1) {
			std::string fp = d[i]->text().toStdString();
			prj.delFile(fp);
		} else
			continue;
	}
	fillProject();
	projectBreakpointsToDebugger();
	fillBreakpoints();
	updateUI();
}

void dbgController::onProjectCellDoubleClicked(int row, int column)
{
	Q_UNUSED(column);
	if (row < 0 || row >= tblProject->rowCount())
		return;
	QString f = tblProject->item(row, 1)->text();
	gotoLine(f, -1);
	updateUI();
}




//////////////////////////////////////////////////////////////////////////////
// compilation
//////////////////////////////////////////////////////////////////////////////

QString dbgController::fileContent(const char* path)
{
	QString res;
	QFile file(path);
	if (file.open(QFile::ReadOnly | QIODevice::Text)) {
		res = file.readAll();
		file.close();
	}

	return res;
}

void my_fatal (void *udata, const char *msg)
{
	Q_UNUSED(udata);
	dbgController::Instance().debugFatal(msg);
	abort();
}

void dbgController::createContext()
{
	freeContext();
	ctx = duk_create_heap_default();
//   void *my_udata = (void *) 0xdeadbeef;
//   ctx = duk_create_heap(NULL, NULL, NULL, my_udata, my_fatal);
}

void dbgController::freeContext()
{
	if (!ctx)
		return;

	if (deb->isAttached())
		deb->detach();

	duk_destroy_heap(ctx);
	ctx = nullptr;
}

// auto-save all modified files;
// ask to save all unsaved documents; return false if not done
bool dbgController::preCompile()
{
	bool res = true;
	for (int i = 0; i<editParent->count(); i++) {
		JSEdit* e = (JSEdit*)editParent->widget(i);
		if (!e->saveFile(e->getFilePath()))
			 res = false;
	}
	updateUI();
	return res;
}

void dbgController::compileProject()
{
	createContext();

	if (!preCompile())
		return;

	emit compiled();

	debugPrint("Compiling...", llInfo);

	for (int i=0; i<prj.numEntries(); i++) {
		QString content = fileContent(prj.getEntry(i)->filePath.c_str());
		// push filename (important):
		duk_push_string(ctx, prj.getEntry(i)->filePath.c_str());

		// print info
		QString fl = QString::fromStdString(prj.getEntry(i)->filePath);
		QFileInfo fi(fl);
//		std::string s = "Compiling " + fi.fileName().toStdString() + "...";
//		debugPrint(s.c_str(), llInfo);
		debugOutput(" .. ", fi.fileName(), true, false);

		// compile:
		if (duk_pcompile_string_filename(ctx, 0, (char*)content.toUtf8().data())) {
			debugOutput("ERR!", fi.fileName(), false, true);
			std::string err = "Compilation failed in file: " + prj.getEntry(i)->filePath;
			debugPrint(err.c_str(), llError);
			debugPrint(duk_safe_to_string(ctx, -1), llError);
			needsCompilation = true;
			return;
		}
		// call entire script without the debugger being attached:
		if (duk_pcall(ctx, 0) != DUK_EXEC_SUCCESS) {
			debugOutput("RUN!", fi.fileName(), false, true);
			std::string err = "Evaluation failed in file: " + prj.getEntry(i)->filePath;
			debugPrint(err.c_str(), llError);
			debugPrint(duk_safe_to_string(ctx, -1), llError);
			needsCompilation = true;
			return;
		}
		// discard result:
		duk_pop(ctx);

		// print info
		debugOutput(" OK ", fi.fileName(), false, true);
		//debugPrint("[OK]", llInfo);

//      emit(compiled());
	}

	deb->attach(ctx);
	deb->resume();

	needsCompilation = false;
	projectModified(needsCompilation);
}

void dbgController::callFunction(const QString &func)
{
	if (needsCompilation)
		return;
	if (!ctx)
		return;

	deb->attach(ctx);
	deb->resume();

	// --- call function ---
	// find "func" (will arrive at TOS):
	if (!duk_get_global_string(ctx, func.toUtf8().data())) {
		std::string err = "Runtime error: function '"+std::string(func.toUtf8().data())+"' not found";
		debugPrint(err.c_str(), llError);
		debugPrint(duk_safe_to_string(ctx, -1), llError);
		deb->detach();
		return;;
	}
	// perform the actual function-call:
	if (duk_pcall(ctx, 0)) {
		std::string err = "Runtime error in function '"+std::string(func.toUtf8().data())+"':";
		debugPrint(err.c_str(), llError);
		debugPrint(duk_safe_to_string(ctx, -1), llError);
		deb->detach();
		return;
	}
	// discard result:
	duk_pop(ctx);

	deb->detach();
}



//////////////////////////////////////////////////////////////////////////////
// ui
//////////////////////////////////////////////////////////////////////////////

void dbgController::updateUI()
{
	updateBreakpointsUI();

	bool script = (editParent->count() > 0);
	bool canCloseScript = script && ((JSEdit*)editParent->currentWidget());
	bool canSaveScript = canCloseScript &&
								((JSEdit*)editParent->currentWidget())->document()->isModified() &&
								!getCurFilePath().isEmpty();

	// update actions
	act->actDbgSaveScript->setEnabled(canSaveScript);
	act->actDbgSaveAsScript->setEnabled(canCloseScript);
	act->actDbgCloseScript->setEnabled(canCloseScript);

	act->actDbgSaveProject->setEnabled(!prj.isEmpty());
	act->actDbgSaveAsProject->setEnabled(prj.numEntries()>0);
	act->actDbgCloseProject->setEnabled(prj.numEntries()>0);

	act->btnAddToProject->setEnabled(editParent->count() > 0 && prj.indexOf(getCurFilePath().toStdString()) < 0);
	act->btnDelFromProject->setEnabled(tblProject->currentRow() >= 0);

	act->btnAddVar->setEnabled(prj.numEntries()>0);
	act->btnDelVar->setEnabled(tblVars->currentRow() >= 0);
	act->btnClearVars->setEnabled(tblVars->rowCount() > 0);
}

