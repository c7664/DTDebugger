#ifndef DEBUGCONSOLE_H
#define DEBUGCONSOLE_H

#include <QPlainTextEdit>
#include <QKeyEvent>
#include <QTextBlock>
#include "../dbg_core/debugger.h"
#include "dbgController.h"
#include <dukglue/dukglue.h>


#define REPL_HISTORY_SIZE 100

// implementation of a REPL (Read-Evaluate-Print-Loop)

class DebugConsole : public QPlainTextEdit
{
	Q_OBJECT
public:
	enum class Mode {
		read,
		eval,
		print
	};
	DebugConsole(const QString &text, QWidget *parent = nullptr);
	DebugConsole(QWidget *parent = nullptr);

public slots:
	void clear();

protected:
	void keyPressEvent(QKeyEvent *e) override;

	void init();

	int caretPos();
	void sanatizeCaretPos();

	void welcome();
	void prompt(bool newLine = true);

	void eval();

	void histUp();
	void histDown();
	void toHistory(QString text);
	void replaceCurLineWith(const QString& text);

	void beep();

	Mode mode;
	int line, startPos;
	QString  curText;
	QTextBlock curBlock;
	QString history[REPL_HISTORY_SIZE];
	int hIdx = 0, hSize = 0, hCur = 0;

	QString promptStr;
	int promptLen;
};

#endif // DEBUGCONSOLE_H
