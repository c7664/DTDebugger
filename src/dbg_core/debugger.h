/* Duktape Debugger
   written 2022 by ageman's stuff

   The Debugger-class is a wrapper for Duktape's debugging
   capabilities.
   It is utilizing the example-protocol from Duktape's samples
   (see duk_trans_dvalue).

   Because of that simple data-stream remote-debugging via TCP
   is not possible here. I wanted a simple local JS-Debugger, that
   is easy to embed in your application.


   ageman, 2022

*/

#ifndef __DEBUG_HANDLER_H__
#define __DEBUG_HANDLER_H__

#include <vector>
#include <string>
#include <duktape.h>
extern "C" {
#include <debug_proto/duk_trans_dvalue.h>
}

#define DUK_USE_DEBUG
#define DUK_USE_DEBUGGER_SUPPORT

struct Breakpoint {
   std::string file;
   int line;
   bool active;
   int stopCount;
   int count;
};

struct Stackframe {
  std::string file;
  std::string func;
  int line;
  int pc;
};

struct Variable {
   std::string name;
   std::string val;
   bool newVal;
};

enum dbgDataType {
  variables,
  callstack,
  undef
};

enum dbgLogLevel {
   llInternal,
   llScript,
   llInfo,
   llWarning,
   llError,
   llFatal
};

typedef void(*dbgBreak)(duk_trans_dvalue_ctx *ctx);
typedef void(*dbgData)(dbgDataType type);
typedef void(*dbgIdle)();
typedef void(*dbgLog)(const char* msg, dbgLogLevel level);



class Debugger
{
public:
   static Debugger &Instance();
   void setBreakCallback(dbgBreak fn) {breakFunction = fn; }
   void setDataCallback(dbgData fn) { dataFunction = fn; }
   void setIdleCallback(dbgIdle fn) { idleFunction = fn; }
   void setLogCallback(dbgLog fn) { logFunction = fn; }
   void attach(duk_context* context);
   void detach();
   bool isAttached() { return vm != nullptr; }

   // Breakpoints
   int addBreakpoint(const char* file, int line); // returns index of bp
   void delBreakpoint(int index);
   int findBreakpoint(const char* file, int line);
   void activateBreakpoint(int index, bool activate);
   void clearBreakpoints();
   int numBreakpoints() { return breakpoints.size(); }
   Breakpoint& getBreakpoint(int index);

   // Control
   bool pause();
   bool resume();
   bool stop();
   bool stepOver();
   bool stepInto();
   bool stepOut();
   bool isPaused() { return paused; }

   // data
   void requestData(dbgDataType type);
   int addVariable(const char* varName, const char* varValue = nullptr);
   void delVariable(const char* varName);
   void delVariable(int index);
   int findVariable(const char* var);
   void clearVariables();
   std::vector<Stackframe*> stackFrames() { return frames; }
   std::vector<Variable*> &Variables() { return localVars; }

   void installPrinting();

private:
   friend void dbg_detached(duk_trans_dvalue_ctx *ctx);
   friend void dbg_received(duk_trans_dvalue_ctx *c, duk_dvalue *dv);
   friend void dbg_cooperate(duk_trans_dvalue_ctx *ctx, int block);
   friend void dbg_handshake(duk_trans_dvalue_ctx *ctx, const char *line);
   friend void dbg_msg(long level, const char* file, long line, const char* func, const char* msg);
   friend void dbg_fatal(const char* msg);
   friend duk_ret_t native_print(duk_context*);

   std::string getVersionString() const;

   Debugger();
   ~Debugger();
   void sendAddBP(const char* file, int line);
   void sendDelBP(const char* file, int line);
   void eval(const char* cmd);
   void setVariableValue(int index, const char* val, bool newVal);

   void clearFrames();
//   void clearLocals();
   void dataReady(dbgDataType type);
   dbgBreak breakFunction = nullptr;
   dbgData  dataFunction = nullptr;
   dbgIdle  idleFunction = nullptr;
   dbgLog   logFunction  = nullptr;

   void log(const char* msg, dbgLogLevel level = llInfo);
   bool logInternals = false;

   dbgDataType curData = undef;
   duk_trans_dvalue_ctx *ctx = nullptr;
   duk_context* vm = nullptr;
   std::vector<Breakpoint> breakpoints;
   bool paused = false;

   std::vector<Stackframe*> frames;
   std::vector<Variable*>   localVars;
};

//static Debugger& debugger() { return Debugger::Instance(); };

#endif
