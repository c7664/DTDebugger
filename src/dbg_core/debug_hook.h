/* Duktape Debugger
   written 2022 by ageman's stuff

   Function-prototypes for Duktape's internal debug-messages
*/

#ifndef __DEBUG_HOOK_H__
#define __DEBUG_HOOK_H__

#ifdef __cplusplus
extern "C" {
#endif
void dbg_msg(
   long level,
   const char* file,
   long line,
   const char* func,
   const char* msg);

void dbg_fatal(const char* msg);
#ifdef __cplusplus
}
#endif
#endif
