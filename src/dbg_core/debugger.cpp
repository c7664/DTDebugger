/* Duktape Debugger
   written 2022 by ageman's stuff

   The Debugger-class is a wrapper for Duktape's debugging
   capabilities.
   It is utilizing the example-protocol from Duktape's samples
   (see duk_trans_dvalue).

   Because of that simple data-stream remote-debugging via TCP
   is not possible here. I wanted a simple local JS-Debugger, that
   is easy to embed in your application.


   ageman, 2022

*/

#include <stdio.h>
#include <assert.h>
#include "debugger.h"
#include "debug_hook.h"
#include <debug_proto/duk_trans_dvalue.h>
#include <stdio.h>


/* Commands initiated by debug client. */
#ifndef DUK_DBG_CMD_BASICINFO

#define DUK_DBG_CMD_BASICINFO           0x10
#define DUK_DBG_CMD_TRIGGERSTATUS       0x11
#define DUK_DBG_CMD_PAUSE               0x12
#define DUK_DBG_CMD_RESUME              0x13
#define DUK_DBG_CMD_STEPINTO            0x14
#define DUK_DBG_CMD_STEPOVER            0x15
#define DUK_DBG_CMD_STEPOUT             0x16
#define DUK_DBG_CMD_LISTBREAK           0x17
#define DUK_DBG_CMD_ADDBREAK            0x18
#define DUK_DBG_CMD_DELBREAK            0x19
#define DUK_DBG_CMD_GETVAR              0x1a
#define DUK_DBG_CMD_PUTVAR              0x1b
#define DUK_DBG_CMD_GETCALLSTACK        0x1c
#define DUK_DBG_CMD_GETLOCALS           0x1d
#define DUK_DBG_CMD_EVAL                0x1e
#define DUK_DBG_CMD_DETACH              0x1f
#define DUK_DBG_CMD_DUMPHEAP            0x20
#define DUK_DBG_CMD_GETBYTECODE         0x21
#define DUK_DBG_CMD_APPREQUEST          0x22
#define DUK_DBG_CMD_GETHEAPOBJINFO      0x23
#define DUK_DBG_CMD_GETOBJPROPDESC      0x24
#define DUK_DBG_CMD_GETOBJPROPDESCRANGE 0x25

#endif

// duktape notify commands:
#ifndef DUK_DBG_CMD_STATUS
#define DUK_DBG_CMD_STATUS    0x01
#define DUK_DBG_CMD_UNUSED_2  0x02 /* Duktape 1.x: print notify */
#define DUK_DBG_CMD_UNUSED_3  0x03 /* Duktape 1.x: alert notify */
#define DUK_DBG_CMD_UNUSED_4  0x04 /* Duktape 1.x: log notify */
#define DUK_DBG_CMD_THROW     0x05
#define DUK_DBG_CMD_DETACHING 0x06
#define DUK_DBG_CMD_APPNOTIFY 0x07
#endif

static int lvarcnt = -1;
static Debugger& debugger() { return Debugger::Instance(); };

duk_ret_t native_print(duk_context *ctx) {
   duk_push_string(ctx, " ");
   duk_insert(ctx, 0);
   duk_join(ctx, duk_get_top(ctx) - 1);
   if (debugger().logFunction)
      debugger().logFunction(duk_to_string(ctx, -1), llScript);
   return 0;
}

extern "C" void dbg_msg(
      long level,
      const char* file,
      long line,
      const char* func,
      const char* msg)
{
   if (!debugger().logInternals)
      return;
   char buf[2000];
   sprintf(buf, "INTERNAL: '%s'; Level %li file %s, functions %s, line %li.", msg, level, file, func, line);
   debugger().log(buf, llInternal);
}

extern "C" void dbg_fatal(const char* msg)
{
   if (!debugger().logInternals)
      return;
   debugger().log(msg, llFatal);
}

void dbg_cooperate(duk_trans_dvalue_ctx *ctx, int block)
{
   if (debugger().idleFunction)
      debugger().idleFunction();

   if (!block) {
      return;
   }

   //   static int i = 0;
   //   i++;
   //   fprintf(stderr, "Break: %d\n", i);

   if (debugger().breakFunction && !debugger().isPaused()) {
      debugger().paused = true;
      debugger().breakFunction(ctx);
   }
//   else if (debugger().idleFunction)
//      debugger().idleFunction();
}

void dbg_received(duk_trans_dvalue_ctx *c, duk_dvalue *dv)
{
   (void) c;
   static int cnt = -1;
   static Stackframe* f;
   static bool foundVar = false;


   char buf[1000];
   //  duk_dvalue_to_string(dv, buf);
   //  fprintf(stderr, "%s\n", buf);


   if (dv->tag == DUK_DVALUE_REP) {
      switch (debugger().curData) {
         case variables:
            cnt = 0;
            foundVar = false;
            goto quit;
         case callstack:
            cnt = 0;
            f = new Stackframe;
            goto quit;
         default: ;
      }
   }

   if (cnt>=0) {
      switch (debugger().curData) {
         case variables:
            if (cnt == 0) {
               if (dv->tag == DUK_DVALUE_EOM) {
                  cnt = -1;
                  debugger().curData = undef;
                  goto quit;
               }
               assert(dv->tag == DUK_DVALUE_INTEGER);
               foundVar = dv->i == 1;
               cnt++;
            } else if (cnt == 1) {
               if (dv->tag == DUK_DVALUE_EOM) {
                  cnt = -1;
                  debugger().curData = undef;
                  goto quit;
               }
               lvarcnt++;
               duk_dvalue_to_string(dv, buf);
               if (foundVar) {
                  debugger().setVariableValue(lvarcnt, buf, true);
               }
               cnt++;
            } else if (cnt == 2) {
               assert (dv->tag == DUK_DVALUE_EOM);
               cnt = -1;
               if (lvarcnt == (int)debugger().Variables().size()-1) {
                  lvarcnt = -1;
                  debugger().dataReady(variables);
               }
            }
            break;
         case callstack:
            if (cnt == 0) {
               if (dv->tag == DUK_DVALUE_EOM) {
                  cnt = -1;
                  delete f;
                  debugger().curData = undef;
                  goto quit;
               }
               assert(dv->tag == DUK_DVALUE_STRING);
               f->file = std::string((char*)dv->buf);
               cnt++;
            } else if (cnt == 1) {
               assert(dv->tag == DUK_DVALUE_STRING);
               f->func = std::string((char*)dv->buf);
               cnt++;
            } else if (cnt == 2) {
               assert(dv->tag == DUK_DVALUE_INTEGER);
               f->line = dv->i;
               cnt++;
            } else if (cnt == 3) {
               assert(dv->tag == DUK_DVALUE_INTEGER);
               f->pc = dv->i;
               cnt++;
            } else if (cnt == 4) {
               debugger().frames.push_back(f);
               if (dv->tag == DUK_DVALUE_EOM) {
                  cnt = -1;
                  debugger().dataReady(callstack);
               } else {
                  assert(dv->tag == DUK_DVALUE_STRING);
                  f = new Stackframe;
                  f->file = std::string((char*)dv->buf);
                  cnt = 1;
               }
               goto quit;
            }
            break;
         default: ;
      }
   }

quit:
   duk_dvalue_free(dv);
}

void dbg_handshake(duk_trans_dvalue_ctx* /* ctx */, const char * /*line*/)
{
   //   char buf[1000];
   //   sprintf(buf, "HANSHAKE occurred in line %s.", line);
   //   debugger().log(buf, llInfo);
}

void dbg_detached(duk_trans_dvalue_ctx *ctx)
{
   if (ctx == debugger().ctx) {
      std::string m = debugger().getVersionString() + " - debugger detached.\n";
      debugger().log(m.c_str(), llInternal);
      debugger().ctx = nullptr;
   }
}



//////////////////////////////////////////////////////////////////////////////
// ctor/dtor
//////////////////////////////////////////////////////////////////////////////

Debugger::Debugger()
{
   //   ctx = duk_trans_dvalue_init();
   //   if (!ctx) {
   //      fprintf(stderr, "FATAL: could not instantiate debugger-context.");
   //      fflush(stderr);
   //      exit(1);
   //   }
}

Debugger::~Debugger()
{
   //duk_trans_dvalue_free(ctx);
   detach();
}

Debugger& Debugger::Instance()
{
   static Debugger d;
   return d;
}

std::string Debugger::getVersionString() const
{
   int v = DUK_VERSION;
   int v1 = v % 100;
   int v2 = (v / 100) % 100;
   int v3 = (v / 10000) % 100;
   char buf[40];
   sprintf(buf, "Duktape %d.%02d.%02d", v3, v2, v1);
   return std::string(buf);
}



//////////////////////////////////////////////////////////////////////////////
// attach/detach vm
//////////////////////////////////////////////////////////////////////////////

void Debugger::installPrinting()
{
   duk_push_c_function(vm, native_print, DUK_VARARGS);
   duk_put_global_string(vm, "print");
}

void Debugger::attach(duk_context* context)
{
   if (isAttached())
      throw "Already attached!";
   if (!context)
      throw "Invalid context!";

   vm = context;

   ctx = duk_trans_dvalue_init();
   if (!ctx) {
      fprintf(stderr, "FATAL: could not instantiate debugger-context.");
      fflush(stderr);
      exit(1);
   }

   ctx->cooperate = dbg_cooperate;
   ctx->received = dbg_received;
   ctx->handshake = dbg_handshake;
   ctx->detached = dbg_detached;

   duk_debugger_attach(
            vm,
            duk_trans_dvalue_read_cb,
            duk_trans_dvalue_write_cb,
            duk_trans_dvalue_peek_cb,
            duk_trans_dvalue_read_flush_cb,
            duk_trans_dvalue_write_flush_cb,
            NULL,  /* app request cb */
            duk_trans_dvalue_detached_cb,
            (void *) ctx);

   duk_push_c_function(vm, native_print, DUK_VARARGS);
   duk_put_global_string(vm, "print");

   paused = true;

   std::string m = debugger().getVersionString() + " - debugger attached.\n";
   log(m.c_str(), llInternal);

   for (int i=0; i<(int)breakpoints.size(); i++) {
      if (breakpoints[i].active)
         sendAddBP(breakpoints[i].file.c_str(), breakpoints[i].line);
   }
}

void Debugger::detach()
{
   if (!isAttached())
      return;
   for (int i=0; i<(int)breakpoints.size(); i++) {
      if (breakpoints[i].active)
         sendDelBP(breakpoints[i].file.c_str(), breakpoints[i].line);
   }
   duk_debugger_detach(vm);
   duk_trans_dvalue_free(ctx);
   vm = nullptr;
   paused = false;
}



//////////////////////////////////////////////////////////////////////////////
// Breakpoints
//////////////////////////////////////////////////////////////////////////////

void Debugger::sendAddBP(const char* file, int line)
{
   if (isAttached()) {
      duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_ADDBREAK);
      duk_trans_dvalue_send_string(ctx, file);
      duk_trans_dvalue_send_integer(ctx, line);
      duk_trans_dvalue_send_eom(ctx);
   }
}

void Debugger::sendDelBP(const char* file, int line)
{
   if (isAttached()) {
      duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_DELBREAK);
      duk_trans_dvalue_send_string(ctx, file);
      duk_trans_dvalue_send_integer(ctx, line);
      duk_trans_dvalue_send_eom(ctx);
   }
}

void Debugger::eval(const char *cmd)
{
   if (isAttached()) {
      duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_EVAL);
      duk_trans_dvalue_send_integer(ctx, -1);
      duk_trans_dvalue_send_string(ctx, cmd);
      duk_trans_dvalue_send_eom(ctx);
   }
}

void Debugger::setVariableValue(int index, const char *val, bool newVal)
{
   assert(index>=0 && index<(int)localVars.size());

   localVars[index]->newVal = true;
   if (newVal) {
      if (val)
         localVars[index]->val = val;
      else
         localVars[index]->val.clear();
   }
}

int Debugger::addBreakpoint(const char* file, int line)
{
   int idx = findBreakpoint(file, line);
   if (idx >=0)
      return idx;

   Breakpoint bp { file, line, true, 0, 0 };
   breakpoints.push_back(bp);
   sendAddBP(file, line);
   return breakpoints.size()-1;
}

void Debugger::delBreakpoint(int index)
{
   if (index>=0 && index<(int)breakpoints.size()) {
      sendDelBP(breakpoints[index].file.c_str(), breakpoints[index].line);
      breakpoints.erase(breakpoints.begin() + index);
   }
}

int Debugger::findBreakpoint(const char* file, int line)
{
   for (int i=0; i<(int)breakpoints.size(); i++)
      if (line == breakpoints[i].line && strcmp(file, breakpoints[i].file.c_str()) == 0)
         return i;
   return -1;
}

void Debugger::activateBreakpoint(int index, bool activate)
{
   if (index>=0 && index<(int)breakpoints.size() && breakpoints[index].active != activate) {
      if (activate) {
         sendAddBP(breakpoints[index].file.c_str(), breakpoints[index].line);
         breakpoints[index].active = true;
      } else {
         sendDelBP(breakpoints[index].file.c_str(), breakpoints[index].line);
         breakpoints[index].active = false;
      }
   }
}

Breakpoint& Debugger::getBreakpoint(int index)
{
   return breakpoints[index];
}

void Debugger::clearBreakpoints()
{
   for (int i=0; i<(int)breakpoints.size(); i++) {
      if (isAttached() && breakpoints[i].active)
         sendDelBP(breakpoints[i].file.c_str(), breakpoints[i].line);
   }
   breakpoints.clear();
}


//////////////////////////////////////////////////////////////////////////////
// Control
//////////////////////////////////////////////////////////////////////////////

bool Debugger::pause()
{
   if (!isAttached() || paused)
      return false;

   duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_PAUSE);
   duk_trans_dvalue_send_eom(ctx);
   return true;
}

bool Debugger::resume()
{
   if (!isAttached() || !paused || !ctx)
      return false;

   paused = false;
   duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_RESUME);
   duk_trans_dvalue_send_eom(ctx);
   return true;
}

bool Debugger::stop()
{
   if (!isAttached())
      return false;

   duk_fatal_raw(vm, "Abort!");
//   eval("thow new Error(\"Execution aborted by debugger.\");");
//   eval("abort();");
//   detach();

   return true;
}


bool Debugger::stepOver()
{
   if (!isAttached() || !paused)
      return false;

   paused = false;
   duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_STEPOVER);
   duk_trans_dvalue_send_eom(ctx);
   return true;
}

bool Debugger::stepInto()
{
   if (!isAttached() || !paused)
      return false;

   paused = false;
   duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_STEPINTO);
   duk_trans_dvalue_send_eom(ctx);
   return true;
}

bool Debugger::stepOut()
{
   if (!isAttached() || !paused)
      return false;

   paused = false;
   duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_STEPOUT);
   duk_trans_dvalue_send_eom(ctx);
   return true;
}



/////////////////////////////////////////////////////////////////////////////
// logging
//////////////////////////////////////////////////////////////////////////////

void Debugger::log(const char *msg, dbgLogLevel level)
{
   if (logFunction)
      logFunction(msg, level);
}




/////////////////////////////////////////////////////////////////////////////
// data
//////////////////////////////////////////////////////////////////////////////

void Debugger::dataReady(dbgDataType type)
{
   curData = undef;
   if (dataFunction)
      dataFunction(type);
}

void Debugger::requestData(dbgDataType t)
{
   if (!isAttached() || !paused)
      return;
   switch (t) {
      case variables:
         curData = variables;
         lvarcnt = -1;
         for (int i = 0; i < (int)localVars.size(); i++) {
            localVars[i]->newVal = false;
            duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_GETVAR);
            duk_trans_dvalue_send_integer(ctx, -1); // stack-depth !
            duk_trans_dvalue_send_string(ctx, localVars[i]->name.c_str());
            duk_trans_dvalue_send_eom(ctx);
         }
         break;
      case callstack:
         curData = callstack;
         clearFrames();
         duk_trans_dvalue_send_req_cmd(ctx, DUK_DBG_CMD_GETCALLSTACK);
         duk_trans_dvalue_send_eom(ctx);
         break;
      case undef: ;
   }
}

void Debugger::clearFrames()
{
   for (size_t i=0; i<frames.size(); i++) {
      delete frames[i];
   }
   frames.clear();
}

int Debugger::findVariable(const char *var)
{
   for (int i=0; i<(int)localVars.size(); i++)
      if (strcmp(var, localVars[i]->name.c_str()) == 0)
         return i;
   return -1;
}

int Debugger::addVariable(const char *varName, const char *varValue)
{
   int idx = findVariable(varName);

   if (idx >= 0) {
      localVars[idx]->val = std::string(varValue);
      return idx;
   } else {
      Variable *v = new Variable;
      v->name = std::string(varName);
      if (varValue)
         v->val = std::string(varValue);
      v->newVal = false;
      localVars.push_back(v);
   }
   return localVars.size()-1;
}

void Debugger::delVariable(const char *varName)
{
   delVariable(findVariable(varName));
}

void Debugger::delVariable(int index)
{
   if (index>=0) {
      delete localVars[index];
      localVars.erase(localVars.begin() + index);
   }
}

void Debugger::clearVariables()
{
   for (size_t i=0; i<localVars.size(); i++) {
      delete localVars[i];
   }
   localVars.clear();
}


