#ifndef ANI_FUNCTIONGENERATOR_H
#define ANI_FUNCTIONGENERATOR_H

#include <math.h>
#include <QtGlobal>

namespace ani {

enum class Waveform {
   sine,
   triangle,
   sawtooth,
   invSawtooth,
   square,
   pwm,
   random
};

class FunctionGenerator
{
public:
   FunctionGenerator();

   qreal eval(qreal delta);
   void reset();

   Waveform getWaveForm() const { return wave; }
   qreal getFreq() const { return freq; }
   qreal getAmp() const { return amp; }
   qreal getOffset() const { return offset; }
   qreal getPhase() const { return phase; }
   qreal getPulseWidth() const { return pw; }

   void setWave(Waveform w) { wave = w; }
   void setFreq(qreal f);
   void setAmp(qreal a) { amp = a; }
   void setOffset(qreal o) { offset = o; }
   void setPhase(qreal p);
   void setPulseWidth(qreal p) { pw = p; }

protected:
   qreal evalSine(qreal delta);
   qreal evalTri(qreal delta);
   qreal evalSaw(qreal delta);
   qreal evalInvSaw(qreal delta);
   qreal evalSquare(qreal delta);
   qreal evalPWM(qreal delta);
   qreal evalRandom(qreal delta);

   Waveform wave;
   qreal freq;
   qreal amp;
   qreal offset;
   qreal phase;
   qreal pw;
   qreal t;
   qreal w; // omega
   qreal ph; // phase in radians
   qreal per; // period
   qreal tp; // phase in seconds
   qreal last;
   qreal last_t;
};

} // namespace ani

#endif // ANI_FUNCTIONGENERATOR_H
