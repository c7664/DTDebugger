#include "FunctionGenerator.h"
#include <stdlib.h>

namespace ani {

FunctionGenerator::FunctionGenerator()
{
   wave = Waveform::sine;
   setFreq(1.0);
   amp = 0.5;
   offset = 0.5;
   phase = 0.0;
   tp = 0.0;
   ph = 0.0;
   pw = 0.5;
   t = 0.0;
   last = 0.0;
   last_t = 0.0;
}

qreal FunctionGenerator::eval(qreal delta)
{
   switch (wave) {
      case Waveform::sine:        return evalSine(delta);
      case Waveform::triangle:    return evalTri(delta);
      case Waveform::sawtooth:    return evalSaw(delta);
      case Waveform::invSawtooth: return evalInvSaw(delta);
      case Waveform::square:      return evalSquare(delta);
      case Waveform::pwm:         return evalPWM(delta);
      case Waveform::random:      return evalRandom(delta);
   }
   return 0.0;
}

void FunctionGenerator::reset()
{
   t = 0.0;
   last = 0.0;
   last_t = 0.0;
}

void FunctionGenerator::setFreq(qreal f) {
   freq = qBound(1e-4, f, 1e4);
   w = 2.0 * M_PI * f;
   per = 1.0/f;
   tp = ph / w;
}

void FunctionGenerator::setPhase(qreal p) {
   phase = qBound(0.0, p, 359.9999999999);
   ph = phase * M_PI / 180.0;
   tp = ph / w;
}

qreal FunctionGenerator::evalSine(qreal delta)
{
   t += delta;
   return sin(w * t + ph) * amp + offset;
}

qreal FunctionGenerator::evalTri(qreal delta)
{
   t += delta;
   qreal c = fmod(t + tp, per) * freq;
   if (c >= 0.5)
      c = 1.0 - c;
   return 2.0 * amp * (c - 0.5) + offset;
}

qreal FunctionGenerator::evalSaw(qreal delta)
{
   t += delta;
   qreal c = fmod(t + tp, per) * freq;
   return 2.0 * amp * (c - 0.5) + offset;
}

qreal FunctionGenerator::evalInvSaw(qreal delta)
{
   t += delta;
   qreal c = 1.0 - fmod(t + tp, per) * freq;
   return 2.0 * amp * (c - 0.5) + offset;

}

qreal FunctionGenerator::evalSquare(qreal delta)
{
   t += delta;
   qreal c = fmod(t + tp, per) * freq;
   if (c >= 0.5)
      return amp + offset;
   else
      return -amp + offset;
}

qreal FunctionGenerator::evalPWM(qreal delta)
{
   t += delta;
   qreal c = fmod(t + tp, per) * freq;
   if (c <= pw)
      return amp + offset;
   else
      return -amp + offset;
}

qreal FunctionGenerator::evalRandom(qreal delta)
{
   t += delta;
   if (t-tp-last_t > per) {
     qreal c = 2.0 * (qreal)rand()/RAND_MAX - 1.0;
     last = amp * c + offset;
     last_t = t;
   }
   return last;
}


} // namespace ani
