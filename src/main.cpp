#include "MainWindow.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  a.setApplicationName("Duktape Debugger");
  a.setOrganizationName("ageman's stuff");

  QSurfaceFormat fmt;
  fmt.setSamples(16);
  QSurfaceFormat::setDefaultFormat(fmt);

  MainWindow w;
  a.setPalette(w.palette());
  w.show();
  return a.exec();
}
