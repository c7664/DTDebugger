#ifndef DLGADDNODE_H
#define DLGADDNODE_H

#include <QDialog>
#include <QVariant>
#include "ui_dlgAddNode.h"
#include "ECS/Node.h"


namespace Ui {
class dlgAddNode;
}

class dlgAddNode : public QDialog
{
   Q_OBJECT

public:
   explicit dlgAddNode(QWidget *parent = nullptr);
   ~dlgAddNode();

   ecs::NodeClass getNodeClass() { return (ecs::NodeClass)ui->lst->currentItem()->data(Qt::UserRole).value<int>(); }
   QString getName() { return ui->edtName->text(); }

protected:
   void changeEvent(QEvent *e);

private:
   Ui::dlgAddNode *ui;
};

#endif // DLGADDNODE_H
