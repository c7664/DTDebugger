#include "Rect.h"
#include "Consts.h"
#include <QPainter>
#include <QPen>

namespace ecs {

void Rect::registerScripting(duk_context *ctx)
{
   dukglue_set_base_class<ecs::Node, ecs::Rect>(ctx);
   dukglue_register_constructor<ecs::Rect>(ctx, "Rect");
   dukglue_register_method(ctx, &Node::asRect, "AsRect");
   dukglue_register_property(ctx, &Rect::getP1X, &Rect::setP1X, "P1_X");
   dukglue_register_property(ctx, &Rect::getP1Y, &Rect::setP1Y, "P1_Y");
   dukglue_register_property(ctx, &Rect::getP2X, &Rect::setP2X, "P2_X");
   dukglue_register_property(ctx, &Rect::getP2Y, &Rect::setP2Y, "P2_Y");
   //dukglue_register_property(ctx, &Rect::getP1, &Rect::setP1, "P1");
   //dukglue_register_property(ctx, &Rect::getP2, &Rect::setP1, "P2");
   dukglue_register_property(ctx, &Rect::getWidth, &Rect::setWidth, "Width");
   dukglue_register_property(ctx, &Rect::getHeight, &Rect::setHeight, "Height");
   dukglue_register_property(ctx, &Rect::getPenColor, &Rect::setPenColor, "PenColor");
   dukglue_register_property(ctx, &Rect::getBrushColor, &Rect::setBrushColor, "BrushColor");
   dukglue_register_property(ctx, &Rect::getPenWidth, &Rect::setPenWidth, "PenWidth");
   dukglue_register_property(ctx, &Rect::getPenWidthInPixel, &Rect::setPenWidthInPixel, "PenWidthInPixel");
}

QRectF Rect::getAABB()
{
   if (dirty)
      calcMatrix();
   QPointF s1 = M.map(p1);
   QPointF s2 = M.map(QPointF(p1.x(), p2.y()));
   QPointF s3 = M.map(p2);
   QPointF s4 = M.map(QPointF(p2.x(), p1.y()));
   QPointF q1 = QPointF(smallest(s1.x(), s2.x(), s3.x(), s4.x()), smallest(s1.y(), s2.y(), s3.y(), s4.y()));
   QPointF q2 = QPointF(largest(s1.x(), s2.x(), s3.x(), s4.x()), largest(s1.y(), s2.y(), s3.y(), s4.y()));
   QRectF res(q1, q2);
   return res;
}

Rect::Rect()
{
   name = "Untitled " + QString::fromUtf8(NodeClassName());

   cp.setBuddy(Color::buddyKind::pen, &pen);
   cb.setBuddy(Color::buddyKind::brush, &brush);
   setPenColor(priv::colRectPen);
   setBrushColor(priv::colRectBrush);
   setPenWidth(priv::penWidth);
   setPenWidthInPixel(priv::penWidthInPixel);
   p1 = QPointF(-50., -50.0);
   p2 = QPointF(50.0, 50.0);
   connect(&cp, &Color::colorChanged, this, &Rect::onCPChanged);
   connect(&cb, &Color::colorChanged, this, &Rect::onCBChanged);
}

void Rect::setPenColor(const Color &col)
{
   cp = col;
   emit penColorChanged(cp.asQColor());
}

void Rect::setBrushColor(const Color &col)
{
   cb = col;
   brush.setStyle(Qt::BrushStyle::SolidPattern);
   emit brushColorChanged(cb.asQColor());
}

void Rect::paint(QPainter &p)
{
   p.setPen(pen);
   p.setBrush(brush);

   p.drawRect(QRectF(p1.x(), p1.y(), p2.x()-p1.x(), p2.y()-p1.y()));
}

void Rect::onCPChanged()
{
   emit penColorChanged(cp.asQColor());
}

void Rect::onCBChanged()
{
   emit brushColorChanged(cb.asQColor());
}

}
