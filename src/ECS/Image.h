#ifndef ECS_IMAGE_H
#define ECS_IMAGE_H

#include <QDir>
#include <QPixmap>
#include "Node.h"

namespace ecs {

class Image : public Node
{
	Q_OBJECT
public:
	Image();
	~Image();

	static void registerScripting(duk_context *ctx);
	virtual NodeClass nodeClass() const override { return NodeClass::Image; }

	static const QStringList& getFilters();

	const char* getPathCStr() const { return dir.path().toUtf8().data(); }
	QString getPath() const { return dir.path(); }
	void setPathCStr(const char* p) { dir.setPath(QString::fromUtf8(p)); newFilename=true; needsRecache=true; createPixmap();  }
	QString getInfo();

	qreal getOpacity() const { return opacity; }
	bool getHInvert() const { return hInvert; }
	bool getVInvert() const { return vInvert; }

public slots:
	void setPath(const QString& path) { dir.setPath(path); needsRecache=true; newFilename=true; createPixmap(); emit pathChanged(path); }
	void setOpacity(qreal o) { opacity = qBound(0.0, o, 1.0); emit opacityChanged(opacity); }
	void setHInvert(bool hinvert) { hInvert = hinvert; needsRecache=true; emit hInvertChanged(hInvert); }
	void setVInvert(bool vinvert) { vInvert = vinvert; needsRecache=true; emit vInvertChanged(vInvert); }

signals:
	void pathChanged(QString path);
	void opacityChanged(qreal opacity);
	void hInvertChanged(bool invert);
	void vInvertChanged(bool invert);
	void infoChanged(QString info);

protected slots:
	virtual void paint(QPainter& p) override;
	virtual void createPixmap();

protected:
	QPixmap* pm;

	QDir dir;
	qreal opacity;
	bool hInvert, vInvert;

	bool needsRecache;
	bool newFilename;
	static QStringList filters;
};

} // namespace ecs

#endif // ECS_IMAGE_H
