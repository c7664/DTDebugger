#ifndef ECS_TEXT_H
#define ECS_TEXT_H

#include <QPainter>
#include <QPainterPath>

#include "Node.h"
#include "RoundRect.h"
#include "../dbg_ui/dbgController.h"
#include <dukglue/dukglue.h>

namespace ecs {

class Text : public RoundRect
{
   Q_OBJECT
public:
   enum class HAlign {
      alhLeft,
      alhRight,
      alhCenter
   };
   enum class VAlign {
      alvTop,
      alvBottom,
      alvCenter
   };

   Text();
   ~Text();
   static void registerScripting(duk_context *ctx);
   virtual NodeClass nodeClass() const override { return NodeClass::Text; }

   // caption
   const char *getCaptionCStr() const { return text.toUtf8().data(); }
   QString getCaption() const { return text; }
   void setCaptionCStr(const char* caption) { text = QString::fromUtf8(caption); needsRecache = true; emit captionChanged(text);  }

   // rect
   bool getRectVisible() const { return drawRect; }

   // colors
   Color* getTextPenColor() { return &ctp; }
   Color* getTextBrushColor() { return &ctb; }

   // pen width
   qreal getTextPenWidth() const { return textPenWidth; }
   bool getTextPenWidthInPixel() const { return textPenWidthInPixel; }

   // Text size (0.5..1.5, default=1.0)
   qreal getTextSize() const { return size; }

   // text alignment
   int getHAlign() const { return (int)alh; }
   int getVAlign() const { return (int)alv; }
   Qt::Alignment getQtAlignment() const;
   void setQtAlignment(Qt::Alignment align);

   // font
   const char *getFontNameCStr() const { return font.family().toUtf8().data(); }
   void setFontNameCStr(const char* name) { font.setFamily(QString::fromUtf8(name)); needsRecache = true; emit fontChanged(QString::fromUtf8(name)); }
   QString getFontName() const { return font.family(); }
   bool getBold() const { return font.bold(); }
   bool getItalic() const { return font.italic(); }
   bool getUnderline() const { return font.underline(); }
   bool getStrikeThrough() const { return font.strikeOut(); }


public slots:
   virtual void setP1X(qreal x) override { RoundRect::setP1X(x); needsRecache=true; }
   virtual void setP1Y(qreal y) override { RoundRect::setP1Y(y); needsRecache=true; }
   virtual void setP2X(qreal x) override { RoundRect::setP2X(x); needsRecache=true; }
   virtual void setP2Y(qreal y) override { RoundRect::setP2Y(y); needsRecache=true; }
   virtual void setP1(const QPointF& pos) override { RoundRect::setP1(pos); needsRecache=true; }
   virtual void setP2(const QPointF& pos) override { RoundRect::setP2(pos); needsRecache=true; }
   virtual void setWidth(qreal w) override { RoundRect::setWidth(w); needsRecache=true; }
   virtual void setHeight(qreal h) override { RoundRect::setHeight(h); needsRecache=true; }

   virtual void setCaption(const QString& cap) { text = cap;needsRecache=true; emit captionChanged(text); }
   virtual void setRectVisible(bool visible) { drawRect = visible; emit rectVisibleChanged(visible); }
   virtual void setTextPenColor(const Color& col) { ctp = col; needsRecache = true; emit textPenColorChanged(ctp.asQColor()); }
   virtual void setTextBrushColor(const Color& col) { ctb = col; needsRecache = true; emit brushColorChanged(ctb.asQColor()); }
   virtual void setTextPenQColor(const QColor& col) { setTextPenColor(Color(col)); }
   virtual void setTextBrushQColor(const QColor& col) { setTextBrushColor(Color(col)); }
   virtual void setTextPenColorName(const QString& colName) { setTextPenColor(QColor(colName)); }
   virtual void setTextBrushColorName(const QString& colName) { setTextBrushColor(QColor(colName)); }
   virtual void setTextPenWidth(qreal width) { textPenWidth = width; needsRecache = true; emit textPenWidthChanged(width); }
   virtual void setTextPenWidthInPixel(bool inPixel) { textPenWidthInPixel=inPixel; needsRecache = true; emit textPenWidthInPixelChanged(inPixel); }
   virtual void setTextSize(qreal sz) { size = qBound(0.5, sz, 1.5); needsRecache = true; emit textSizeChanged(sz); }
   virtual void setHAlign(int align) { alh = (HAlign)align; needsRecache = true; emit horzAlignChanged(align); }
   virtual void setVAlign(int align) { alv = (VAlign)align; needsRecache = true; emit vertAlignChanged(align); }
   virtual void setFontName(const QString& fname) { font.setFamily(fname); needsRecache=true; emit fontChanged(fname); }
   virtual void setBold(bool bold) { font.setBold(bold); needsRecache=true; emit boldChanged(bold); }
   virtual void setItalic(bool italic) { font.setItalic(italic); needsRecache= true; emit italicChanged(italic);  }
   virtual void setUnderline(bool underline) { font.setUnderline(underline); needsRecache=true; emit underlineChanged(underline); }
   virtual void setStrikeThrough(bool strike) { font.setStrikeOut(strike); needsRecache=true; emit strikeThroughChanged(strike); }

   virtual void paint(QPainter& p) override;

signals:
   void captionChanged(QString caption);
   void rectVisibleChanged(bool visible);
   void textPenColorChanged(QColor color);
   void textBrushColorChanged(QColor color);
   void textPenWidthInPixelChanged(bool inPixel);
   void textPenWidthChanged(qreal width);
   void textSizeChanged(qreal size);
   void horzAlignChanged(int hAlign);
   void vertAlignChanged(int vAlign);
   void fontChanged(QString fontName);
   void boldChanged(bool bold);
   void italicChanged(bool italic);
   void underlineChanged(bool underline);
   void strikeThroughChanged(bool strikeThrough);

protected slots:
   void onCTPChanged() { emit textPenColorChanged(ctp.asQColor()); }
   void onCTBChanged() { emit textBrushColorChanged(ctb.asQColor()); }

protected:
   void createPainterPath();

   QString text;
   QPainterPath pp;
   QRectF clip;
   QFont   font;
   QPen textPen;
   QBrush textBrush;
   Color  ctp, ctb;
   qreal size; // default: 1.0; range 0.5..1.5
               // should not be smaller than 0.5 to avoid loss of precision
               // should not exceed 1.5 to avoid clipping
   HAlign alh;
   VAlign alv;
   qreal textPenWidth;
   bool textPenWidthInPixel;
   bool drawRect;
   bool needsRecache;
};

} // namespace ecs

#endif // ECS_TEXT_H
