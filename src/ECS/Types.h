#ifndef ECS_POINT_H
#define ECS_POINT_H

#include <QtGlobal>
#include <QPainter>

namespace ecs {

extern qreal smallest(qreal v1, qreal v2, qreal v3, qreal v4);
extern qreal largest(qreal v1, qreal v2, qreal v3, qreal v4);

class Point
{
public:
   Point() : pt(QPointF()) {}
   Point(qreal x, qreal y) : pt(QPointF(x, y)) {}
   Point(const QPointF& p) : pt(p) {}
   Point(const Point& p) : pt(QPointF(p.x(), p.y())) {}
   Point operator=(const Point& other) { pt = other.pt; return *this; }

   qreal x() const { return pt.x();}
   qreal y() const { return pt.y();}
   void setX(qreal x) { pt.setX(x);}
   void setY(qreal y) { pt.setY(y);}

protected:
   QPointF pt;
};

class Color : public QObject
{
   Q_OBJECT
public:
   enum class buddyKind {
      none,
      pen,
      brush
   } ;

   Color() : buddy(nullptr), col(QColor()) {};
   Color(qreal r, qreal g, qreal b, qreal a=1.0) : buddy(nullptr), col(QColor(r*255, g*255, b*255, a*255)) {}
   Color(const char* name) : buddy(nullptr), col(QColor(name)) {}
   Color(const QString& name) : buddy(nullptr), col(QColor(name)) {}
   Color(const Color& init) : QObject(), buddy(init.buddy), col(init.col) {}
   Color(const QColor& init) : buddy(nullptr), col(init) {}

   Color& operator=(const Color& other) { col=other.col; toBuddy(); return *this; }
   Color& operator=(const QColor& other) {col = other; toBuddy(); return *this; }

   void setBuddy(buddyKind k, void* b) { kind = k; buddy = b; fromBuddy(); }

   qreal getR() const { return col.redF(); }
   qreal getG() const { return col.greenF(); }
   qreal getB() const { return col.blueF(); }
   qreal getA() const { return col.alphaF(); }

   void setR(qreal r) { col.setRedF(qBound(0.0, r, 1.0)); toBuddy(); }
   void setG(qreal g) { col.setGreenF(qBound(0.0, g, 1.0)); toBuddy(); }
   void setB(qreal b) { col.setBlueF(qBound(0.0, b, 1.0)); toBuddy(); }
   void setA(qreal a) { col.setAlphaF(qBound(0.0, a, 1.0)); toBuddy(); }

   void setColor(const char* name) { col.setNamedColor(name); toBuddy(); }
   void setRGB(qreal r, qreal g, qreal b) {
      col.setRedF(qBound(0.0, r, 1.0));
      col.setGreenF(qBound(0.0, g, 1.0));
      col.setBlueF(qBound(0.0, b, 1.0));
      toBuddy();
   }
   void setRGBA(qreal r, qreal g, qreal b, qreal a) {
      col.setRedF(qBound(0.0, r, 1.0));
      col.setGreenF(qBound(0.0, g, 1.0));
      col.setBlueF(qBound(0.0, b, 1.0));
      col.setAlphaF(qBound(0.0, a, 1.0));
      toBuddy();
   }

   QColor asQColor() const { return col; }

signals:
   void colorChanged();

private:
   void* buddy;
   QColor col;
   buddyKind kind = buddyKind::none;

   void fromBuddy() {
      if (buddy && kind>buddyKind::none) {
         switch (kind) {
            case buddyKind::pen:
               col = ((QPen*)buddy)->color();
               break;
            case buddyKind::brush:
               col = ((QBrush*)buddy)->color();
               break;
            default: ;
         }
      }
   }

   void toBuddy() {
      if (buddy && kind>buddyKind::none) {
         switch (kind) {
            case buddyKind::pen:
               ((QPen*)buddy)->setColor(col);
               break;
            case buddyKind::brush:
               ((QBrush*)buddy)->setColor(col);
               break;
            default: ;
         }
      }
      emit colorChanged();
   }
};

} // namespace ecs

#endif // ECS_POINT_H
