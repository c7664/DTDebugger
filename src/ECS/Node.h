#ifndef NODE_H
#define NODE_H

#include <QTransform>
#include <QtGlobal>
#include <vector>
#include "../dbg_ui/dbgController.h"
#include <dukglue/dukglue.h>
#include "math.h"
#include "Types.h"

namespace ecs {

#define id quint32
#define index_t qint32
#define num_t quint32
#define EPS 1e-12

enum class NodeClass {
	Node,
	Rect,
	RoundRect,
	Text,
	Image,
	__count__
};


extern const char* NodeClassNames[(int)NodeClass::__count__];
extern QPixmap* NodeClassIcons[(int)NodeClass::__count__];

class Rect;
class RoundRect;
class Text;
class Image;

class Node : public QObject
{
	Q_OBJECT
public:
	Node();
	virtual ~Node();
	static void registerScripting(duk_context *ctx);
	virtual NodeClass nodeClass() const { return NodeClass::Node; }
	virtual void paint(QPainter& p) { Q_UNUSED(p) }
	virtual QRectF getAABB();

	const char* NodeClassName() const { return NodeClassNames[NodeClassInt()]; }
	int NodeClassInt() const { return static_cast<int>(nodeClass()); }
	QPixmap* getIcon() { return NodeClassIcons[NodeClassInt()]; }

	void setNameCStr(const char* Name) { name = QString::fromUtf8(Name); emit nameChanged(name); }
	const char* getNameCStr() const { return name.toUtf8().data(); }
	const QString& getName() const { return name; }

	static Node* createNode(NodeClass cls, const QString& name=QString());
	Node* getParent() { return parent; }
	int addChild(Node* node);
	bool delChild(index_t idx);
	bool delChild(Node* child);
	Node* child(index_t idx);
	Node* operator[](index_t idx) { return child(idx); }
	num_t count() const { return (num_t)children.size(); }
	index_t indexOf(Node* child) const;
	void clear();
	void moveUp();
	void moveDown();
	bool canMoveUp();
	bool canMoveDown();

	// no overloads in scripting; therefore:
	bool dcn(Node* child) { return delChild(child); }
	bool dci(index_t idx) { return delChild(idx); }


	QMatrix& getMatrix() { if (dirty) calcMatrix(); return M; }

	// visibillity
	bool getVisible() const { return visible; }

	// pivot
	qreal getPivotX() const { return pivot.x(); }
	qreal getPivotY() const { return pivot.y(); }
	QPointF getPivot() const { return pivot; }
	QPointF* getPivotScript() { return &pivot; }

	// translation
	qreal getX() const { return pos.x(); }
	qreal getY() const { return pos.y(); }
	QPointF getPos() const { return pos; }
	QPointF* getPosScript() { return &pos; }

	// rotation
	qreal getRotDeg() const { return rot; }
	qreal getRotRad() const { return rot * M_PI/180.0; }

	// scale
	qreal getScaleX() const { return scale.x(); }
	qreal getScaleY() const { return scale.y(); }
	QPointF getScale() const { return scale; }
	QPointF* getScaleScript() { return &scale; }

	// scripting
	Rect* asRect() { return (Rect*)this; }
	RoundRect* asRoundRect() { return (RoundRect*)this; }
	Text* asText() { return (Text*)this; }
	Image* asImage() { return (Image*)this; }

	void doPaint(QPainter& p, const QTransform & t);
	virtual void calcMatrix();

	static bool paintPivot;
	static bool paintAABB;

public slots:
	virtual void setName(const QString& newName) { setNameCStr(newName.toUtf8().data()); }
	virtual void setVisible(bool vis) { visible = vis; emit visibilityChanged(vis); }
	virtual void setPivotX(qreal x);
	virtual void setPivotY(qreal y);
	virtual void setPivot(const QPointF& p);
	virtual void setPivotXY(qreal x, qreal y) { setPivot(QPointF(x, y)); }
	virtual void setX(qreal x);
	virtual void setY(qreal y);
	virtual void setPos(const QPointF& p);
	virtual void setXY(qreal x, qreal y) { setPos(QPointF(x, y)); }
	virtual void setRotDeg(qreal rotDeg);
	virtual void setRotRad(qreal rotRad);
	virtual void setScaleX(qreal sx);
	virtual void setScaleY(qreal sy);
	virtual void setScale(const QPointF& s);
	virtual void setScaleXY(qreal x, qreal y) { setScale(QPointF(x, y)); }
	virtual void setUniformScale(qreal s) { setScale(QPointF(s, s)); } // uniform scaling

signals:
	void nameChanged(QString newName);
	void visibilityChanged(bool visible);
	void pivotXChanged(qreal x);
	void pivotYChanged(qreal y);
	void translationXChanged(qreal x);
	void translationYChanged(qreal y);
	void scaleXChanged(qreal x);
	void scaleYChanged(qreal y);
	void rotationDegChanged(qreal rotDeg);

protected:
	static QColor colPivot;
	static QColor colAABB;

	void setDirty();
	void loadIcons();

	bool dirty = false;
	QString name;
	Node *parent = nullptr;
	QPointF pos;
	QPointF pivot, scale;
	qreal rot;
	QMatrix M;
	bool visible;
	std::vector<Node*> children;
};

}

Q_DECLARE_METATYPE(ecs::Node*)


#endif // NODE_H


