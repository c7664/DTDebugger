#ifndef CONSTS_H
#define CONSTS_H

#include <QPainter>

namespace ecs {
namespace priv {

// debug-drawing
const bool paintPivot = false;
const bool paintAABB = false;
const QColor colPivot = QColor(128, 255, 128, 255);
const QColor colAABB = QColor(255, 128, 128, 255);

// Monitor
const QColor colMonitorBG = QColor(0, 0, 0, 255);
const QColor colMonitorOrg = QColor(64, 64, 64, 255);

// rects
const QColor colRectPen = QColor(255, 255, 255, 255);
const QColor colRectBrush = QColor(32, 32, 128, 255);
const qreal penWidth = 1.5;
const bool penWidthInPixel = false;


}
}

#endif // CONSTS_H
