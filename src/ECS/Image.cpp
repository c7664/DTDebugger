#include "Image.h"

namespace ecs {

QStringList Image::filters;

void Image::registerScripting(duk_context *ctx)
{
	dukglue_set_base_class<ecs::Node, ecs::Image>(ctx);
	dukglue_register_constructor<ecs::Image>(ctx, "Image");
	dukglue_register_method(ctx, &Node::asImage, "AsImage");

	dukglue_register_property(ctx, &Image::getPathCStr, &Image::setPathCStr, "Path");
	dukglue_register_property(ctx, &Image::getOpacity, &Image::setOpacity, "Opacity");
	dukglue_register_property(ctx, &Image::getHInvert, &Image::setHInvert, "HInvert");
	dukglue_register_property(ctx, &Image::getVInvert, &Image::setVInvert, "VInvert");
}

const QStringList& Image::getFilters()
{
	if (filters.isEmpty()) {
		filters << "Image files (*.png *.xpm *.jpg *.jpeg *.bmp *.svg *.gif)"
				  << "PNG files (*.png)"
				  << "Bitmap files (*.bmp)"
				  << "JPEG files (*.jpg *.jpeg)"
				  << "Gif files (*.gif)"
				  << "SVG files (*.svg)"
				  << "XPM files (*.xpm)"
				  << "Any file (*)";
	}
	return filters;
}

QString Image::getInfo()
{
	if (newFilename)
		 createPixmap();
	QString res;
	if (pm)
		res = QString::number(pm->width())+"x"+QString::number(pm->height())+"px; "+QString::number(pm->depth())+"bpp";
	else
		res = "-";
	return res;
}

Image::Image() : Node(), pm(nullptr), needsRecache(true)
{
	opacity = 1.0;
	vInvert = hInvert = false;
	newFilename = false;
}

Image::~Image()
{
	if (pm)
		delete pm;
}

void Image::paint(QPainter &p)
{
	if (needsRecache)
		createPixmap();
	if (!pm)
		return;

	p.setOpacity(opacity);
	p.drawPixmap(QPointF(0.0, 0.0), *pm);
	p.setOpacity(1.0);
}

void Image::createPixmap()
{
	if (pm)
		delete pm;
	pm = new QPixmap(dir.absolutePath());
	if (!pm)
		return;
	if (hInvert || !vInvert) {
		QTransform t;
		if (hInvert)
			t = t.scale(-1.0, 1.0);
		if (!vInvert)
			t = t.scale(1.0, -1.0);
		QPixmap* tmp = pm;
		pm = new QPixmap(tmp->transformed(t, Qt::SmoothTransformation));
		delete tmp;
	}
	if (newFilename) {
		setPivot(QPointF((qreal)pm->width() * 0.5, (qreal)pm->height() * 0.5));
		setPos(QPointF(-(qreal)pm->width() * 0.5, -(qreal)pm->height() * 0.5));
		newFilename = false;
		emit infoChanged(getInfo());
	}
	needsRecache=false;
}

} // namespace ecs
