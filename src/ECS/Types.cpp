#include "Types.h"

namespace ecs {

qreal smallest(qreal v1, qreal v2, qreal v3, qreal v4)
{
   return std::min(v1, std::min(v2, std::min(v3, v4)));
}

qreal largest(qreal v1, qreal v2, qreal v3, qreal v4)
{
   return std::max(v1, std::max(v2, std::max(v3, v4)));
}

} // namespace ecs
