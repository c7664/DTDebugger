#include <QPainter>
#include "Node.h"
#include "Types.h"
#include "Consts.h"
#include "Rect.h"
#include "RoundRect.h"
#include "Text.h"
#include "Image.h"


namespace ecs {

const char* NodeClassNames[(int)NodeClass::__count__] =
{
		"Node",
		"Rect",
		"RoundRect",
		"Text",
		"Image"
};

QPixmap* NodeClassIcons[(int)NodeClass::__count__] =
{
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr
};

void Node::loadIcons()
{
	NodeClassIcons[(int)NodeClass::Node] =      new QPixmap(":/res/dk/res/dk/node.svg");
	NodeClassIcons[(int)NodeClass::Rect] =      new QPixmap(":/res/dk/res/dk/nodeRect.svg");
	NodeClassIcons[(int)NodeClass::RoundRect] = new QPixmap(":/res/dk/res/dk/nodeRoundRect.svg");
	NodeClassIcons[(int)NodeClass::Text] =      new QPixmap(":/res/dk/res/dk/nodeText.svg");
	NodeClassIcons[(int)NodeClass::Image] =     new QPixmap(":/res/dk/res/dk/nodeImage.svg");
}


void Node::registerScripting(duk_context *ctx)
{
	// Color
	dukglue_register_constructor<Color>(ctx, "Color");
// dukglue_register_constructor<QColor, int, int, int, int>(ctx, "Color");
// dukglue_register_constructor<QColor, const char*>(ctx, "Color");
// dukglue_register_constructor<QColor, QColor&>(ctx, "Color");
	dukglue_register_property(ctx, &Color::getR, &Color::setR, "R");
	dukglue_register_property(ctx, &Color::getG, &Color::setG, "G");
	dukglue_register_property(ctx, &Color::getB, &Color::setB, "B");
	dukglue_register_property(ctx, &Color::getA, &Color::setA, "A");
	dukglue_register_method(ctx, &Color::setColor, "setNamedColor");
	dukglue_register_method(ctx, &Color::setRGB, "setRGB");
	dukglue_register_method(ctx, &Color::setRGBA, "setRGBA");

	// Point
	dukglue_register_constructor<QPointF>(ctx, "Point");
	dukglue_register_constructor<QPointF, qreal, qreal>(ctx, "Point");
	dukglue_register_method(ctx, &QPointF::x, "x");
	dukglue_register_method(ctx, &QPointF::y, "y");
	dukglue_register_property(ctx, nullptr, &QPointF::setX, "X");
	dukglue_register_property(ctx, nullptr, &QPointF::setY, "Y");

	// type/typename/name
	dukglue_register_property(ctx, &Node::NodeClassInt, nullptr, "ClassType");
	dukglue_register_property(ctx, &Node::NodeClassName, nullptr, "ClassName");
	dukglue_register_property(ctx, &Node::getNameCStr, &Node::setNameCStr, "Name");

	// children
	dukglue_register_property(ctx, &Node::count, nullptr, "Count");
	dukglue_register_method(ctx, &Node::addChild, "addChild");
	dukglue_register_method(ctx, &Node::dcn, "delChildNode");
	dukglue_register_method(ctx, &Node::dci, "delChildIndex");
	dukglue_register_method(ctx, &Node::child, "child");
	dukglue_register_method(ctx, &Node::indexOf, "indexOf");
	dukglue_register_method(ctx, &Node::clear, "clear");

	// visibillity
	dukglue_register_property(ctx, &Node::getVisible, &Node::setVisible, "Visible");

	// pivot
	dukglue_register_property(ctx, &Node::getPivotX, &Node::setPivotX, "PivotX");
	dukglue_register_property(ctx, &Node::getPivotY, &Node::setPivotY, "PivotY");
	dukglue_register_property(ctx, &Node::getPivotScript, &Node::setPivot, "Pivot");
	dukglue_register_method(ctx, &Node::setPivotXY, "setPivot");

	// translation
	dukglue_register_property(ctx, &Node::getX, &Node::setX, "X");
	dukglue_register_property(ctx, &Node::getY, &Node::setY, "Y");
	dukglue_register_property(ctx, &Node::getPosScript, &Node::setPos, "Pos");
	dukglue_register_method(ctx, &Node::setXY, "setPos");

	// rotation
	dukglue_register_property(ctx, &Node::getRotDeg, &Node::setRotDeg, "RotDeg");
	dukglue_register_property(ctx, &Node::getRotRad, &Node::setRotRad, "RotRad");

	// scale
	dukglue_register_property(ctx, &Node::getScaleScript, &Node::setScale, "Scale");
	dukglue_register_property(ctx, &Node::getScaleX, &Node::setScaleX, "ScaleX");
	dukglue_register_property(ctx, &Node::getScaleY, &Node::setScaleY, "ScaleY");
	dukglue_register_property(ctx, nullptr, &Node::setUniformScale, "UniformScale");
	dukglue_register_method(ctx, &Node::setScaleXY, "setScale");
}


bool Node::paintPivot = priv::paintPivot;
bool Node::paintAABB = priv::paintAABB;
QColor Node::colPivot = priv::colPivot;
QColor Node::colAABB = priv::colAABB;

bool nonZero(qreal v)
{
	return abs(v) > EPS;
}

Node::Node() :
	dirty(true),
	pos(QPointF(0.0, 0.0)),
	pivot(QPointF(0.0, 0.0)),
	scale(QPointF(1.0, 1.0)),
	rot(0.0),
	visible(true)
{
	if (!NodeClassIcons[0])
		loadIcons();
}

Node::~Node()
{
	clear();
}

// static:
Node *Node::createNode(NodeClass cls, const QString &name)
{
	Node* res = nullptr;
	switch (cls) {
		case NodeClass::Node:      res = new Node; break;
		case NodeClass::Rect:      res = new Rect; break;
		case NodeClass::RoundRect: res = new RoundRect; break;
		case NodeClass::Text:      res = new Text; break;
		case NodeClass::Image:     res = new Image; break;
		default: assert(false);
	}
	res->setName(name);
	return res;
}

QRectF Node::getAABB()
{
	if (dirty)
		calcMatrix();
	QPointF p1 = M.map(QPointF(-0.5, -0.5));
	QPointF p2 = M.map(QPointF(-0.5,  0.5));
	QPointF p3 = M.map(QPointF( 0.5,  0.5));
	QPointF p4 = M.map(QPointF( 0.5, -0.5));
	QPointF q1 = QPointF(smallest(p1.x(), p2.x(), p3.x(), p4.x()), smallest(p1.y(), p2.y(), p3.y(), p4.y()));
	QPointF q2 = QPointF(largest(p1.x(), p2.x(), p3.x(), p4.x()), largest(p1.y(), p2.y(), p3.y(), p4.y()));
	QRectF res(q1, q2);
	return res;
}

void Node::calcMatrix()
{
	QMatrix m1 = QMatrix().translate(-pivot.x(), -pivot.y());
	QMatrix m2 = QMatrix().scale(scale.x(), scale.y());
	QMatrix m3 = QMatrix().rotate(rot);
	QMatrix m4 = QMatrix().translate(pivot.x() + pos.x(), pivot.y() + pos.y());
	if (parent)
		M = m1 * m2 * m3 * m4  * parent->getMatrix();
	else
		M = m1 * m2 * m3 * m4;
}

void Node::setDirty()
{
	dirty = true;
	for (int i = 0; i < (int)count(); i++)
		children[i]->setDirty();
}

int Node::addChild(Node *node)
{
	if (node) {
		node->parent = this;
		children.push_back(node);
		return (int)children.size()-1;
	} else
		return -1;
}

bool Node::delChild(qint32 idx)
{
	if (idx >= 0 && idx < (index_t)children.size()) {
		delete children[idx];
		children.erase(children.begin() + idx);
		return true;
	}
	return false;
}

bool Node::delChild(Node *child)
{
	return delChild(indexOf(child));
}

Node *Node::child(index_t idx)
{
	if (idx >= 0 && idx < (index_t)children.size())
		return children.at(idx);
	return nullptr;
}

index_t Node::indexOf(Node *child) const
{
	for (index_t i = 0; i < (index_t)count(); i++)
		if (children[i] == child)
			return i;
	return -1;
}

void Node::clear()
{
	for (index_t i = 0; i < (index_t)count(); i++)
		delete children[i];
	children.clear();
}

void Node::moveUp()
{
	if (!parent)
		return;

	int idx = parent->indexOf(this);
	if (idx>0) {
		Node* tmp = parent->children[idx-1];
		parent->children[idx-1] = parent->children[idx];
		parent->children[idx] = tmp;
	}
}

void Node::moveDown()
{
	if (!parent)
		return;

	int idx = parent->indexOf(this);
	if (idx<(int)parent->children.size()-1) {
		Node* tmp = parent->children[idx+1];
		parent->children[idx+1] = parent->children[idx];
		parent->children[idx] = tmp;
	}
}

bool Node::canMoveUp()
{
	if (!parent)
		return false;

	int idx = parent->indexOf(this);
	return (idx>0);
}

bool Node::canMoveDown()
{
	if (!parent)
		return false;

	int idx = parent->indexOf(this);
	return (idx>=0 && idx<(int)parent->count()-1);
}

void Node::setPivotX(qreal x)
{
	qreal v = pivot.x() - x;
	if (nonZero(v)) {
		pivot.setX(x);
		setDirty();
		emit pivotXChanged(x);
	}
}

void Node::setPivotY(qreal y)
{
	qreal v = pivot.y() - y;
	if (nonZero(v)) {
		pivot.setY(y);
		setDirty();
		emit pivotYChanged(y);
	}
}

void Node::setPivot(const QPointF &p)
{
	if (nonZero(pivot.x() - p.x()) || nonZero(pivot.y() - p.y())) {
		pivot = p;
		setDirty();
		emit pivotXChanged(p.x());
		emit pivotYChanged(p.y());
	}
}

void Node::setX(qreal x)
{
	qreal v = pos.x() - x;
	if (nonZero(v)) {
		pos.setX(x);
		setDirty();
		emit translationXChanged(x);
	}
}

void Node::setY(qreal y)
{
	qreal v = pos.y() - y;
	if (nonZero(v)) {
		pos.setY(y);
		setDirty();
		emit translationYChanged(y);
	}
}

void Node::setPos(const QPointF &newpos)
{
	if (nonZero(pos.x() - newpos.x()) || nonZero(pos.y() - newpos.y())) {
		pos = newpos;
		setDirty();
		emit translationXChanged(pos.x());
		emit translationYChanged(pos.y());
	}
}

qreal wrap360(qreal deg)
{
	if (deg >= 0.0)
		return fmod(deg, 360.0);
	else
		return 360.0 - fmod(-deg, 360);
}

void Node::setRotDeg(qreal rotDeg)
{
	qreal v = wrap360(rotDeg);
	if (nonZero(v-rot)) {
		rot = v;
		setDirty();
		emit rotationDegChanged(v);
	}
}

void Node::setRotRad(qreal rotRad)
{
	qreal v = wrap360(rotRad * 180.0/M_PI);
	if (nonZero(v-rot)) {
		rot = v;
		setDirty();
		emit rotationDegChanged(v);
	}
}

void Node::setScaleX(qreal sx)
{
	qreal v = sx-scale.x();
	if (nonZero(v)) {
		scale.setX(sx);
		setDirty();
		emit scaleXChanged(sx);
	}
}

void Node::setScaleY(qreal sy)
{
	qreal v = sy-scale.y();
	if (nonZero(v)) {
		scale.setY(sy);
		setDirty();
		emit scaleYChanged(sy);
	}
}

void Node::setScale(const QPointF& s)
{
	if (nonZero(s.x() - scale.x()) || nonZero(s.y() - scale.y())) {
		scale = s;
		setDirty();
		emit scaleXChanged(s.x());
		emit scaleYChanged(s.y());
	}
}

void Node::doPaint(QPainter &p, const QTransform & t)
{
	if (!visible)
		return;
	if (dirty)
		calcMatrix();
	p.setTransform(QTransform(M) * t);
	paint(p);
	QPointF p1;
	if (paintPivot || paintAABB)
		p.setTransform(t);

	if (paintPivot) {
		QPen pn(colPivot);
		pn.setWidth(0); // cosmetic pen; always 1 pixel width
		p.setPen(pn);
		p1 = M.map(pivot);
		qreal rx;
//      rx = 80.0 / p.device()->width();
		rx = 10.0;
		p.drawLine(QLineF(p1.x() - rx, p1.y(), p1.x() + rx, p1.y() ));
		p.drawLine(QLineF(p1.x(), p1.y() - rx, p1.x(), p1.y() + rx));
	}
	if (paintAABB) {
		QPen pn(colAABB);
		pn.setWidth(0); // cosmetic pen; always 1 pixel width
		p.setPen(pn);
		p.setBrush(Qt::NoBrush);
		p.drawRect(getAABB());
	}
	for (auto& it : children)
		it->doPaint(p, t);
}

}
