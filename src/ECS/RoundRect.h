#ifndef ROUNDRECT_H
#define ROUNDRECT_H

#include <QPainter>
#include "Node.h"
#include "../dbg_ui/dbgController.h"
#include <dukglue/dukglue.h>

namespace ecs {

class RoundRect : public Node
{
   Q_OBJECT
public:
   RoundRect();
   static void registerScripting(duk_context *ctx);
   virtual QRectF getAABB() override;
   virtual NodeClass nodeClass() const override { return NodeClass::RoundRect; }

   qreal getP1X() const { return p1.x(); }
   qreal getP1Y() const { return p1.y(); }
   qreal getP2X() const { return p2.x(); }
   qreal getP2Y() const { return p2.y(); }
   const QPointF& getP1() const { return p1; }
   const QPointF& getP2() const { return p2; }
   qreal getWidth() const { return abs(p2.x() - p1.x()); }
   qreal getHeight() const { return abs(p2.y() - p1.y()); }

   Color* getPenColor() { return &cp; }
   Color* getBrushColor() { return &cb; }
   bool getPenWidthInPixel() const { return pen.isCosmetic(); }
   qreal getPenWidth() const { return pen.widthF(); }

   qreal getRX() const { return rx; }
   qreal getRY() const { return ry; }

public slots:
   virtual void setP1X(qreal x) { p1.setX(x); emit p1xChanged(x); }
   virtual void setP1Y(qreal y) { p1.setY(y); emit p1yChanged(y); }
   virtual void setP2X(qreal x) { p2.setX(x); emit p2xChanged(x); }
   virtual void setP2Y(qreal y) { p2.setY(y); emit p2yChanged(y); }
   virtual void setP1(const QPointF& pos) { p1 = pos; emit p1xChanged(pos.x()); emit p1yChanged(pos.y()); }
   virtual void setP2(const QPointF& pos) { p2 = pos; emit p2xChanged(pos.x()); emit p2yChanged(pos.y()); }
   virtual void setWidth(qreal w) { p2.setX(p1.x() + w); emit p2xChanged(p2.x()); }
   virtual void setHeight(qreal h) { p2.setY(p1.y() + h); emit p2yChanged(p2.y()); }
   virtual void setPenColor(const Color& col) { cp=col; emit penColorChanged(col.asQColor()); }
   virtual void setBrushColor(const Color& col) { cb=col; brush.setStyle(Qt::BrushStyle::SolidPattern); emit brushColorChanged(col.asQColor()); }
   virtual void setPenQColor(const QColor& col) { setPenColor(Color(col)); }
   virtual void setBrushQColor(const QColor& col) { setBrushColor(Color(col)); }
   virtual void setPenColorName(const QString& colName) { setPenColor(QColor(colName)); }
   virtual void setBrushColorName(const QString& colName) { setBrushColor(QColor(colName)); }
   virtual void setPenWidthInPixel(bool inPixel) { pen.setCosmetic(inPixel); emit penWidthInPixelChanged(inPixel); }
   virtual void setPenWidth(qreal width) { pen.setWidthF(width);emit penWidthChanged(width);  }
   virtual void setRX(qreal RX) { rx = RX; emit rxChanged(rx); }
   virtual void setRY(qreal RY) { ry = RY; emit ryChanged(ry); }

   virtual void paint(QPainter& p) override;

signals:
   void p1xChanged(qreal x);
   void p1yChanged(qreal y);
   void p2xChanged(qreal x);
   void p2yChanged(qreal y);
   void penColorChanged(QColor color);
   void brushColorChanged(QColor color);
   void penWidthInPixelChanged(bool inPixel);
   void penWidthChanged(qreal pw);
   void rxChanged(qreal rx);
   void ryChanged(qreal ry);


protected slots:
   void onCPChanged();
   void onCBChanged();

protected:
   QPen pen;
   QBrush brush;
   qreal rx, ry;
   QPointF p1, p2;
   Color cp, cb;
};

}

#endif // ROUNDRECT_H
