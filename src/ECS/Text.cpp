#include "Text.h"

namespace ecs {

void Text::registerScripting(duk_context *ctx)
{
   dukglue_set_base_class<ecs::RoundRect, ecs::Text>(ctx);
   dukglue_register_constructor<ecs::Text>(ctx, "Text");
   dukglue_register_method(ctx, &Node::asText, "AsText");

   dukglue_register_property(ctx, &Text::getCaptionCStr, &Text::setCaptionCStr, "Caption");
   dukglue_register_property(ctx, &Text::getRectVisible, &Text::setRectVisible, "RectVisible");
   dukglue_register_property(ctx, &Text::getTextPenColor, &Text::setTextPenColor, "TextPenColor");
   dukglue_register_property(ctx, &Text::getTextBrushColor, &Text::setTextBrushColor, "TextBrushColor");
   dukglue_register_property(ctx, &Text::getTextPenWidth, &Text::setTextPenWidth, "TextPenWidth");
   dukglue_register_property(ctx, &Text::getTextPenWidthInPixel, &Text::setTextPenWidthInPixel, "TextPenWidthInPixel");
   dukglue_register_property(ctx, &Text::getTextSize, &Text::setTextSize, "TextSize");
   dukglue_register_property(ctx, &Text::getHAlign, &Text::setHAlign, "HorzAlign");
   dukglue_register_property(ctx, &Text::getVAlign, &Text::setVAlign, "VertAlign");
   dukglue_register_property(ctx, &Text::getFontNameCStr, &Text::setFontNameCStr, "FontName");
   dukglue_register_property(ctx, &Text::getBold, &Text::setBold, "Bold");
   dukglue_register_property(ctx, &Text::getItalic, &Text::setItalic, "Italic");
   dukglue_register_property(ctx, &Text::getUnderline, &Text::setUnderline, "Underline");
   dukglue_register_property(ctx, &Text::getStrikeThrough, &Text::setStrikeThrough, "StrikeThrough");
}

Text::Text() : RoundRect()
{
   name = "Untitled " + QString::fromUtf8(NodeClassName());

   setCaptionCStr("Untitled");
   setHAlign((int)HAlign::alhLeft);
   setVAlign((int)VAlign::alvCenter);
   p1.setX(-300.0);
   p2.setX(300.0);
   size = 1.0;
   font.setBold(true);
   ctp.setBuddy(Color::buddyKind::pen, &textPen);
   ctb.setBuddy(Color::buddyKind::brush, &textBrush);
   ctp.setRGBA(0.0, 0.0, 0.0, 1.0);
   ctb.setRGBA(1.0, 1.0, 1.0, 1.0);
   cb.setRGBA(0.0, 0.3, 0.0, 1.0);
   textBrush.setStyle(Qt::SolidPattern);
   textPen.setJoinStyle(Qt::MiterJoin);
   textPenWidthInPixel = false;
   textPenWidth = 2.5;
   drawRect = true;
   connect(&ctp, &Color::colorChanged, this, &Text::onCTPChanged);
   connect(&ctb, &Color::colorChanged, this, &Text::onCTBChanged);
}

Text::~Text()
{
}

void Text::paint(QPainter &p)
{
   if (drawRect)
      RoundRect::paint(p);

   if (needsRecache) {
      createPainterPath();
      needsRecache = false;
   }

   textPen.setWidthF(textPenWidth);
   textPen.setCosmetic(textPenWidthInPixel);
   p.setPen(textPen);
   p.setBrush(textBrush);

   p.setClipRect(clip);
   QTransform t, tmp;
   tmp = p.transform();
   t.scale(1.0, -1.0);
   p.setTransform(t, true);
   p.setClipping(true);
   p.drawPath(pp);
   p.setClipping(false);
   p.setTransform(tmp);
}

Qt::Alignment Text::getQtAlignment() const
{
   Qt::Alignment res = 0;
   switch (alh) {
      case HAlign::alhLeft:
         res |= Qt::AlignLeft;
         break;
      case HAlign::alhRight:
         res |= Qt::AlignRight;
         break;
      case HAlign::alhCenter:
         res |= Qt::AlignHCenter;
         break;
      default:
         res |= Qt::AlignLeft;
         break;
   }
   switch (alv) {
      case VAlign::alvTop:
         res |= Qt::AlignTop;
         break;
      case VAlign::alvBottom:
         res |= Qt::AlignBottom;
         break;
      case VAlign::alvCenter:
         res |= Qt::AlignVCenter;
         break;
      default:
         res |= Qt::AlignVCenter;
         break;
   }

   return res;
}

void Text::setQtAlignment(Qt::Alignment align)
{
   if (align & Qt::AlignHCenter)
      alh = HAlign::alhCenter;
   else if (align & Qt::AlignRight)
      alh = HAlign::alhRight;
   else
      alh = HAlign::alhLeft;

   if (align & Qt::AlignTop)
      alv = VAlign::alvTop;
   else if (align & Qt::AlignBottom)
      alv = VAlign::alvBottom;
   else
      alv = VAlign::alvCenter;

   needsRecache = true;
}




void Text::createPainterPath()
{
   clip = QRectF(p1, p2);

   qreal fh = (qreal)clip.height()*0.4*size;
   font.setPointSizeF(fh);
   QFontMetrics m(font);
   //QRect rct = m.boundingRect(text);
   qreal h2 = m.descent();
   qreal w = m.boundingRect(text).width();
   qreal h = m.capHeight();
   qreal x, y, padX;
   padX = (qreal)clip.width() * 0.015;
   switch (alh) {
      case HAlign::alhLeft:
         x = p1.x() + padX;
         break;
      case HAlign::alhRight:
         x = p2.x() - w - padX;
         break;
      case HAlign::alhCenter:
         x = p1.x() + ((qreal)clip.width() - w) * 0.5;
         break;
      default:
         x = p1.x();
         break;
   }
   switch (alv) {
      case VAlign::alvBottom:
         y = -h2 * 1.4 - p1.y();
         break;
      case VAlign::alvTop:
         y = h * 1.4 - p2.y();
         break;
      case VAlign::alvCenter:
      default:
         y =  - p1.y() - ((qreal)clip.height() - h) * 0.5;
         break;
   }

   pp.clear();
   pp.addText(x, y, font, text);
}




} // namespace ecs
