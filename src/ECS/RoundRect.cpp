#include "RoundRect.h"
#include "src/ECS/Consts.h"
namespace ecs {

void RoundRect::registerScripting(duk_context *ctx)
{
   dukglue_set_base_class<ecs::Node, ecs::RoundRect>(ctx);
   dukglue_register_constructor<ecs::RoundRect>(ctx, "RoundRect");
   dukglue_register_method(ctx, &Node::asRoundRect, "AsRoundRect");
   dukglue_register_property(ctx, &RoundRect::getP1X, &RoundRect::setP1X, "P1_X");
   dukglue_register_property(ctx, &RoundRect::getP1Y, &RoundRect::setP1Y, "P1_Y");
   dukglue_register_property(ctx, &RoundRect::getP2X, &RoundRect::setP2X, "P2_X");
   dukglue_register_property(ctx, &RoundRect::getP2Y, &RoundRect::setP2Y, "P2_Y");
   //dukglue_register_property(ctx, &RoundRect::getP1, &RoundRect::setP1, "P1");
   //dukglue_register_property(ctx, &RoundRect::getP2, &RoundRect::setP1, "P2");
   dukglue_register_property(ctx, &RoundRect::getWidth, &RoundRect::setWidth, "Width");
   dukglue_register_property(ctx, &RoundRect::getHeight, &RoundRect::setHeight, "Height");
   dukglue_register_property(ctx, &RoundRect::getPenColor, &RoundRect::setPenColor, "PenColor");
   dukglue_register_property(ctx, &RoundRect::getBrushColor, &RoundRect::setBrushColor, "BrushColor");
   dukglue_register_property(ctx, &RoundRect::getPenWidth, &RoundRect::setPenWidth, "PenWidth");
   dukglue_register_property(ctx, &RoundRect::getPenWidthInPixel, &RoundRect::setPenWidthInPixel, "PenWidthInPixel");
   dukglue_register_property(ctx, &RoundRect::getRX, &RoundRect::setRX, "RX");
   dukglue_register_property(ctx, &RoundRect::getRY, &RoundRect::setRY, "RY");
}

QRectF RoundRect::getAABB()
{
   if (dirty)
      calcMatrix();
   QPointF s1 = M.map(p1);
   QPointF s2 = M.map(QPointF(p1.x(), p2.y()));
   QPointF s3 = M.map(p2);
   QPointF s4 = M.map(QPointF(p2.x(), p1.y()));
   QPointF q1 = QPointF(smallest(s1.x(), s2.x(), s3.x(), s4.x()), smallest(s1.y(), s2.y(), s3.y(), s4.y()));
   QPointF q2 = QPointF(largest(s1.x(), s2.x(), s3.x(), s4.x()), largest(s1.y(), s2.y(), s3.y(), s4.y()));
   QRectF res(q1, q2);
   return res;
}

RoundRect::RoundRect()
{
   name = "Untitled " + QString::fromUtf8(NodeClassName());

   cp.setBuddy(Color::buddyKind::pen, &pen);
   cb.setBuddy(Color::buddyKind::brush, &brush);
   setPenColor(priv::colRectPen);
   setBrushColor(priv::colRectBrush);
   setPenWidth(priv::penWidth);
   setPenWidthInPixel(priv::penWidthInPixel);
   rx = ry = 20.0;
   p1 = QPointF(-50.0, -50.0);
   p2 = QPointF(50.0, 50.0);
   connect(&cp, &Color::colorChanged, this, &RoundRect::onCPChanged);
   connect(&cb, &Color::colorChanged, this, &RoundRect::onCBChanged);
}

void RoundRect::onCPChanged()
{
   emit penColorChanged(cp.asQColor());
}
void RoundRect::onCBChanged()
{
   emit brushColorChanged(cb.asQColor());
}

void RoundRect::paint(QPainter &p)
{
   p.setPen(pen);
   p.setBrush(brush);

   p.drawRoundedRect(QRectF(p1.x(), p1.y(), p2.x()-p1.x(), p2.y()-p1.y()), rx, ry, Qt::AbsoluteSize);
}

}
