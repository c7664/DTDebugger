#ifndef RECT_H
#define RECT_H

#include <QPainter>
#include "Node.h"
#include "../dbg_ui/dbgController.h"
#include <dukglue/dukglue.h>

namespace ecs {

class Rect : public Node
{
   Q_OBJECT
public:
   Rect();
   static void registerScripting(duk_context *ctx);
   virtual QRectF getAABB() override;
   virtual NodeClass nodeClass() const override { return NodeClass::Rect; }

   qreal getP1X() const { return p1.x(); }
   qreal getP1Y() const { return p1.y(); }
   qreal getP2X() const { return p2.x(); }
   qreal getP2Y() const { return p2.y(); }
   const QPointF& getP1() const { return p1; }
   const QPointF& getP2() const { return p2; }
   qreal getWidth() const { return abs(p2.x() - p1.x()); }
   qreal getHeight() const { return abs(p2.y() - p1.y()); }

   Color* getPenColor() { return &cp; }
   Color* getBrushColor() { return &cb; }
   bool getPenWidthInPixel() const { return pen.isCosmetic(); }
   qreal getPenWidth() const { return pen.widthF(); }

public slots:
   virtual void setP1X(qreal x) { p1.setX(x); emit p1xChanged(x); }
   virtual void setP1Y(qreal y) { p1.setY(y); emit p1yChanged(y); }
   virtual void setP2X(qreal x) { p2.setX(x); emit p2xChanged(x); }
   virtual void setP2Y(qreal y) { p2.setY(y); emit p2yChanged(y); }
   virtual void setP1(const QPointF& pos) { p1 = pos; emit p1xChanged(pos.x()); emit p1yChanged(pos.y()); }
   virtual void setP2(const QPointF& pos) { p2 = pos; emit p2xChanged(pos.x()); emit p2yChanged(pos.y()); }
   virtual void setWidth(qreal w) { p2.setX(p1.x() + w); emit p2xChanged(p2.x()); }
   virtual void setHeight(qreal h) { p2.setY(p1.y() + h); emit p2yChanged(p2.y()); }
   virtual void setPenColor(const Color& col);
   virtual void setBrushColor(const Color& col);
   virtual void setPenQColor(const QColor& col) { setPenColor(Color(col)); }
   virtual void setBrushQColor(const QColor& col) { setBrushColor(Color(col)); }
   virtual void setPenColorName(const QString& colName) { QColor c; c.setNamedColor(colName); setPenColor(Color(c)); }
   virtual void setBrushColorName(const QString& colName) { QColor c; c.setNamedColor(colName); setBrushColor(Color(c)); }
   virtual void setPenWidthInPixel(bool inPixel) { pen.setCosmetic(inPixel); emit penWidthInPixelChanged(inPixel);  }
   virtual void setPenWidth(qreal width) { pen.setWidthF(width); emit penWidthChanged(width); }

   virtual void paint(QPainter& p) override;

signals:
   void p1xChanged(qreal p1x);
   void p1yChanged(qreal p1y);
   void p2xChanged(qreal p2x);
   void p2yChanged(qreal p2y);
   void penColorChanged(QColor col);
   void brushColorChanged(QColor col);
   void penWidthInPixelChanged(bool inPixel);
   void penWidthChanged(qreal pw);

protected slots:
   void onCPChanged();
   void onCBChanged();

protected:
   QPen pen;
   QBrush brush;
   QPointF p1, p2;
   Color cp, cb;
};

}

#endif // RECT_H
