#include "dlgAddNode.h"
#include "ECS/Node.h"

dlgAddNode::dlgAddNode(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::dlgAddNode)
{
   ui->setupUi(this);

   for(int i=0; i<(int)ecs::NodeClass::__count__; i++) {
      QPixmap* pm = ecs::NodeClassIcons[i];
      QIcon icon(*pm);
      QString name = QString::fromUtf8(ecs::NodeClassNames[i]);
      QListWidgetItem* it = new QListWidgetItem(icon, name);
      it->setData(Qt::UserRole, i);
      ui->lst->addItem(it);
   }
}

dlgAddNode::~dlgAddNode()
{
   delete ui;
}

void dlgAddNode::changeEvent(QEvent *e)
{
   QDialog::changeEvent(e);
   switch (e->type()) {
      case QEvent::LanguageChange:
         ui->retranslateUi(this);
         break;
      default:
         break;
   }
}
