#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QFont>
#include <QSettings>
#include <QLabel>
#include <QThread>
#include <QInputDialog>
#include <QMetaMethod>
#include <dukglue/dukglue.h>
#include <dukglue/dukexception.h>
#include "dbg_ui/dbgController.h"
#include "ECS/Node.h"
#include "ECS/Rect.h"
#include "ECS/RoundRect.h"
#include "ECS/Text.h"
#include "dlgAddNode.h"


MainWindow* mainWnd;

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	mainWnd = this;
	ui->setupUi(this);

	initDebugController();
	initDockWidgets();
	initStatusBar();
	initInspector();
	initSceneTree();

	connect(ctrl, &dbgController::compiled, this, &MainWindow::onCompiled);

	ecs::Rect *r = new ecs::Rect;
	mon = new Monitor(this);
	mon->setECSRoot(r);
	auto n = new ecs::RoundRect;
	r->setBrushColor(QColor(0, 0, 128, 128));
	r->setX(1.0);
	r->setRotDeg(20.0);
	n->setPos(QPointF(200.0, 0.0));
	n->setPenWidth(3);
	n->setBrushColor(QColor(128, 0, 0, 128));
	//n->setRotDeg(-20.0);
	//n->setScaleX(2.0);
	r->addChild(n);
	mon->show();

	fillSceneTree();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	Debugger::Instance().detach();
	saveSettings();
	QMainWindow::closeEvent(event);
}

void MainWindow::showEvent(QShowEvent * /*event*/)
{
	if (!settingsRead) {
		loadSettings();
		settingsRead = true;
		updateSceneUI();
	}
}

void MainWindow::loadSettings()
{
	QSettings settings;
	QByteArray arr;
	arr = settings.value("callstack_column_widths", "").toByteArray();
	ui->tblStack->horizontalHeader()->restoreState(arr);
	arr = settings.value("variables_column_widths", "").toByteArray();
	ui->tblVars->horizontalHeader()->restoreState(arr);
	arr = settings.value("breakpoints_column_widths", "").toByteArray();
	ui->tblBreakpoinzts->horizontalHeader()->restoreState(arr);
	arr = settings.value("project_column_widths", "").toByteArray();
	ui->tblProject->horizontalHeader()->restoreState(arr);
	arr = settings.value("scene_column_widths", "").toByteArray();
	ui->treeScene->header()->restoreState(arr);
	mon->restoreGeometry(settings.value("geometryMonitor").toByteArray());
	if (settings.contains("geometry")) {
		restoreGeometry(settings.value("geometry").toByteArray());
		restoreState(settings.value("windowState").toByteArray());
	} else {
		// setup an initial layout...
		tabifyDockWidget(ui->dockOutput, ui->dockConsole);
		addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, ui->dockCallstack, Qt::Vertical);
		addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, ui->dockVars, Qt::Vertical);
	}
}

void MainWindow::saveSettings()
{
	QSettings settings;
	settings.setValue("geometry", saveGeometry());
	settings.setValue("windowState", saveState());
	QByteArray arr;
	arr = ui->tblStack->horizontalHeader()->saveState();
	settings.setValue("callstack_column_widths", arr);
	arr = ui->tblVars->horizontalHeader()->saveState();
	settings.setValue("variables_column_widths", arr);
	arr = ui->tblBreakpoinzts->horizontalHeader()->saveState();
	settings.setValue("breakpoints_column_widths", arr);
	arr = ui->tblProject->horizontalHeader()->saveState();
	settings.setValue("project_column_widths", arr);
	arr = ui->treeScene->header()->saveState();
	settings.setValue("scene_column_widths", arr);
	settings.setValue("geometryMonitor", mon->saveGeometry());
}

void MainWindow::initStatusBar()
{
	QStatusBar* sb = ui->sb;

	sbCPos = new QLabel(sb);
	sbCPos->setFont(QFont("Courier", 10));
	sbCPos->setText("[    1:1    ]");
	sb->addPermanentWidget(sbCPos);
	sb->showMessage("Ok.");
}

void MainWindow::initDockWidgets()
{
	// movable ui-elements need a name in order to save/restore their geometry:
	ui->toolBar->setObjectName("toolbarDebug");
	ui->dockOutput->setObjectName("dockOutput");
	ui->dockConsole->setObjectName("dockConsole");
	ui->dockBreakpoints->setObjectName("dockBreakpoints");
	ui->dockCallstack->setObjectName("dockCallstack");
	ui->dockVars->setObjectName("dockVariables");
	ui->dockProject->setObjectName("DockProject");
}

void MainWindow::initDebugController()
{
	ctrl = &dbgController::Instance();
	ctrl->setupWidgets(ui->tabEditor,
						  ui->tblBreakpoinzts,
						  ui->tblVars,
						  ui->tblStack,
						  ui->tblProject,
						  ui->edOutput);
	dbgControllerActions* act = new dbgControllerActions;
	act->btnAddBP = ui->btnBPAdd;
	act->btnDelBP = ui->btnBPDel;
	act->btnClearBP = ui->btnBPClear;
	act->btnActiveBP = ui->chkBPActive;
	act->btnHitcountBP = ui->chkBPCounter;
	act->edtHitCount = ui->edtBPCount;
	act->btnAddVar = ui->btnVarAdd;
	act->btnDelVar = ui->btnVarDel;
	act->btnClearVars = ui->btnVarClear;
	act->btnAddToProject = ui->btnPrjAdd;
	act->btnDelFromProject = ui->btnPrjDel;

	act->actDbgCompile = ui->actDbgCompile;
	act->actDbgOver = ui->actDbgOver;
	act->actDbgIn = ui->actDbgIn;
	act->actDbgOut = ui->actDbgOut;
	act->actDbgPause = ui->actDbgPause;
	act->actDbgStart = ui->actDbgStart;
	act->actDbgStop = ui->actDbgStop;

	act->actDbgNewScript = ui->actFileNewScript;
	act->actDbgOpenScript = ui->actFileOpenScript;
	act->actDbgSaveScript = ui->actFileSaveScript;
	act->actDbgSaveAsScript = ui->actFileSaveAsScript;
	act->actDbgCloseScript = ui->actFileCloseScript;

	act->actDbgNewProject = ui->actFileNewProject;
	act->actDbgOpenProject = ui->actFileOpenProject;
	act->actDbgSaveProject = ui->actFileSaveProject;
	act->actDbgSaveAsProject = ui->actFileSaveAsProject;
	act->actDbgCloseProject = ui->actFileCloseProject;

	ctrl->setupActions(act); // will take ownership of the struct

	connect(ctrl, &dbgController::cursorMoved, this, &MainWindow::onCursorMove);
}

void MainWindow::initInspector()
{
	insp = new Inspector;
	ui->inspScroll->setWidget(insp);
	insp->init();
}








void MainWindow::initSceneTree()
{
	ui->treeScene->setHeaderLabels(QStringList() << tr("Class") << tr("Name"));
	connect(ui->treeScene, &QTreeWidget::currentItemChanged, this, &MainWindow::onCurrentNodeChanged);
	connect(ui->btnNodeUp, &QPushButton::clicked, this, &MainWindow::onMoveCurrentNodeUp);
	connect(ui->btnNodeDown, &QPushButton::clicked, this, &MainWindow::onMoveCurrentNodeDown);
}

QTreeWidgetItem* findWidgetItem(QTreeWidgetItem* curItem, ecs::Node* node)
{
	if (!curItem || !node)
		return nullptr;

	ecs::Node* f = curItem->data(0, Qt::UserRole).value<ecs::Node*>();
	if (f == node)
		return curItem;

	for (int i= 0; i<curItem->childCount(); i++)
	{
		if (findWidgetItem(curItem->child(i), node))
			return curItem->child(i);
	}

	return nullptr;
}

void MainWindow::fillSceneTree()
{
	QTreeWidget* tree = ui->treeScene;
	ecs::Node* root = mon->getECSRoot();

	// save a possible selection
	ecs::Node* sel = nullptr;
	if (tree->currentItem())
		sel = tree->currentItem()->data(0, Qt::UserRole).value<ecs::Node*>();

	tree->clear();
	if (!root)
		return;

	QTreeWidgetItem* rootItem = fillSceneTree(root, nullptr);

	// try to restore previous selection
	if (sel && rootItem) {
		QTreeWidgetItem* it = findWidgetItem(rootItem, sel);
		if (it)
			tree->setCurrentItem(it);
	}
}

QTreeWidgetItem* MainWindow::fillSceneTree(ecs::Node *node, QTreeWidgetItem* parent)
{
	QTreeWidget* tree = ui->treeScene;
	QTreeWidgetItem* it = new QTreeWidgetItem();
	it->setIcon(0, QIcon(*node->getIcon()));
	it->setText(0, QString::fromUtf8(node->NodeClassName()));
	it->setText(1, node->getName());
	it->setData(0, Qt::UserRole, QVariant::fromValue(node));


	connect(node, &ecs::Node::nameChanged, this, &MainWindow::onNodeNameChanged);

	if (parent)
		parent->addChild(it);
	else
		tree->addTopLevelItem(it);

	if (node->count()>0)
		it->setExpanded(true);
	for (quint32 i=0; i<node->count(); i++)
		fillSceneTree(node->child(i), it);

	return it;
}

void MainWindow::onCurrentNodeChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
	if (previous) {
		QVariant q = previous->data(0, Qt::UserRole);
		if (q.isValid()) {
			ecs::Node* prevNode = q.value<ecs::Node*>();
			if (prevNode)
				insp->unbindNode(prevNode);
		}
	}
	if (current) {
		ecs::Node* curNode = current->data(0, Qt::UserRole).value<ecs::Node*>();
		insp->bindNode(curNode);
		ui->btnNodeDel->setEnabled(true);
	} else
		insp->bindNode(nullptr);
	updateSceneUI();
}

void MainWindow::updateSceneUI()
{
	QTreeWidgetItem *current = ui->treeScene->currentItem();
	ecs::Node* node = current ? current->data(0, Qt::UserRole).value<ecs::Node*>() : nullptr;
	ui->btnNodeDel->setEnabled(node);
	ui->btnNodeAdd->setEnabled(node);
	if (node) {
		ui->btnNodeUp->setEnabled(node->canMoveUp());
		ui->btnNodeDown->setEnabled(node->canMoveDown());
	} else {
		ui->btnNodeUp->setEnabled(false);
		ui->btnNodeDown->setEnabled(false);
	}
}

void MainWindow::onMoveCurrentNodeUp()
{
	QTreeWidgetItem *current = ui->treeScene->currentItem();
	if (!current)
		return;

	ecs::Node* node = current->data(0, Qt::UserRole).value<ecs::Node*>();
	if (node) {
		node->moveUp();
		ui->treeScene->clear();
		rootItem = fillSceneTree(mon->getECSRoot(), nullptr);

		// try to restore previous selection
		if (rootItem && node) {
			QTreeWidgetItem* it = findWidgetItem(rootItem, node);
			if (it)
				ui->treeScene->setCurrentItem(it);
		}
	}
}

void MainWindow::onMoveCurrentNodeDown()
{
	QTreeWidgetItem *current = ui->treeScene->currentItem();
	if (!current)
		return;

	ecs::Node* node = current->data(0, Qt::UserRole).value<ecs::Node*>();
	if (node) {
		node->moveDown();
		ui->treeScene->clear();
		rootItem = fillSceneTree(mon->getECSRoot(), nullptr);

		// try to restore previous selection
		if (rootItem && node) {
			QTreeWidgetItem* it = findWidgetItem(rootItem, node);
			if (it)
				ui->treeScene->setCurrentItem(it);
		}
	}
}

void MainWindow::onAddNode()
{
	QTreeWidgetItem* it = ui->treeScene->currentItem();
	if (!it)
		return; // no parent
	ecs::Node* parent = it->data(0, Qt::UserRole).value<ecs::Node*>();
	if (!parent)
		return;
	dlgAddNode dlg;
	if (dlg.exec()) {
		ecs::Node* newNode = ecs::Node::createNode(dlg.getNodeClass(), dlg.getName());
		parent->addChild(newNode);

		ui->treeScene->clear();
		rootItem = fillSceneTree(mon->getECSRoot(), nullptr);

		// try to restore previous selection
		if (rootItem && newNode) {
			QTreeWidgetItem* it = findWidgetItem(rootItem, newNode);
			if (it)
				ui->treeScene->setCurrentItem(it);
		}
	}
}

void MainWindow::onDelNode()
{
	QTreeWidgetItem* it = ui->treeScene->currentItem();
	if (!it)
		return; // no parent
	ecs::Node* node = it->data(0, Qt::UserRole).value<ecs::Node*>();
	if (!node)
		return;
	ecs::Node* parent = node->getParent();
	if (!parent)
		return;

	if (node)
		insp->unbindNode(node);

	if (parent->delChild(node)) {
		it->setData(0, Qt::UserRole, QVariant::fromValue(nullptr));
		ui->treeScene->clear();
		fillSceneTree(mon->getECSRoot(), nullptr);
		ui->btnNodeDel->setEnabled(false);
	}
}

void MainWindow::onNodeNameChanged(QString name)
{
	ecs::Node* node = (ecs::Node*)sender();
	QTreeWidgetItem* it = ui->treeScene->currentItem();
	if (node == it->data(0, Qt::UserRole).value<ecs::Node*>())
		it->setText(1, name);
	else
	{
		if (!rootItem)
			return;
		it = findWidgetItem(rootItem, node);
		if (it)
			it->setText(1, name);
	}
}









void MainWindow::onCursorMove(int line, int column)
{
	QString l = "     " + QString::number(line);
	l = l.right(5);
	QString r = QString::number(column) + "      ";
	r = r.left(5);
	QString s = "[" + l + ":" + r + "]";
	sbCPos->setText(s);
}

void MainWindow::onCompiled()
{
	dukglue_register_global(ctrl->getCtx(), mon, "Monitor");
	Monitor::registerScripting(ctrl->getCtx());
	ecs::Node::registerScripting(ctrl->getCtx());
	ecs::Rect::registerScripting(ctrl->getCtx());
	ecs::RoundRect::registerScripting(ctrl->getCtx());
	ecs::Text::registerScripting(ctrl->getCtx());

	//if (!mon->getTimer().isSignalConnected(QMetaMethod::fromSignal(&QTimer::timeout)))
	disconnect(&mon->getTimer(), &QTimer::timeout, this, &MainWindow::onTimer);
	connect(&mon->getTimer(), &QTimer::timeout, this, &MainWindow::onTimer);



	mon->getTimer().start();
}

void MainWindow::onTimer()
{
	static bool needStart = true;

	if (needStart) {
		try {
			dukglue_pcall_method<void>(ctrl->getCtx(), mon, "onStart");
			mon->update();
			fillSceneTree();
			needStart = false;
		}  catch (DukException &e) {
			ctrl->debugPrint(e.what(), dbgLogLevel::llError);
			mon->getTimer().stop();
			needStart = true;
		}
	}

	try {
		dukglue_pcall_method<void>(ctrl->getCtx(), mon, "onTick");
		mon->update();
	}  catch (DukException &e) {
		ctrl->debugPrint(e.what(), dbgLogLevel::llError);
		mon->getTimer().stop();
	}
}


