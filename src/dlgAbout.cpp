#include "dlgAbout.h"
#include "ui_dlgAbout.h"

dlgAbout::dlgAbout(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::dlgAbout)
{
   ui->setupUi(this);
   setFixedSize(size());
}

dlgAbout::~dlgAbout()
{
   delete ui;
}

void dlgAbout::changeEvent(QEvent *e)
{
   QDialog::changeEvent(e);
   switch (e->type()) {
      case QEvent::LanguageChange:
         ui->retranslateUi(this);
         break;
      default:
         break;
   }
}
