#include "Monitor.h"
#include "src/ECS/Consts.h"
#include <QPainter>
#include <QResizeEvent>
#include <QSurfaceFormat>
#include <dukglue/dukglue.h>

void Monitor::registerScripting(duk_context *ctx)
{
   dukglue_register_method(ctx, &Monitor::getECSRoot, "Root");

}

Monitor::Monitor(QWidget *parent) :
   QOpenGLWidget(parent),
   drawOrigin(false)
{

   setWindowFlags(Qt::Window);
   setWindowTitle("Monitor");
   setMinimumSize(400, 400);
   setViewport(QRectF(-890, -300, 1780, 1000));
   setBackgroundColor(ecs::priv::colMonitorBG);
   setOriginColor(ecs::priv::colMonitorOrg);
   setDrawOrigin(true);
   timer.setInterval(20);
   timer.start();
}

void Monitor::setViewport(const QRectF &viewport)
{
   vp = viewport;
}

void Monitor::setBackgroundColor(const QColor &bkgcol)
{
   if (bkgcol != bgcol) {
      bgcol = bkgcol;
      update();
   }
}

void Monitor::setOriginColor(const QColor &originColor)
{
   if (originColor != orgcol) {
      orgcol = originColor;
      update();
   }
}

void Monitor::setDrawOrigin(bool drawOrg)
{
   if (drawOrg != drawOrigin) {
      drawOrigin = drawOrg;
      update();
   }
}

void Monitor::initializeGL()
{
   glEnable(GL_MULTISAMPLE);
}

void Monitor::paintEvent(QPaintEvent *e)
{
   Q_UNUSED(e)
   QPainter p(this);
   p.setRenderHint(QPainter::Antialiasing, true);
   p.setRenderHint(QPainter::TextAntialiasing, true);
   p.setRenderHint(QPainter::SmoothPixmapTransform, true);
   applyVP(p);

   p.fillRect(vp, bgcol);

   if (drawOrigin) {
      QPen pen { orgcol };
      pen.setWidthF(0);
      pen.setCosmetic(true);
      p.setPen(pen);
      p.drawLine(QPointF(vp.x(), 0.0), QPointF(vp.x()+vp.width(), 0.0));
      p.drawLine(QPointF(0.0, vp.y()), QPointF(0.0, vp.y()+vp.height()));
   }

   if (root)
      root->doPaint(p, getWorldTransform());
}


void Monitor::applyVP(QPainter& p)
{
   qreal ar_win = (qreal)width() / (qreal)height();
   qreal ar_view = vp.width() / vp.height();
   QRect rct;

   if (abs(ar_win - ar_view) < 1e-10)
      rct = QRect(vp.x(), -vp.height()-vp.y(), vp.width(), vp.height());
   else {
      QPointF org;
      qreal w, h, f;
      if (ar_win > ar_view) {
         f = ar_win / ar_view;
         org.setX(vp.x() * f);
         org.setY((-vp.height()-vp.y()));
         w = vp.width() * f;
         h = vp.height();
      } else {
         f = ar_view / ar_win;
         org.setX(vp.x());
         org.setY((-vp.height()-vp.y()) * f);
         w = vp.width();
         h = vp.height() * f;
      }
      rct = QRect(org.x(), org.y(), w, h);
   }

   p.setWindow(rct);
   wt = p.worldTransform();
   wt = wt.scale(1.0, -1.0);
   p.setWorldTransform(wt, false);

}
