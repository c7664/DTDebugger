#ifndef INSPECTOR_H
#define INSPECTOR_H

#include <QDebug>
#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QPushButton>
#include <QFontComboBox>
#include <QColorDialog>
#include <QAnimationGroup>
#include <QPropertyAnimation>
#include <vector>
#include "src/ECS/Types.h"
#include "src/ECS/Node.h"
#include "src/ECS/Rect.h"
#include "src/ECS/RoundRect.h"
#include "src/ECS/Text.h"
#include "src/ECS/Image.h"

enum class PropertyType {
	vec2,
	scalar,
	string,
	file,
	font,
	color,
	boolean,
	integer,
	Enum
};

struct Enum {
	QString name;
	std::vector<std::pair<qint32, QString>> vals;
};

struct Property {
	PropertyType type;
	QString caption;
	QString subCap[2];
	QString stringVal;
	QString stringDefault;
	bool readOnly;
	union {
		quint32 u32;
		qint32 i32;
		qreal f64[2];
		bool boolean;
	} val;
	union {
		quint32 u32;
		qint32 i32;
		qreal f64[2];
		bool boolean;
	} vdefault;
	union {
		quint32 u32;
		qint32 i32;
		qreal f64[2];
	} vmin;
	union  {
		quint32 u32;
		qint32 i32;
		qreal f64[2];
	} vmax;
	Enum* EnumStruct;
	QWidget* editor[2];
	void* adr; // bound val (like QColor*, QPointF* etc)
};

Q_DECLARE_METATYPE(Property*)

class HeaderWidget;




class InspectorPage : public QWidget {
	Q_OBJECT
public:
  InspectorPage(const QString &title);

  int addStringOroperty(const QString& caption, const QString& def, bool readOnly, bool addSpace);
  int addFontProperty(const QString& caption, const QString& def, bool addSpace);
  int addVec2Property(const QString& caption, const QString& subCap1, const QString& subCap2, qreal vmin1, qreal vmin2, qreal vmax1, qreal vmax2, qreal vdef1, qreal vdef2);
  int addScalarProperty(const QString& caption, const QString& subCap, qreal vmin, qreal vmax, qreal vdef, bool addSpace);
  int addBoolProperty(const QString& caption, const QString& buttonCaption, bool def, bool addSpace);
  int addColorProperty(const QString& caption, const ecs::Color& def, bool addSpace);
  int addEnumProperty(const QString& caption, const QString& subCap, Enum* vals, qint32 def, bool addSpace);
  int addFileProperty(const QString& caption, const QStringList& filter, bool addSpace);

  int propCount() const { return (int)props.size(); }
  Property* property(int index) { return props.at(index); }

protected slots:
  void onFileOpenSelect();

private:
	QWidget *item;
	HeaderWidget *l;
	QGridLayout *grid;
	std::vector<Property*> props;
	int curRow = 0;
};







class Inspector : public QWidget
{
	Q_OBJECT
public:
	explicit Inspector(QWidget *parent = nullptr);

	void init();

	InspectorPage* addPage(const QString& caption);
	InspectorPage* page(int index) { return pages.at(index); }
	Property* prop(int page, int index) { return pages.at(page)->property(index); }

	void bindNode(ecs::Node* node);
	void unbindNode(ecs::Node* node);

signals:

protected:
	void hideAllPages();
	void showPagesForClass(ecs::NodeClass cls);
	void bindRect(ecs::Rect* rect);
	void bindRoundRect(ecs::RoundRect* roundRect);
	void bindText(ecs::Text* text);
	void bindImage(ecs::Image* img);

	void unbindRect(ecs::Rect* rect);
	void unbindRoundRect(ecs::RoundRect* roundRect);
	void unbindText(ecs::Text* text);
	void unbindImage(ecs::Image* img);

private:
	QVBoxLayout *l;
	std::vector<InspectorPage*> pages;
	std::vector<Enum> enums;

	std::vector<std::pair<ecs::NodeClass, std::vector<qint32>>> classPages;
	quint32 uImgSel;
};






class DoubleSpinBox : public QDoubleSpinBox
{
	Q_OBJECT
public:
	explicit DoubleSpinBox() : QDoubleSpinBox() { init(); }
	explicit DoubleSpinBox(QWidget* parent) : QDoubleSpinBox(parent) { init(); }

	void stepBy(int steps) override
	{
		QDoubleSpinBox::stepBy(steps);
		emit safeValueChanged(value());
	}
signals:
	// only emitted, when stepping current value, or after pressing enter/
	// losing focus; not emitted while enetering numbers via keyboard:
	void safeValueChanged(qreal val);

protected slots:
	void onEditingFinished() {
		emit safeValueChanged(value());
	}
protected:
	void init()
	{
		connect(this, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
	}
};

class LineEdit : public QLineEdit
{
	Q_OBJECT
public:
	LineEdit() : QLineEdit() {
		connect(this, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
	}
	explicit LineEdit(QWidget* parent) : QLineEdit(parent) {
		connect(this, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
	}
	void sendEdited() {
		emit editingFinished(text());
	}

signals:
	void editingFinished(QString text);

protected slots:
	void onEditingFinished() {
		emit editingFinished(text());
	}
};

class ColorWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ColorWidget(QWidget *parent = nullptr) : QWidget(parent){
		setCursor(Qt::PointingHandCursor);
		setMinimumSize(QSize(20, 20));
		setMaximumSize(QSize(24, 24));
		if (!colbkg)
			colbkg = new QPixmap(":/res/dk/res/colbkg.png");
	}

	const ecs::Color& getColor() const { return col; }
	void setBuddy(LineEdit* ed) { buddy=ed; }

public slots:
	void setColor(const ecs::Color& color) {
		col = color;
		update();
		if (buddy)
			buddy->setText(col.asQColor().name(QColor::HexArgb));
	}
	void setColor(QColor c) {
		col = ecs::Color(c);
		update();
		if (buddy)
			buddy->setText(col.asQColor().name(QColor::HexArgb));
	}

signals:
	void colorChanged(QColor color);

protected:
	void paintEvent(QPaintEvent *event) override {
		Q_UNUSED(event)
		QPainter p(this);
		p.setPen(Qt::NoPen);
		QBrush bkg;
		bkg.setTexture(*colbkg);
		p.fillRect(rect(), bkg);
		p.fillRect(rect(), col.asQColor());
	}
	void mousePressEvent(QMouseEvent* event) override {
		if (event->button() == Qt::LeftButton) {
			col = QColorDialog::getColor(col.asQColor(), nullptr, tr("Select Color"), QColorDialog::ShowAlphaChannel | QColorDialog::DontUseNativeDialog);
			update();
			if (buddy)
				buddy->setText(col.asQColor().name(QColor::HexArgb));
			emit colorChanged(col.asQColor());
		}
	}
	ecs::Color col;
	LineEdit* buddy = nullptr;
	static QPixmap* colbkg;;
};





class HeaderWidget : public QWidget
{
	Q_OBJECT
public:
	HeaderWidget(const QString& caption, QWidget *item, QWidget *parent = nullptr) :
		QWidget(parent)
	{
		title = caption;
		content = item;
		setCursor(Qt::PointingHandCursor);
		setVisible(true);
		setMinimumSize(20, 20);
		loadPixmaps();
		vis = true;
		h = content->height();

		ani = new QPropertyAnimation(content, "maximumHeight");
		connect(ani, &QPropertyAnimation::finished, this, &HeaderWidget::onAniFinished);
	}
	~HeaderWidget() { if (ani) delete ani; }

	QSize sizeHint() const override {
		return QSize(160, 20);
	}

protected:
	void paintEvent(QPaintEvent *event) override {
		Q_UNUSED(event)
		QPainter p(this);
		QRect r = rect();
		p.setPen(palette().color(QPalette::WindowText));
		p.setBrush(palette().color(QPalette::Dark));
		p.fillRect(r, palette().color(QPalette::Dark));
		p.drawText(r, Qt::AlignCenter, title);
		if (content) {
			QRect rt { 4, 4, 12, 12 };
			if (vis)
				p.drawPixmap(rt, *pmOpen, pmOpen->rect());
			else
				p.drawPixmap(rt, *pmClosed, pmClosed->rect());
		}
	}

	void mousePressEvent(QMouseEvent *event) override {
		if (rect().contains(event->pos()) &&
			 event->button() == Qt::LeftButton &&
			 ani->state() != QAbstractAnimation::Running) {
			if (content->isVisible()) {
				vis = false;
				h = content->height();
				ani->setDuration(100);
				ani->setStartValue(h);
				ani->setEndValue(0);
				ani->start();
			} else {
				vis = true;
				ani->setDuration(100);
				ani->setStartValue(0);
				ani->setEndValue(h);
				ani->start();
				content->setVisible(true);
			}
			update();
		}
	}

	void onAniFinished() {
		content->setVisible(vis);
	}

	void loadPixmaps() {
		if (!pmClosed)
			pmClosed = new QPixmap(":/res/dk/res/dk/right.svg");
		if (!pmOpen)
			pmOpen = new QPixmap(":/res/dk/res/dk/down.svg");
	}

	static QPixmap* pmClosed;
	static QPixmap* pmOpen;
	QWidget *content;
	QString title;
	int h;
	bool vis;
	QPropertyAnimation* ani = nullptr;
};


#endif // INSPECTOR_H
