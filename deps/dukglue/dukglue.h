#pragma once

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wreturn-type"
#pragma GCC diagnostic ignored "-Wunused-value"
#pragma GCC diagnostic ignored "-Wunused-but-set-parameter"
#endif

#include "register_function.h"
#include "register_class.h"
#include "register_property.h"
#include "public_util.h"
#include "dukvalue.h"

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif
