#include <unordered_map>
#include <QFileDialog>
#include <QApplication>
#include "Inspector.h"

QPixmap* HeaderWidget::pmClosed = nullptr;
QPixmap* HeaderWidget::pmOpen = nullptr;
QPixmap* ColorWidget::colbkg = nullptr;

std::unordered_map<quint32, QPushButton*> fileSelectButtons;
std::unordered_map<quint32, QPushButton*> defaultButtons;

//////////////////////////////////////////////////////////////////////////////
//
// InspectorPage
//
//////////////////////////////////////////////////////////////////////////////

InspectorPage::InspectorPage(const QString &title) : item(nullptr)
{
	QVBoxLayout *layout = new QVBoxLayout;
	layout->setContentsMargins(0, 0, 0, 0);

	item = new QWidget();
	l = new HeaderWidget(title, item, this);
	layout->addWidget(l);

	grid = new QGridLayout();
	grid->setAlignment(Qt::AlignTop);
	grid->setColumnStretch(2, 10);

	item->setLayout(grid);

	layout->addWidget(item);
	layout->setAlignment(Qt::AlignTop);

	setLayout(layout);

	item->setVisible(true);
}

int InspectorPage::addVec2Property(
		const QString& caption,
		const QString& subCap1,
		const QString& subCap2,
		qreal vmin1,
		qreal vmin2,
		qreal vmax1,
		qreal vmax2,
		qreal vdef1,
		qreal vdef2)
{
	Property *p = new Property;
	p->type = PropertyType::vec2;
	p->readOnly = false;
	p->caption = caption;
	p->subCap[0] = subCap1;
	p->subCap[1] = subCap2;
	p->val.f64[0] = vdef1;
	p->val.f64[1] = vdef2;
	p->vdefault.f64[0] = vdef1;
	p->vdefault.f64[1] = vdef2;
	p->vmin.f64[0] = vmin1;
	p->vmin.f64[1] = vmin2;
	p->vmax.f64[0] = vmax1;
	p->vmax.f64[1] = vmax2;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	lb = new QLabel(subCap1);
	lb->setMinimumWidth(16);
	grid->addWidget(lb, curRow, 1);
	lb = new QLabel(subCap2);
	lb->setMinimumWidth(16);
	grid->addWidget(lb, curRow+1, 1);
	DoubleSpinBox *edt = new DoubleSpinBox();
	p->editor[0] = edt;
	edt->setMinimum(vmin1);
	edt->setMaximum(vmax1);
	edt->setSingleStep((vmax1-vmin1)/500.0);
	edt->setValue(vdef1);
	grid->addWidget(edt, curRow, 2);
	edt->setVisible(true);
	edt = new DoubleSpinBox();
	p->editor[1] = edt;
	edt->setMinimum(vmin2);
	edt->setMaximum(vmax2);
	edt->setSingleStep((vmax2-vmin1)/500.0);
	edt->setValue(vdef2);
	grid->addWidget(edt, curRow+1, 2);
	edt->setVisible(true);
	QPushButton *btn = new QPushButton();
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	btn->setMaximumHeight(2000);
	//btn->setProperty("Prop", p);
	grid->addWidget(btn, curRow, 3, 2, 1);
	btn->setVisible(true);

	grid->setRowMinimumHeight(curRow+2, 10);
	curRow += 3;
	return (int)props.size()-1;
}

int InspectorPage::addScalarProperty(const QString &caption, const QString& subCap, qreal vmin, qreal vmax, qreal vdef, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::scalar;
	p->readOnly = false;
	p->caption = caption;
	p->readOnly = false;
	p->vdefault.f64[0] = vdef;
	p->val.f64[0] = vdef;
	p->vmin.f64[0] = vmin;
	p->vmax.f64[0] = vmax;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	lb = new QLabel(subCap);
	grid->addWidget(lb, curRow, 1);
	DoubleSpinBox *edt = new DoubleSpinBox();
	p->editor[0] = edt;
	edt->setMinimum(vmin);
	edt->setMaximum(vmax);
	edt->setSingleStep((vmax-vmin)/100.0);
	edt->setValue(vdef);
	grid->addWidget(edt, curRow, 2);
	edt->setVisible(true);
	QPushButton *btn = new QPushButton;
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	btn->setMaximumHeight(2000);
	grid->addWidget(btn, curRow, 3);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addBoolProperty(const QString &caption, const QString &buttonCaption, bool def, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::boolean;
	p->readOnly = false;
	p->caption = caption;
	p->readOnly = false;
	p->vdefault.boolean = def;
	p->val.boolean = def;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);

	QPushButton *btn = new QPushButton(buttonCaption);
	btn->setCheckable(true);
	btn->setChecked(def);
	p->editor[0] = btn;
	grid->addWidget(btn, curRow, 2);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addStringOroperty(const QString &caption, const QString &def, bool readOnly, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::string;
	p->readOnly = false;
	p->caption = caption;
	p->stringDefault = def;
	p->stringVal = def;
	p->readOnly = readOnly;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);

	if (readOnly) {
		QLabel* ed = new QLabel;
		p->editor[0] = ed;
		ed->setText(def);
		grid->addWidget(ed, curRow, 2);
	} else {
		LineEdit* ed = new LineEdit;
		p->editor[0] = ed;
		ed->setText(def);
		grid->addWidget(ed, curRow, 2);
	}
	QPushButton *btn;
	if (!readOnly) {
		btn = new QPushButton;
		btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
		btn->setMaximumWidth(24);
		grid->addWidget(btn, curRow, 3);
	}

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addFontProperty(const QString &caption, const QString &def, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::font;
	p->readOnly = false;
	p->caption = caption;
	p->stringDefault = def;
	p->stringVal = def;
	p->readOnly = false;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	QFontComboBox* fc = new QFontComboBox;
	fc->setMinimumWidth(80);
	if (!def.isEmpty())
		fc->setCurrentIndex(fc->findText(def));
	grid->addWidget(fc, curRow, 2);
	p->editor[0] = fc;
	QPushButton *btn = new QPushButton;
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	btn->setMaximumHeight(2000);
	grid->addWidget(btn, curRow, 3);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addColorProperty(const QString &caption, const ecs::Color &def, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::color;
	p->readOnly = false;
	p->caption = caption;
	p->readOnly = false;
	p->adr = nullptr;
	p->stringDefault = def.asQColor().name(QColor::HexArgb);
	p->stringVal = def.asQColor().name(QColor::HexArgb);

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	ColorWidget *cw = new ColorWidget;
	cw->setColor(def);
	p->editor[0] = cw;
	grid->addWidget(cw, curRow, 1);
	LineEdit* le = new LineEdit;
	le->setFont(QFont("Courier"));
	le->setText(p->stringDefault);
	p->editor[1] = le;
	cw->setBuddy(le);
	grid->addWidget(le, curRow, 2);
	QPushButton* btn = new QPushButton;
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	grid->addWidget(btn, curRow, 3);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addEnumProperty(
		const QString &caption,
		const QString &subCap,
		Enum *vals,
		qint32 def, bool
		addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::Enum;
	p->readOnly = false;
	p->caption = caption;
	p->subCap[0] = subCap;
	p->readOnly = false;
	p->EnumStruct = vals;
	p->vdefault.i32 = def;
	p->adr = nullptr;

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	lb = new QLabel(subCap);
	grid->addWidget(lb, curRow, 1);
	QComboBox *cmb = new QComboBox;
	p->editor[0] = cmb;
	for (int i=0; i<(int)vals->vals.size(); i++) {
		auto p = vals->vals[i];
		cmb->addItem(p.second, QVariant::fromValue(p.first));
	}
	cmb->setCurrentIndex(def);
	grid->addWidget(cmb, curRow, 2);
	QPushButton* btn = new QPushButton;
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	grid->addWidget(btn, curRow, 3);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}

int InspectorPage::addFileProperty(const QString& caption, const QStringList& filter, bool addSpace)
{
	Property *p = new Property;
	p->type = PropertyType::file;
	p->readOnly = false;
	p->caption = caption;
	p->readOnly = false;
	p->adr = nullptr;
	p->stringDefault = filter.join("|");

	props.push_back(p);

	QLabel *lb = new QLabel(caption);
	grid->addWidget(lb, curRow, 0);
	QPushButton *btn = new QPushButton("...");
	connect(btn, &QPushButton::clicked, this, &InspectorPage::onFileOpenSelect);
	p->editor[0] = btn;
	btn->setMaximumWidth(24);
	fileSelectButtons[props.size()-1] = btn;
	grid->addWidget(btn, curRow, 1);
	LineEdit* le = new LineEdit;
//	le->setFont(QFont("Courier"));
//	le->setText(p->stringDefault);
	p->editor[1] = le;
	grid->addWidget(le, curRow, 2);
	btn = new QPushButton;
	btn->setIcon(QIcon(":/res/dk/res/dk/default.svg"));
	btn->setMaximumWidth(24);
	grid->addWidget(btn, curRow, 3);

	if (addSpace) {
		grid->setRowMinimumHeight(curRow+1, 10);
		curRow += 2;
	} else
		curRow += 1;
	return (int)props.size()-1;
}


void InspectorPage::onFileOpenSelect()
{
	QVariant v = sender()->property("PropAddress");
	if (!v.isValid())
		return;
	Property* p = v.value<Property*>();
	if (!p)
		return;
	QStringList filters = p->stringDefault.split("|");
	QString name = ((LineEdit*)p->editor[1])->text();

	QFileDialog fd(nullptr, tr("Select file"));
	fd.selectFile(name);
	fd.setNameFilters(filters);
	if (fd.exec()) {
		((LineEdit*)p->editor[1])->setText(fd.selectedFiles().at(0));
		((LineEdit*)p->editor[1])->sendEdited();
	}
}





//////////////////////////////////////////////////////////////////////////////
//
// Inspector
//
//////////////////////////////////////////////////////////////////////////////

Inspector::Inspector(QWidget *parent) : QWidget(parent)
{
	l = new QVBoxLayout(this);
	setLayout(l);
	l->setAlignment(Qt::AlignTop);
}

InspectorPage *Inspector::addPage(const QString &caption)
{
	InspectorPage* p = new InspectorPage(caption);
	p->setVisible(false);
	l->addWidget(p);
	pages.push_back(p);
	return p;
}



void Inspector::bindNode(ecs::Node *node)
{
	if (!node) {
		hideAllPages();
		return;
	}
	showPagesForClass(node->nodeClass());

	Property* p;
	p = prop(0, 0); // class name [QString]
	((QLabel*)p->editor[0])->setText(QString::fromUtf8(node->NodeClassName()));
	p = prop(0, 1); // node name [QString]
	connect(node, SIGNAL(nameChanged(QString)),(LineEdit*)p->editor[0], SLOT(setText(QString)));
	connect((LineEdit*)p->editor[0], SIGNAL(editingFinished(QString)), node, SLOT(setName(QString)));
	((LineEdit*)p->editor[0])->setText(node->getName());
	p = prop(0, 2); // node visibility [bool]
	connect(node, SIGNAL(visibilityChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), node, SLOT(setVisible(bool)));
	((QPushButton*)p->editor[0])->setChecked(node->getVisible());
	p = prop(1, 0); // transform pivot x, y [double, double]
	connect(node,SIGNAL(pivotXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(node, SIGNAL(pivotYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setPivotX(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setPivotY(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(node->getPivotX());
	((DoubleSpinBox*)p->editor[1])->setValue(node->getPivotY());
	p = prop(1, 1); // transform translation x, y [double, double]
	connect(node, SIGNAL(translationXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(node, SIGNAL(translationYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setX(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setY(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(node->getX());
	((DoubleSpinBox*)p->editor[1])->setValue(node->getY());
	p = prop(1, 2); // transform scale x, y [double, double]
	connect(node, SIGNAL(scaleXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(node, SIGNAL(scaleYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setScaleX(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setScaleY(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(node->getScaleX());
	((DoubleSpinBox*)p->editor[1])->setValue(node->getScaleY());
	p = prop(1, 3); // transform rotation degree [double]
	connect(node, SIGNAL(rotationDegChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setRotDeg(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(node->getRotDeg());

	switch (node->nodeClass()) {
		case ecs::NodeClass::Rect:
			bindRect((ecs::Rect*)node);
			return;
		case ecs::NodeClass::RoundRect:
			bindRoundRect((ecs::RoundRect*)node);
			return;
		case ecs::NodeClass::Text:
			bindRoundRect((ecs::RoundRect*)node);
			bindText((ecs::Text*)node);
			return;
		case ecs::NodeClass::Image:
			bindImage((ecs::Image*)node);
			return;
		default: ;
	}
}

void Inspector::bindRect(ecs::Rect* rect)
{
	Property *p;
	p = prop(2, 0); // P1 x, y [double, double]
	connect(rect, SIGNAL(p1xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(rect, SIGNAL(p1yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1X(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1Y(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getP1X());
	((DoubleSpinBox*)p->editor[1])->setValue(rect->getP1Y());
	p = prop(2, 1); // P2 x, y [double, double]
	connect(rect, SIGNAL(p2xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(rect, SIGNAL(p2yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2X(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2Y(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getP2X());
	((DoubleSpinBox*)p->editor[1])->setValue(rect->getP2Y());
	p = prop(2, 2); // pen color [QColor]
	connect(rect, SIGNAL(penColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setPenQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setPenColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(rect->getPenColor()->asQColor());
	p = prop(2, 3); // pen width [double]
	connect(rect, SIGNAL(penWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setPenWidth(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getPenWidth());
	p = prop(2, 4); // pen width in pixel [bool]
	connect(rect, SIGNAL(penWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), rect, SLOT(setPenWidthInPixel(bool)));
	((QPushButton*)p->editor[0])->setChecked(rect->getPenWidthInPixel());
	p = prop(2, 5); // brush color [QColor]
	connect(rect, SIGNAL(brushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setBrushQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setBrushColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(rect->getBrushColor()->asQColor());
}

void Inspector::bindRoundRect(ecs::RoundRect* rect)
{
	Property *p;
	p = prop(3, 0); // P1 x, y [double, double]
	connect(rect, SIGNAL(p1xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(rect, SIGNAL(p1yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1X(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1Y(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getP1X());
	((DoubleSpinBox*)p->editor[1])->setValue(rect->getP1Y());
	p = prop(3, 1); // P2 x, y [double, double]
	connect(rect, SIGNAL(p2xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect(rect, SIGNAL(p2yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2X(double)));
	connect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2Y(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getP2X());
	((DoubleSpinBox*)p->editor[1])->setValue(rect->getP2Y());
	p = prop(3, 2); // rx [double]
	connect(rect, SIGNAL(rxChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setRX(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getRX());
	p = prop(3, 3); // ry [double]
	connect(rect, SIGNAL(ryChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setRY(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getRY());
		p = prop(3, 4); // pen color [QColor]
	connect(rect, SIGNAL(penColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setPenQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setPenColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(rect->getPenColor()->asQColor());
	p = prop(3, 5); // pen width [double]
	connect(rect, SIGNAL(penWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setPenWidth(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getPenWidth());
	p = prop(3, 6); // pen width in pixel [bool]
	connect(rect, SIGNAL(penWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), rect, SLOT(setPenWidthInPixel(bool)));
	((QPushButton*)p->editor[0])->setChecked(rect->getPenWidthInPixel());
	p = prop(3, 7); // brush color [QColor]
	connect(rect, SIGNAL(brushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setBrushQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setBrushColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(rect->getBrushColor()->asQColor());
}

void Inspector::bindText(ecs::Text* text)
{
	Property *p;
	p = prop(4, 0); // caption [QString]
	connect(text, SIGNAL(captionChanged(QString)), (LineEdit*)p->editor[0], SLOT(setText(QString)));
	connect((LineEdit*)p->editor[0], SIGNAL(editingFinished(QString)), text, SLOT(setCaption(QString)));
	((LineEdit*)p->editor[0])->setText(text->getCaption());
	p = prop(4, 1); // rect visible [bool]
	connect(text, SIGNAL(rectVisibleChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setRectVisible(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getRectVisible());
	p = prop(4, 2); // text pen color [QColor]
	connect(text, SIGNAL(textPenColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), text, SLOT(setTextPenQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), text, SLOT(setTextPenColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(text->getTextPenColor()->asQColor());
	p = prop(4, 3); // text pen width [double]
	connect(text, SIGNAL(textPenWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), text, SLOT(setTextPenWidth(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(text->getTextPenWidth());
	p = prop(4, 4); // text pen width in pixel [bool]
	connect(text, SIGNAL(textPenWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setTextPenWidthInPixel(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getTextPenWidthInPixel());
	p = prop(4, 5); // text brush color [QColor]
	connect(text, SIGNAL(textBrushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	connect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), text, SLOT(setTextBrushQColor(QColor)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), text, SLOT(setTextBrushColorName(QString)));
	((ColorWidget*)p->editor[0])->setColor(text->getTextBrushColor()->asQColor());
	p = prop(4, 6); // text size [double]
	connect(text, SIGNAL(textSizeChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), text, SLOT(setTextSize(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(text->getTextSize());
	p = prop(4, 7); // horz align [enum]
	connect(text, SIGNAL(horzAlignChanged(int)), (QComboBox*)p->editor[0], SLOT(setCurrentIndex(int)));
	connect((QComboBox*)p->editor[0], SIGNAL(currentIndexChanged(int)), text, SLOT(setHAlign(int)));
	((QComboBox*)p->editor[0])->setCurrentIndex(text->getHAlign());
	p = prop(4, 8); // vert align [enum]
	connect(text, SIGNAL(vertAlignChanged(int)), (QComboBox*)p->editor[0], SLOT(setCurrentIndex(int)));
	connect((QComboBox*)p->editor[0], SIGNAL(currentIndexChanged(int)), text, SLOT(setVAlign(int)));
	((QComboBox*)p->editor[0])->setCurrentIndex(text->getVAlign());
	p = prop(4, 9); // font [QString]
	connect(text, SIGNAL(fontChanged(QString)), (LineEdit*)p->editor[0], SLOT(setCurrentText(QString)));
	connect((QFontComboBox*)p->editor[0], SIGNAL(currentTextChanged(QString)), text, SLOT(setFontName(QString)));
	((QFontComboBox*)p->editor[0])->setCurrentText(text->getFontName());
	p = prop(4, 10); // bold [bool]
	connect(text, SIGNAL(boldChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setBold(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getBold());
	p = prop(4, 11); // italic [bool]
	connect(text, SIGNAL(italicChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setItalic(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getItalic());
	p = prop(4, 12); // underline [bool]
	connect(text, SIGNAL(italicChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setUnderline(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getUnderline());
	p = prop(4, 13); // strike through [bool]
	connect(text, SIGNAL(strikeThroughChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setStrikeThrough(bool)));
	((QPushButton*)p->editor[0])->setChecked(text->getStrikeThrough());
}

void Inspector::bindImage(ecs::Image* img)
{
	Property *p;
	p = prop(5, 0); // filename [QString]
	connect(img, SIGNAL(pathChanged(QString)),(LineEdit*)p->editor[1], SLOT(setText(QString)));
	connect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), img, SLOT(setPath(QString)));
	((LineEdit*)p->editor[1])->setText(img->getPath());
	fileSelectButtons[uImgSel]->setProperty("PropAddress", QVariant::fromValue(p));
	p = prop(5, 1); // opacity [double]
	connect(img, SIGNAL(opacityChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	connect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), img, SLOT(setOpacity(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(img->getOpacity());
	p = prop(5, 2); // H-Invert [bool]
	connect(img, SIGNAL(hInvertChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), img, SLOT(setHInvert(bool)));
	((QPushButton*)p->editor[0])->setChecked(img->getHInvert());
	p = prop(5, 3); // V-Invert [bool]
	connect(img, SIGNAL(vInvertChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	connect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), img, SLOT(setVInvert(bool)));
	((QPushButton*)p->editor[0])->setChecked(img->getVInvert());
	p = prop(5, 4); // resolution info [QString]
	connect(img, SIGNAL(infoChanged(QString)), (QLabel*)p->editor[0], SLOT(setText(QString)));
	((QLabel*)p->editor[0])->setText(img->getInfo());
}





void Inspector::unbindNode(ecs::Node* node)
{
	if (!node)
		return;
	Property* p;
	p = prop(0, 1); // node name
	disconnect(node, SIGNAL(nameChanged(QString)),(LineEdit*) p->editor[0], SLOT(setText(QString)));
	disconnect((LineEdit*)p->editor[0], SIGNAL(editingFinished(QString)), node, SLOT(setName(QString)));
	p = prop(0, 2); // node visibility
	disconnect(node, SIGNAL(visibilityChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), node, SLOT(setVisible(bool)));
	p = prop(1, 0); // transform pivot x, y
	disconnect(node,SIGNAL(pivotXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(node, SIGNAL(pivotYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setPivotX(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setPivotY(double)));
	p = prop(1, 1); // transform translation x, y
	disconnect(node, SIGNAL(translationXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(node, SIGNAL(translationYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setX(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setY(double)));
	p = prop(1, 2); // transform scale x, y
	disconnect(node, SIGNAL(scaleXChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(node, SIGNAL(scaleYChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setScaleX(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), node, SLOT(setScaleY(double)));
	p = prop(1, 3); // transform rotation degree
	disconnect(node, SIGNAL(rotationDegChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), node, SLOT(setRotDeg(double)));

	for (int i=0; i<(int)fileSelectButtons.size(); i++)
		fileSelectButtons[i]->setProperty("PropAddress", QVariant::fromValue(nullptr));
	for (int i=0; i<(int)defaultButtons.size(); i++)
		defaultButtons[i]->setProperty("PropAddress", QVariant::fromValue(nullptr));

	switch (node->nodeClass()) {
		case ecs::NodeClass::Rect:
			unbindRect((ecs::Rect*)node);
			return;
		case ecs::NodeClass::RoundRect:
			unbindRoundRect((ecs::RoundRect*)node);
			return;
		case ecs::NodeClass::Text:
			unbindRoundRect((ecs::RoundRect*)node);
			unbindText((ecs::Text*)node);
			return;
		case ecs::NodeClass::Image:
			unbindImage((ecs::Image*)node);
			return;
		default: ;
	}
}

void Inspector::unbindRect(ecs::Rect* rect)
{
	Property *p;
	p = prop(2, 0); // P1 x, y [double, double]
	disconnect(rect, SIGNAL(p1xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(rect, SIGNAL(p1yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1X(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1Y(double)));
	p = prop(2, 1); // P2 x, y [double, double]
	disconnect(rect, SIGNAL(p2xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(rect, SIGNAL(p2yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2X(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2Y(double)));
	p = prop(2, 2); // pen color [QColor]
	disconnect(rect, SIGNAL(penColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setPenQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setPenColorName(QString)));
	p = prop(2, 3); // pen width [double]
	disconnect(rect, SIGNAL(penWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setPenWidth(double)));
	p = prop(2, 4); // pen width in pixel [bool]
	disconnect(rect, SIGNAL(penWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), rect, SLOT(setPenWidthInPixel(bool)));
	p = prop(2, 5); // brush color [QColor]
	disconnect(rect, SIGNAL(brushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setBrushQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setBrushColorName(QString)));
}

void Inspector::unbindRoundRect(ecs::RoundRect* rect)
{
	Property *p;
	p = prop(3, 0); // P1 x, y [double, double]
	disconnect(rect, SIGNAL(p1xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(rect, SIGNAL(p1yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1X(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP1Y(double)));
	p = prop(3, 1); // P2 x, y [double, double]
	disconnect(rect, SIGNAL(p2xChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect(rect, SIGNAL(p2yChanged(double)), (DoubleSpinBox*)p->editor[1], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2X(double)));
	disconnect((DoubleSpinBox*)p->editor[1], SIGNAL(safeValueChanged(double)), rect, SLOT(setP2Y(double)));
	p = prop(3, 2); // rx [double]
	disconnect(rect, SIGNAL(rxChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setRX(double)));
	((DoubleSpinBox*)p->editor[0])->setValue(rect->getRX());
	p = prop(3, 3); // ry [double]
	disconnect(rect, SIGNAL(ryChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setRY(double)));
	p = prop(3, 4); // pen color [QColor]
	disconnect(rect, SIGNAL(penColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setPenQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setPenColorName(QString)));
	p = prop(3, 5); // pen width [double]
	disconnect(rect, SIGNAL(penWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), rect, SLOT(setPenWidth(double)));
	p = prop(3, 6); // pen width in pixel [bool]
	disconnect(rect, SIGNAL(penWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), rect, SLOT(setPenWidthInPixel(bool)));
	p = prop(3, 7); // brush color [QColor]
	disconnect(rect, SIGNAL(brushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), rect, SLOT(setBrushQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), rect, SLOT(setBrushColorName(QString)));
}

void Inspector::unbindText(ecs::Text* text)
{
	Property *p;
	p = prop(4, 0); // caption [QString]
	disconnect(text, SIGNAL(captionChanged(QString)), (LineEdit*)p->editor[0], SLOT(setText(QString)));
	disconnect((LineEdit*)p->editor[0], SIGNAL(editingFinished(QString)), text, SLOT(setCaption(QString)));
	p = prop(4, 1); // rect visible [bool]
	disconnect(text, SIGNAL(rectVisibleChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setRectVisible(bool)));
	p = prop(4, 2); // text pen color [QColor]
	disconnect(text, SIGNAL(textPenColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), text, SLOT(setTextPenQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), text, SLOT(setTextPenColorName(QString)));
	p = prop(4, 3); // text pen width [double]
	disconnect(text, SIGNAL(textPenWidthChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), text, SLOT(setTextPenWidth(double)));
	p = prop(4, 4); // text pen width in pixel [bool]
	disconnect(text, SIGNAL(textPenWidthInPixelChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setTextPenWidthInPixel(bool)));
	p = prop(4, 5); // text brush color [QColor]
	disconnect(text, SIGNAL(textBrushColorChanged(QColor)), (ColorWidget*)p->editor[0], SLOT(setColor(QColor)));
	disconnect((ColorWidget*)p->editor[0], SIGNAL(colorChanged(QColor)), text, SLOT(setTextBrushQColor(QColor)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), text, SLOT(setTextBrushColorName(QString)));
	p = prop(4, 6); // text size [double]
	disconnect(text, SIGNAL(textSizeChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), text, SLOT(setTextSize(double)));
	p = prop(4, 7); // horz align [enum]
	disconnect(text, SIGNAL(horzAlignChanged(int)), (QComboBox*)p->editor[0], SLOT(setCurrentIndex(int)));
	disconnect((QComboBox*)p->editor[0], SIGNAL(currentIndexChanged(int)), text, SLOT(setHAlign(int)));
	p = prop(4, 8); // vert align [enum]
	disconnect(text, SIGNAL(vertAlignChanged(int)), (QComboBox*)p->editor[0], SLOT(setCurrentIndex(int)));
	disconnect((QComboBox*)p->editor[0], SIGNAL(currentIndexChanged(int)), text, SLOT(setVAlign(int)));
	p = prop(4, 9); // font [QString]
	disconnect(text, SIGNAL(fontChanged(QString)), (LineEdit*)p->editor[0], SLOT(setCurrentText(QString)));
	disconnect((LineEdit*)p->editor[0], SIGNAL(currentTextChanged(QString)), text, SLOT(setFontName(QString)));
	p = prop(4, 10); // bold [bool]
	disconnect(text, SIGNAL(boldChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setBold(bool)));
	p = prop(4, 11); // italic [bool]
	disconnect(text, SIGNAL(italicChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setItalic(bool)));
	p = prop(4, 12); // underline [bool]
	disconnect(text, SIGNAL(italicChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setUnderline(bool)));
	p = prop(4, 13); // strike through [bool]
	disconnect(text, SIGNAL(strikeThroughChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), text, SLOT(setStrikeThrough(bool)));
}

void Inspector::unbindImage(ecs::Image* img)
{
	Property *p;
	p = prop(5, 0); // filename [QString]
	disconnect(img, SIGNAL(pathChanged(QString)),(LineEdit*)p->editor[1], SLOT(setText(QString)));
	disconnect((LineEdit*)p->editor[1], SIGNAL(editingFinished(QString)), img, SLOT(setPath(QString)));
	fileSelectButtons[uImgSel]->setProperty("PropAddress", QVariant::fromValue(nullptr));
	p = prop(5, 1); // opacity [double]
	disconnect(img, SIGNAL(opacityChanged(double)), (DoubleSpinBox*)p->editor[0], SLOT(setValue(double)));
	disconnect((DoubleSpinBox*)p->editor[0], SIGNAL(safeValueChanged(double)), img, SLOT(setOpacity(double)));
	p = prop(5, 2); // H-Invert [bool]
	disconnect(img, SIGNAL(hInvertChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), img, SLOT(setHInvert(bool)));
	p = prop(5, 3); // V-Invert [bool]
	disconnect(img, SIGNAL(vInvertChanged(bool)), (QPushButton*)p->editor[0], SLOT(setChecked(bool)));
	disconnect((QPushButton*)p->editor[0], SIGNAL(clicked(bool)), img, SLOT(setVInvert(bool)));
	p = prop(5, 4); // resolution info [QString]
	disconnect(img, SIGNAL(infoChanged(QString)), (QLabel*)p->editor[0], SLOT(setText(QString)));
}






void Inspector::hideAllPages()
{
	for (int i=0; i<(int)pages.size(); i++)
		pages[i]->hide();
}

void Inspector::showPagesForClass(ecs::NodeClass cls)
{
	hideAllPages();
	auto v = classPages[(size_t)cls].second;
	for (size_t i=0; i<v.size(); i++)
		pages[v[i]]->setVisible(true);
}





void Inspector::init()
{
	Enum e;
	e.name = "HorzAlign";
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::HAlign::alhLeft, "Left"));
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::HAlign::alhRight, "Right"));
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::HAlign::alhCenter, "Center"));
	enums.push_back(e);

	e.vals.clear();
	e.name = "VertAlign";
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::VAlign::alvTop, "Top"));
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::VAlign::alvBottom, "Bottom"));
	e.vals.push_back(std::make_pair<qint32, QString>((qint32)ecs::Text::VAlign::alvCenter, "Center"));
	enums.push_back(e);

/* P 0 */   InspectorPage* pg = addPage(tr("General"));
/*   0 */   pg->addStringOroperty("Class", "Node", true, false);
/*   1 */   pg->addStringOroperty("Name", "", false, false);
/*   2 */   pg->addBoolProperty("Visibility", "Show", true, false);


/* P 1 */   pg = addPage(tr("Transform"));
/*   0 */   pg->addVec2Property("Pivot", "X", "Y", -1e4, -1e4, 1e4, 1e4, 0.0, 0.0);
/*   1 */   pg->addVec2Property("Translation", "X", "Y", -1e4, -1e4, 1e4, 1e4, 0.0, 0.0);
/*   2 */   pg->addVec2Property("Scale", "X", "Y", 1e-3, 1e-3, 1e1, 1e1, 1.0, 1.0);
/*   3 */   pg->addScalarProperty("Rotation", "", 0.0, 360.0, 0.0, false);


/* P 2 */   pg = addPage(tr("Rect"));
/*   0 */   pg->addVec2Property("P1 (lower left)", "X", "Y", -1e4, -1e4, 1e4, 1e4, -50.0, -50.0);
/*   1 */   pg->addVec2Property("P2 (upper right)", "X", "Y", -1e4, -1e4, 1e4, 1e4, 50.0, 50.0);

/*   2 */   pg->addColorProperty("Pen Color", ecs::Color("#ffffffff"), false);
/*   3 */   pg->addScalarProperty("Pen Width", "", 0.0, 100.0, 2.5, false);
/*   4 */   pg->addBoolProperty("Pen Width In:", "Pixels", false, true);
/*   5 */   pg->addColorProperty("Brush Color", ecs::Color("#ff00007f"), false);


/* P 3 */   pg = addPage(tr("RoundRect"));
/*   0 */   pg->addVec2Property("P1 (lower left)", "X", "Y", -1e4, -1e4, 1e4, 1e4, -50.0, -50.0);
/*   1 */   pg->addVec2Property("P2 (upper right)", "X", "Y", -1e4, -1e4, 1e4, 1e4, 50.0, 50.0);

/*   2 */   pg->addScalarProperty("Corner Radius", "X", 0.0, 100.0, 20.0, false);
/*   3 */   pg->addScalarProperty("", "Y", 0.0, 100.0, 20.0, true);

/*   4 */   pg->addColorProperty("Pen Color", ecs::Color("#ffffffff"), false);
/*   5 */   pg->addScalarProperty("Pen Width", "", 0.0, 100.0, 2.5, false);
/*   6 */   pg->addBoolProperty("Pen Width In:", "Pixels", false, true);
/*   7 */   pg->addColorProperty("Brush Color", ecs::Color("#ff00007f"), false);


/* P 4 */   pg = addPage(tr("Text"));
/*   0 */   pg->addStringOroperty("Caption", "", false, true);
/*   1 */   pg->addBoolProperty("RoundRect visible", "On", true, true);

/*   2 */   pg->addColorProperty("Text Pen Color", ecs::Color("#ff000000"), false);
/*   3 */   pg->addScalarProperty("Text Pen Width", "", 0.0, 100.0, 2.5, false);
/*   4 */   pg->addBoolProperty("Text Pen Width In:", "Pixels", false, true);
/*   5 */   pg->addColorProperty("Text Brush Color", ecs::Color("#ff00007f"), true);

/*   6 */   pg->addScalarProperty("Text Size", "", 0.5, 1.5, 1.0, true);

/*   7 */   pg->addEnumProperty("Horz Align", "", &enums[0], 0, false);
/*   8 */   pg->addEnumProperty("Vert Align", "", &enums[1], 2, true);

/*   9 */   pg->addFontProperty("Font", "Times New Roman", false);
/*  10 */   pg->addBoolProperty("Bold", "On", false, false);
/*  11 */   pg->addBoolProperty("Italic", "On", false, false);
/*  12 */   pg->addBoolProperty("Underline", "On", false, false);
/*  13 */   pg->addBoolProperty("Strike-Through", "On", false, false);

/* P 5 */   pg = addPage(tr("Image"));
/*   0 */   uImgSel = pg->addFileProperty("Filename", ecs::Image::getFilters(), false);
/*   1 */   pg->addScalarProperty("Opacity", "", 0.0, 1.0, 1.0, false);
/*   2 */   pg->addBoolProperty("H-Invert", "On", false, false);
/*   3 */   pg->addBoolProperty("V-Invert", "On", false, false);
/*   4 */	pg->addStringOroperty("Resolution", "-", true, false);


	ecs::NodeClass cls;
	cls = ecs::NodeClass::Node;
	auto v = new std::vector<qint32>;
	v->push_back(0); // general
	v->push_back(1); // transform
	classPages.push_back(std::make_pair(cls, *v));

	cls = ecs::NodeClass::Rect;
	v = new std::vector<qint32>;
	v->push_back(0); // general
	v->push_back(1); // transform
	v->push_back(2); // rect
	classPages.push_back(std::make_pair(cls, *v));

	cls = ecs::NodeClass::RoundRect;
	v = new std::vector<qint32>;
	v->push_back(0); // general
	v->push_back(1); // transform
	v->push_back(3); // roundrect
	classPages.push_back(std::make_pair(cls, *v));

	cls = ecs::NodeClass::Text;
	v = new std::vector<qint32>;
	v->push_back(0); // general
	v->push_back(1); // transform
	v->push_back(3); // roundrect
	v->push_back(4); // text
	classPages.push_back(std::make_pair(cls, *v));

	cls = ecs::NodeClass::Image;
	v = new std::vector<qint32>;
	v->push_back(0); // general
	v->push_back(1); // transform
	v->push_back(5); // image
	classPages.push_back(std::make_pair(cls, *v));
}

