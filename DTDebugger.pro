QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += deps \
               deps/duktape

SUBDIRS += src/src.pro \
   deps/deps.pro \
	deps/dukglue/dukglue.pro \
	deps/dukglue/dukglue.pro \
	deps/duktape/duktape.pro \
	src/JSedit/JSEdit.pro \
	src/dbg_core/dbg_core.pro


SOURCES += \
   Inspector.cpp \
	Monitor.cpp \
	deps/JSEdit/jsedit.cpp \
	deps/duktape/debug_proto/duk_trans_dvalue.c \
	deps/duktape/duktape.c \
	src/Ani/FunctionGenerator.cpp \
   src/ECS/Image.cpp \
	src/ECS/Node.cpp \
	src/ECS/Rect.cpp \
	src/ECS/RoundRect.cpp \
	src/ECS/Text.cpp \
	src/ECS/Types.cpp \
	src/MainWindow.cpp \
	src/dbg_core/debugger.cpp \
	src/dbg_ui/AMSDockWidget.cpp \
	src/dbg_ui/dbgConsole.cpp \
	src/dbg_ui/dbgController.cpp \
	src/dbg_ui/dbgProject.cpp \
	src/dlgAddNode.cpp \
	src/main.cpp \
	src/dlgAbout.cpp

HEADERS += \
   Inspector.h \
	Monitor.h \
	deps/JSEdit/jsedit.h \
	deps/dukglue/detail_class_proto.h \
	deps/dukglue/detail_constructor.h \
	deps/dukglue/detail_function.h \
	deps/dukglue/detail_method.h \
	deps/dukglue/detail_primitive_types.h \
	deps/dukglue/detail_refs.h \
	deps/dukglue/detail_stack.h \
	deps/dukglue/detail_traits.h \
	deps/dukglue/detail_typeinfo.h \
	deps/dukglue/detail_types.h \
	deps/dukglue/dukexception.h \
	deps/dukglue/dukglue.h \
	deps/dukglue/dukvalue.h \
	deps/dukglue/public_util.h \
	deps/dukglue/register_class.h \
	deps/dukglue/register_function.h \
	deps/dukglue/register_property.h \
	deps/duktape/debug_proto/duk_trans_dvalue.h \
	deps/duktape/duk_config.h \
	deps/duktape/duktape.h \
	src/Ani/FunctionGenerator.h \
	src/ECS/Consts.h \
   src/ECS/Image.h \
	src/ECS/Node.h \
	src/ECS/Rect.h \
	src/ECS/RoundRect.h \
	src/ECS/Text.h \
	src/ECS/Types.h \
	src/MainWindow.h \
	src/dbg_core/debug_hook.h \
	src/dbg_core/debugger.h \
	src/dbg_ui/AMSDockWidget.h \
	src/dbg_ui/dbgConsole.h \
	src/dbg_ui/dbgController.h \
	src/dbg_ui/dbgProject.h \
	src/dlgAbout.h \
	src/dlgAddNode.h


FORMS += \
    src/ui_files/MainWindow.ui \
	 src/ui_files/dlgAbout.ui \
	 src/ui_files/dlgAddNode.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

RESOURCES += \
   src/rc_files/DTDebugger.qrc

