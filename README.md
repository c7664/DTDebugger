DTDebugger
==========

> NOTE: This project is in pre-alpha state!

This is a small JavaScript-debugger using [Qt5](https://qt.io) as a gui-frontend, [Duktape](https://github.com/svaarala/duktape) as the scripting-engine and [Dukglue](https://github.com/Aloshi/dukglue) as a C++-wrapper for Duktape.

As a programmer you would normally use an external editor/debugger/IDE to maintain your scripts, especially in a game-engine. When you want to ship your application with a fully integrated solution including a debugger, you'll have to start somewhere.

This project might be used as a starting-point for applications in need of a scripting-engine/sorce-code editor/debugger.

There is a script-file in the tests-folder that looks like this:

*[JavaScript]*

```js
//use strict;
//var evalMe = 0;
// a function
function inc(val) {
	var res = val + 1;
	return res;
}

// another function
function sqr(x) {
	var y = inc(x);
	return y * y;
}

// a function
function myLoop() {
	var i=0;
	for (i = 0; i < 10; i++) {
		var evalMe = sqr(i);
		print('evalMe = ', evalMe);
	}
}
```

Current development state:


![screenshot](screenshots/Screenshot_01.png)





