include::../config.adoc[]
// German translation, courtesy of Florian Wilhelm
// zu finden in: /usr/share/ruby-asciidoctor/locale
:appendix-caption: Anhang
:appendix-refsig: {appendix-caption}
:caution-caption: Achtung
:chapter-label: Kapitel
:chapter-refsig: {chapter-label}
:example-caption: Beispiel
:figure-caption: Abbildung
:important-caption: Wichtig
:last-update-label: Zuletzt aktualisiert
ifdef::listing-caption[:listing-caption: Listing]
ifdef::manname-title[:manname-title: Bezeichnung]
:note-caption: Anmerkung
:part-label: Teil
:part-refsig: {part-label}
ifdef::preface-title[:preface-title: Vorwort]
:section-refsig: Abschnitt
:table-caption: Tabelle
:tip-caption: Hinweis
:untitled-label: Ohne Titel
:version-label: Version
:warning-caption: Warnung
ifeval::["{backend}" != "pdf"]
:toc-title: Inhalt
:showtitle!:
= {AppName}
endif::[]
ifeval::["{backend}" == "pdf"]
:toc-title: Inhaltsverzeichnis
= {AppName}: {appdesc}
{appauthor} <{appauthoremail}>
V {appversion}, {appyear}
:reproducible:
:title-page:
:toc:
:pdf-page-size: Letter
:title-logo-image: image:handbook.svg[handbook]
endif::[]

// landing page
ifeval::["{backend}" != "pdf"]
//image::SLS.png[align="center", width=64]
[.text-center]
{appname} - {appdesc} - V {appversion} + 
{appauthor} <{appauthoremail}>, {appyear} + 
Quelltext: {appurl} + 

//image::sc0.png[width=50%,align="center"]
endif::[]
// TOC
// in pdf make the license the preamble:
ifeval::["{backend}" == "pdf"]
include::LicensePlain.adoc[Lizenz]
endif::[]

== Willkommen

ifeval::["{backend}" != "pdf"]
include::License.adoc[Lizenz]
endif::[]
include::AboutQt.adoc[Über Qt]
include::AboutDocumentation.adoc[Über diese Dokumentation]
include::WhatsNew.adoc[Neuigkeiten]
include::Welcome.adoc[Willkommen]

== Erste Schritte

=== Installation

include::InstallWindows.adoc[Windows]
include::InstallLinux.adoc[Linux]
include::InstallMac.adoc[Mac]

=== Schnellübersicht

ifeval::["{backend}" == "pdf"]
[index]
== Index
endif::[]
